using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace NExtension.UI.NEditor
{
    public class NUIEditorUtils
    {
        [ExecuteInEditMode]
        [MenuItem("NEditorUtils/FindNButtonEffects")]
        public static void findNButtonEffects()
        {
            var objects = new List<UnityEngine.Object>();
            var sltObject = Selection.gameObjects;
            if (sltObject.Length <= 0)
            {
                sltObject = Editor.FindObjectsOfType<GameObject>(true).ToArray();
            }
            foreach (var g in sltObject)
            {
                var es = g.GetComponentsInChildren<NButtonEffect>(true);
                foreach (var e in es)
                {
                    if (!objects.Contains(e.gameObject))
                    {
                        objects.Add(e.gameObject);
                    }
                }
            }
            Selection.objects = objects.ToArray();
        }
    }
}
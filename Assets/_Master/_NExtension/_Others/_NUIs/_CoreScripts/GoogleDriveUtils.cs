﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.Networking;
using NExtension;
using System.Collections;
using NExtension.NUtilities;

public class GoogleDriveUtils 
{
    #region GG services config
    private const int MAX_SINGLE_UPLOAD = 5 * 1024 * 1024;

    private static string CLIENT_ID;
    private static string CLIENT_SECRET;
    private static string REFRESH_TOKEN;
    #endregion

    public static string ACCESS_TOKEN;
    private static float TokenCreatedDateTime;

    private static void runDriveFileResponseCallback(UnityWebRequest request, Action<bool, DriveFileResponse> callback)
    {
        Debug.Log(request.downloadHandler.text);
        if (request.result != UnityWebRequest.Result.Success)
        {
            callback?.Invoke(false, null);
        }
        else
        {
            var fileRes = JsonUtility.FromJson<DriveFileResponse>(request.downloadHandler.text);
            callback?.Invoke(true, fileRes);
        }
    }
    private static void runDriveSearchResponseCallback(UnityWebRequest request, Action<bool, DriveSearchResponse> callback)
    {
        Debug.Log(request.downloadHandler.text);
        if (request.result != UnityWebRequest.Result.Success)
        {
            callback?.Invoke(false, null);
        }
        else
        {
            var fileRes = JsonUtility.FromJson<DriveSearchResponse>(request.downloadHandler.text);
            callback?.Invoke(true, fileRes);
        }
    }

    public static void init(string clientId, string clientSecret, string refreshToken)
    {
        CLIENT_ID = clientId;
        CLIENT_SECRET = clientSecret;
        REFRESH_TOKEN = refreshToken;
    }
    public static void updateToken(Action<bool> comepleteCallback)
    {
        if (string.IsNullOrWhiteSpace(ACCESS_TOKEN) || string.IsNullOrEmpty(ACCESS_TOKEN) || (Time.unscaledDeltaTime - TokenCreatedDateTime) > 2000)
        {
            refreshToken((tk) =>
            {
                ACCESS_TOKEN = tk;
                TokenCreatedDateTime = Time.unscaledDeltaTime;
                comepleteCallback?.Invoke(true);
            }, () =>
            {
                comepleteCallback?.Invoke(false);
            });
        }
        else
        {
            comepleteCallback?.Invoke(true);
        }
    }
    public static void refreshToken(Action<string> successfulCallback = null, Action failedCallback = null)
    {
        IEnumerator enumeratorRefreshToken()
        {
            WWWForm form = new WWWForm();
            form.AddField("client_id", CLIENT_ID);
            form.AddField("client_secret", CLIENT_SECRET);
            form.AddField("refresh_token", REFRESH_TOKEN);
            form.AddField("grant_type", "refresh_token");
            UnityWebRequest request = UnityWebRequest.Post("https://oauth2.googleapis.com/token", form);
            yield return request.SendWebRequest();
            if (request.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Error: " + request.error);
                failedCallback?.Invoke();
            }
            else
            {
                var res = JsonUtility.FromJson<RefreshTokenResponse>(request.downloadHandler.text);
                Debug.Log("---> New token: " + res.access_token);
                successfulCallback?.Invoke(res.access_token);
            }
        }
        NCoroutine.startCoroutine(enumeratorRefreshToken());
    }
    public static void getListFiles(Action<string> listFilesCallback = null)
    {
        updateToken((success) =>
        {
            if (success)
            {
                IEnumerator enumeratorListFiles()
                {
                    var uri = Uri.EscapeUriString($"https://www.googleapis.com/drive/v3/files?access_token={ACCESS_TOKEN}");
                    UnityWebRequest request = UnityWebRequest.Get(uri);
                    yield return request.SendWebRequest();
                    if (request.result != UnityWebRequest.Result.Success)
                    {
                        Debug.Log(request.error);
                    }
                    else
                    {
                        Debug.Log("---> Files: " + request.downloadHandler.text);
                        listFilesCallback?.Invoke(request.downloadHandler.text);
                    }
                }
                NCoroutine.startCoroutine(enumeratorListFiles());

            }
        });
    }

    public static void uploadFile(byte[] fileData, string name, string parentId = null, Action<float> uploadingCallback = null, Action<bool, DriveFileResponse> completedCallback = null)
    {
        if (fileData.Length > MAX_SINGLE_UPLOAD)
        {
            uploadLargeFile(fileData, name, parentId, uploadingCallback, completedCallback);
        }
        else
        {
            updateToken((success) =>
            {
                if (success)
                {
                    IEnumerator enumeratorUploadTexture()
                    {
                        var data = fileData;
                        var request = createUploadSmallFileRequest(data, name, parentId);
                        var o = request.SendWebRequest();
                        while (!o.isDone)
                        {
                            uploadingCallback?.Invoke(request.uploadProgress);
                            yield return null;
                        }

                        runDriveFileResponseCallback(request, completedCallback);
                    }
                    NCoroutine.startCoroutine(enumeratorUploadTexture());
                }
                else
                {
                    completedCallback?.Invoke(false, null);
                }
            });
        }
    }
    public static void uploadTextureAsJPG(Texture2D texture, string name, Action<float> uploadingCallback = null, Action<bool,DriveFileResponse> completedCallback = null)
    {
        var jpg = texture.EncodeToJPG();
        uploadFile(jpg, name, null, uploadingCallback, completedCallback);
    }
    public static void uploadTextureAsJPG(Texture2D texture, string name, string parentId, Action<float> uploadingCallback = null, Action<bool, DriveFileResponse> completedCallback = null)
    {
        var jpg = texture.EncodeToJPG();
        uploadFile(jpg, name, parentId, uploadingCallback, completedCallback);
    }
    public static void createFolder(string name, string parentFolderId = null, Action<bool,DriveFileResponse> completedCallback = null)
    {
        updateToken((success) =>
        {
            if (success)
            {
                IEnumerator enumeratorCreateFolder()
                {
                    var request = createFolderRequest(name, parentFolderId);

                    yield return request.SendWebRequest();
                    runDriveFileResponseCallback(request, completedCallback);
                }
                NCoroutine.startCoroutine(enumeratorCreateFolder());
            }
            else
            {
                completedCallback?.Invoke(false, null);
            }
        });
    }
    public static void findFolder(string folderName, string parentId = null, Action<bool,DriveFileResponse> completedCallback = null)
    {
        updateToken((success) =>
        {
            if (success)
            {
                IEnumerator enumeratorCreateFolder()
                {
                    var request = createRequestFindRequest(folderName, parentId);

                    yield return request.SendWebRequest();
                    runDriveSearchResponseCallback(request, (b, searchResult) =>
                    {
                        if (!b)
                        {
                            completedCallback?.Invoke(false, null);
                        }
                        else
                        {
                            bool isFound = false; 
                            foreach (var f in searchResult.files)
                            {
                                if (f.name == folderName && f.mimeType == "application/vnd.google-apps.folder")
                                {
                                    completedCallback?.Invoke(true, f);
                                    isFound = true;
                                    break;
                                }
                            }
                            if (!isFound)
                            {
                                completedCallback?.Invoke(false, null);
                            }
                        }
                    });
                }
                NCoroutine.startCoroutine(enumeratorCreateFolder());
            }
            else
            {
                completedCallback?.Invoke(false, null);
            }
        });
    }
    public static void findFile(string fileName, string parentId = null, Action<bool, DriveFileResponse> completedCallback = null)
    {
        updateToken((success) =>
        {
            if (success)
            {
                IEnumerator enumeratorCreateFolder()
                {
                    var request = createRequestFindRequest(fileName, parentId);

                    yield return request.SendWebRequest();
                    runDriveSearchResponseCallback(request, (b, searchResult) =>
                    {
                        if (!b)
                        {
                            completedCallback?.Invoke(false, null);
                        }
                        else
                        {
                            bool isFound = false;
                            foreach (var f in searchResult.files)
                            {
                                if (f.name == fileName && f.mimeType != "application/vnd.google-apps.folder")
                                {
                                    completedCallback?.Invoke(true, f);
                                    isFound = true;
                                    break;
                                }
                            }
                            if (!isFound)
                            {
                                completedCallback?.Invoke(false, null);
                            }
                        }
                    });
                }
                NCoroutine.startCoroutine(enumeratorCreateFolder());
            }
        });
    }
    //public static void getUrlDownload(string fileId, Action<bool, string> completeCallback = null)
    //{
    //    updateToken((success) =>
    //    {
    //        if (success)
    //        {
    //            IEnumerator enumeratorGetDownloadLink()
    //            {
    //                var linkRequest = createDownloadRequest(fileId);
    //                yield return linkRequest.SendWebRequest();
    //                Debug.Log(linkRequest.downloadHandler.text);
    //                if (linkRequest.isNetworkError || linkRequest.isHttpError)
    //                {
    //                    completeCallback?.Invoke(false, null);
    //                }
    //                else
    //                {
    //                    var linkRes = JsonUtility.FromJson<DriveLinkDownloadResponse>(linkRequest.downloadHandler.text);
    //                    completeCallback?.Invoke(true, linkRes.webContentLink);
    //                }
    //            }
    //            NCoroutine.startCoroutine(enumeratorGetDownloadLink());
    //        }
    //    });
    //}
    public static void downloadFile(string fileId, Action<bool,byte[]> completedCallback = null)
    {
        updateToken((success) =>
        {
            if (success)
            {
                IEnumerator enumeratorRequestLink()
                {
                    var request = createDownloadRequest(fileId);
                    yield return request.SendWebRequest();
                    Debug.Log(request.downloadHandler.data.Length);
                    if (request.result != UnityWebRequest.Result.Success)
                    {
                        completedCallback?.Invoke(false, null);
                    }
                    else
                    {
                        completedCallback?.Invoke(true, request.downloadHandler.data);
                        //var linkRes = JsonUtility.FromJson<DriveLinkDownloadResponse>(linkRequest.downloadHandler.text);
                        //UnityWebRequest request = UnityWebRequest.Get(linkRes.webContentLink);
                        //request.SetRequestHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                        //yield return request.SendWebRequest();
                        //if (request.result != UnityWebRequest.Result.Success)
                        //{
                        //    completedCallback?.Invoke(false, null);
                        //}
                        //else
                        //{
                        //    completedCallback?.Invoke(true, request.downloadHandler.data);
                        //}
                    }
                }
                NCoroutine.startCoroutine(enumeratorRequestLink());
            }
        });
    }

    //public static void downloadFromUrl(string url, Action<bool,byte[]> completedCallback)
    //{
    //    IEnumerator enumeratorDownloadFromUrl()
    //    {
    //        UnityWebRequest request = UnityWebRequest.Get(url);
    //        request.SetRequestHeader("Authorization", "Bearer " + ACCESS_TOKEN);
    //        yield return request.SendWebRequest();
    //        if (request.result != UnityWebRequest.Result.Success)
    //        {
    //            completedCallback?.Invoke(false, null);
    //        }
    //        else
    //        {
    //            completedCallback?.Invoke(true, request.downloadHandler.data);
    //        }
    //    }
    //    NCoroutine.startCoroutine(enumeratorDownloadFromUrl());
    //}

    public static void uploadLargeFile(byte[] jpgData, string name, string parentId, Action<float> uploadingCallback = null, Action<bool, DriveFileResponse> completedCallback = null)
    {
        updateToken((success) =>
        {
            if (success)
            {
                IEnumerator enumeratorUploadTexture()
                {
                    var jpg = jpgData;
                    var request = createUploadLargeFileInitRequest(name, parentId);
                    yield return request.SendWebRequest();
                    if (request.result != UnityWebRequest.Result.Success)
                    {
                        completedCallback?.Invoke(false, null);
                    }
                    else
                    {
                        int pointer = 0;
                        int chunkSize = 256 * 1024;
                        int chunkFailedCount = 0;
                        int maxChunkFailed = 10;
                        while (pointer < jpgData.Length)
                        {
                            var offset = (jpgData.Length - pointer) < chunkSize ? (jpgData.Length - pointer) : chunkSize;
                            var b =  NUtils.getBlock(jpgData, pointer, offset);
                            var sessionUri = request.GetResponseHeader("Location");
                            var uploadReq = UnityWebRequest.Put(sessionUri, b);
                            uploadReq.SetRequestHeader("Content-Range", $"bytes {pointer}-{pointer + b.Length - 1}/{jpgData.Length}");
                            yield return uploadReq.SendWebRequest();
                            if (uploadReq.responseCode > 400)
                            {
                                chunkFailedCount++;
                                if (chunkFailedCount >= maxChunkFailed)
                                {
                                    completedCallback?.Invoke(false, null);
                                    break;
                                }
                            }
                            else
                            {
                                if (uploadReq.responseCode == 308)
                                {
                                    var range = uploadReq.GetResponseHeader("Range");
                                    if (!string.IsNullOrEmpty(range))
                                    {
                                        chunkFailedCount = 0;
                                        var index = range.IndexOf("-");
                                        var num = int.Parse(range.Remove(0, index + 1)) + 1;
                                        pointer = num;
                                        uploadingCallback?.Invoke(pointer * 1f / jpgData.Length);
                                    }
                                }
                                else
                                {
                                    if (uploadReq.responseCode == 200 || uploadReq.responseCode == 201)
                                    {
                                        var driveFile = JsonUtility.FromJson<DriveFileResponse>(uploadReq.downloadHandler.text);
                                        uploadingCallback?.Invoke(1);
                                        completedCallback?.Invoke(true, driveFile);
                                    }
                                    else
                                    {
                                        completedCallback?.Invoke(false, null);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                NCoroutine.startCoroutine(enumeratorUploadTexture());
            }
        });
    }

    private static UnityWebRequest createUploadSmallFileRequest(byte[] jpg, string fileName, string parentId)
    {
        JsonContainer jsonContainer = new JsonContainer();
        jsonContainer.addNode("name", fileName);
        if (!string.IsNullOrEmpty(parentId) && !string.IsNullOrWhiteSpace(parentId))
        {
            jsonContainer.addArrayNode("parents", parentId);
        }
        MultipartForm metadataPart = new MultipartForm();
        metadataPart.contentType = "application/json; charset=UTF-8";
        metadataPart.sectionData = jsonContainer.toBytes();
        metadataPart.sectionName = "metadata";
        MultipartForm dataPart = new MultipartForm();
        dataPart.sectionData = jpg;
        dataPart.contentType = "image/jpeg";
        dataPart.sectionName = "file";

        var b = UnityWebRequest.GenerateBoundary();
        var boundary = Encoding.ASCII.GetString(b);
        UnityWebRequest request = UnityWebRequest.Post("https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart", new List<IMultipartFormSection>() { metadataPart, dataPart }, b);
        request.SetRequestHeader("Authorization", "Bearer " + ACCESS_TOKEN);
        request.SetRequestHeader("Content-Type", $"multipart/related; boundary={boundary}");
        request.downloadHandler = new DownloadHandlerBuffer();
        return request;
    }
    private static UnityWebRequest createFolderRequest(string folderName, string parentFolderId = null)
    {
        UnityWebRequest request = new UnityWebRequest("https://www.googleapis.com/drive/v3/files", UnityWebRequest.kHttpVerbPOST);
        JsonContainer jsonContainer = new JsonContainer();
        jsonContainer.addNode("mimeType", "application/vnd.google-apps.folder");
        jsonContainer.addNode("name", folderName);
        if (!string.IsNullOrEmpty(parentFolderId) && !string.IsNullOrWhiteSpace(parentFolderId))
        {
            jsonContainer.addArrayNode("parents", parentFolderId);
        }
        UploadHandlerRaw handler = new UploadHandlerRaw(jsonContainer.toBytes());
        request.uploadHandler = handler;
        request.SetRequestHeader("Authorization", "Bearer " + ACCESS_TOKEN);
        request.SetRequestHeader("Content-Type", "application/json");
        request.downloadHandler = new DownloadHandlerBuffer();
        return request;
    }
    private static UnityWebRequest createChangeFileNameRequest(string fileId, string name)
    {
        UnityWebRequest request = new UnityWebRequest("https://www.googleapis.com/drive/v3/files/" + fileId, "PATCH");
        UploadHandlerRaw handler = new UploadHandlerRaw(Encoding.UTF8.GetBytes("{\"name\":\"" + name + "\"}"));
        request.uploadHandler = handler;
        handler.contentType = "application/json";
        request.SetRequestHeader("Authorization", "Bearer " + ACCESS_TOKEN);
        request.downloadHandler = new DownloadHandlerBuffer();
        return request;
    }
    private static UnityWebRequest createRequestFindRequest(string name, string parentId, bool isTrashed = false)
    {
        var uriStr = $"https://www.googleapis.com/drive/v3/files?q=name = '{name}' and trashed = {isTrashed}";
        if (!string.IsNullOrEmpty(parentId) && !string.IsNullOrWhiteSpace(parentId))
        {
            uriStr += $" and '{parentId}' in parents";
        }
        var uri = Uri.EscapeUriString(uriStr);
        UnityWebRequest request = UnityWebRequest.Get(uri);
        request.SetRequestHeader("Authorization", "Bearer " + ACCESS_TOKEN);
        request.downloadHandler = new DownloadHandlerBuffer();
        return request; 
    }
    private static UnityWebRequest createDownloadRequest(string fileId)
    {
        UnityWebRequest request = new UnityWebRequest("https://www.googleapis.com/drive/v3/files/" + fileId + "?alt=media", UnityWebRequest.kHttpVerbGET);
        request.SetRequestHeader("Authorization", "Bearer " + ACCESS_TOKEN);
        request.downloadHandler = new DownloadHandlerBuffer();
        return request;

    }
    private static UnityWebRequest createUploadLargeFileInitRequest(string fileName, string parentId)
    {
        JsonContainer jsonContainer = new JsonContainer();
        jsonContainer.addNode("name", fileName);
        if (!string.IsNullOrEmpty(parentId) && !string.IsNullOrWhiteSpace(parentId))
        {
            jsonContainer.addArrayNode("parents", parentId);
        }

        var b = UnityWebRequest.GenerateBoundary();
        UnityWebRequest request = new UnityWebRequest("https://www.googleapis.com/upload/drive/v3/files?uploadType=resumable", UnityWebRequest.kHttpVerbPOST);
        UploadHandlerRaw handler = new UploadHandlerRaw(jsonContainer.toBytes());
        request.uploadHandler = handler;
        request.SetRequestHeader("Authorization", "Bearer " + ACCESS_TOKEN);
        request.SetRequestHeader("Content-Type", $"application/json; charset=UTF-8");
        request.downloadHandler = new DownloadHandlerBuffer();
        return request;
    }
}

public class RefreshTokenResponse
{
    public string access_token;
    public string expires_in;
    public string scope;
    public string token_type;
}

[Serializable]
public class DriveFileResponse
{
    public string kind;
    public string id;
    public string name;
    public string mimeType;
}

public class JsonContainer
{
    public Dictionary<string, string> JNode = new Dictionary<string, string>();
    public Dictionary<string, string[]> JArrayNode = new Dictionary<string, string[]>();
    public void addNode(string key, string value)
    {
        JNode.Add(key, value);
    }
    public void addArrayNode(string key, params string[] values)
    {
        JArrayNode.Add(key, values);
    }
    public string toJson()
    {
        var str = "{";
        foreach (var d in JNode)
        {
            str += $"\"{d.Key}\":\"{d.Value}\","; 
        }
        foreach (var d in JArrayNode)
        {
            string arrayStr = "";
            foreach (var v in d.Value)
            {
                arrayStr += $"\"{v}\",";
            }
            if (arrayStr.EndsWith(","))
            {
                arrayStr = arrayStr.Remove(arrayStr.Length - 1, 1);
            }
            str += $"\"{d.Key}\":[{arrayStr}],";
        }
        if (str.EndsWith(","))
        {
            str = str.Remove(str.Length - 1, 1);
        }
        str += "}";
        return str;
    }
    public byte[] toBytes()
    {
        return Encoding.UTF8.GetBytes(toJson());
    }
}

public class MultipartForm : IMultipartFormSection
{
    public string sectionName { get; set; }

    public byte[] sectionData { get; set; }

    public string fileName { get; set; }

    public string contentType { get; set; }
}

public class DriveSearchResponse
{
    public string kind;
    public bool incompleteSearch;
    public DriveFileResponse[] files;
}

//public class DriveLinkDownloadResponse
//{
//    public string webContentLink;
//}



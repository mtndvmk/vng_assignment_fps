﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class MyCanvas : MonoBehaviour
{
    [SerializeField] private Canvas m_MyCanvas;
    public Canvas GetMyCanvas => m_MyCanvas;

    private void OnValidate()
    {
        findMyCanvas();
    }
    [ContextMenu("Find my canvas")]
    public void findMyCanvas()
    {
        var p = transform;
        while (!m_MyCanvas)
        {
            if (p)
            {
                m_MyCanvas = p.GetComponent<Canvas>();
                if (m_MyCanvas)
                {
                    break;
                }
                else
                {
                    p = p.parent;
                }
            }
            else
            {
                break;
            }
        }
    }
}

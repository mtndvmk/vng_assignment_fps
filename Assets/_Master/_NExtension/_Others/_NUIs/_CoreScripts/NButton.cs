using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEditor;

namespace NExtension.UI
{
    public class NButton : Button
    {
        public UnityEvent onPointerDownCallback;
        public UnityEvent onPointerUpCallback;

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            onPointerDownCallback?.Invoke();
        }
        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);
            onPointerUpCallback?.Invoke();
        }
    }
}

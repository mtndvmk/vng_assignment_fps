using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NExtension.UI
{
    [RequireComponent(typeof(Button))]
    public class NButtonEffect : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Vector3 m_OriginScale = Vector3.one;
        [SerializeField] private Vector3 m_EnterScale = new Vector3(1.1f, 1.1f, 1.1f);
        [SerializeField] private float m_AnimationDuration = 0.25f;
        [SerializeField] private bool m_IsBlockDuplicateClick = true;
        [SerializeField] private bool m_IsResetOnEnable = true;

        private Button m_SelfButton;

        private void OnEnable()
        {
            if (m_IsResetOnEnable)
            {
                setRaycastTarget(true);
                doScale(m_OriginScale, 0);
            }
        }

        void Start()
        {
            m_SelfButton = GetComponent<Button>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (m_IsBlockDuplicateClick && m_SelfButton.IsInteractable())
            {
                setRaycastTarget(false);
                doScale(m_OriginScale, m_AnimationDuration);
                NCoroutine.runDelay(m_AnimationDuration, () =>
                {
                    setRaycastTarget(true);
                });
            }
        }
        private void setRaycastTarget(bool isTargetable)
        {
            foreach (var g in GetComponentsInChildren<Graphic>(true))
            {
                g.raycastTarget = isTargetable;
            }
        }
        private void doScale(Vector3 target, float duration)
        {
            if (duration > 0)
            {
                NCoroutine.startCoroutine(coroutineScale(target, duration), GetInstanceID(), true);
            }
            else
            {
                transform.transform.localScale = target;
            }
        }
        private IEnumerator coroutineScale(Vector3 target, float duration)
        {
            var t = 0f;
            while (t < duration)
            {
                transform.localScale = Vector3.Lerp(transform.localScale, target, t / duration);
                t += Time.deltaTime;
                yield return null;
            }
            transform.transform.localScale = target;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (m_SelfButton.IsInteractable())
                doScale(m_OriginScale, m_AnimationDuration);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (m_SelfButton.IsInteractable())
                doScale(m_EnterScale, m_AnimationDuration);
        }
    }
}
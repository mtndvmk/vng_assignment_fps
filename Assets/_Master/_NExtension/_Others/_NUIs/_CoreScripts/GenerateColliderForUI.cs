﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class GenerateColliderForUI : MonoBehaviour
{
    [SerializeField] private bool m_IsAllowValidate;
    private void OnValidate()
    {
        if (m_IsAllowValidate)
        {
            updateCollider();
        }
    }
    [ContextMenu("Update collider")]
    public void updateCollider()
    {
        var rt = GetComponent<RectTransform>();
        if (rt)
        {
            var box = GetComponent<BoxCollider>();
            if (!box)
            {
                box = gameObject.AddComponent<BoxCollider>();
                box.size = new Vector3(rt.rect.width, rt.rect.height, 1);
                box.center = new Vector3((0.5f - rt.pivot.x) * box.size.x, (0.5f - rt.pivot.y) * box.size.y);
            }
            else
            {
                box.size = new Vector3(rt.rect.width, rt.rect.height, 1);
                box.center = new Vector3((0.5f - rt.pivot.x) * box.size.x, (0.5f - rt.pivot.y) * box.size.y);
            }
        }
    }
}

using NExtension.NSync;
using NExtension.NSync.V2;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestNSyncBridge : MonoBehaviour
{
    [SerializeField] private NSyncController[] m_NSyncControllers;

    private void Awake()
    {
        foreach (var c in m_NSyncControllers)
        {
            c.init();
        }
        foreach (var c in m_NSyncControllers)
        {
            c.onOutputDataEvent += onInputData;
        }
        m_NSyncControllers[0].setLocalIsSuper();
        m_NSyncControllers[1].connectToNSyncMaster();
    }
    private void onInputData(byte[] data)
    {
        foreach (var c in m_NSyncControllers)
        {
            c.inputData(data);
        }
    }

}

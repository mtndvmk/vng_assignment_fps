using NExtension.NUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NExtension.NSync
{
    [Serializable]
    public class NSyncDataManager
    {
        public const int MAX_PROFILE = 15;
        public const bool IS_RELEASE_OFFLINE = true;
        private NSyncDataManager() { }

        public event Action<bool> onProfileUpdatedEvent;
        public event Action<NSyncProfile> onLocalProfileUpdatedEvent;

        public static NSyncDataManager initLocalProfile()
        {
            var dataManager = new NSyncDataManager();
            dataManager.LocalProfile = new NSyncProfile() { uniqueId = IdGenerator.DHMSx1000.generateId() };
            dataManager.NSyncProfiles.Add(dataManager.LocalProfile.uniqueId, dataManager.LocalProfile);
            dataManager.CurrentProfiles = dataManager.NSyncProfiles.Values.ToList();
            return dataManager;
        }
        /// <summary>
        /// uniqueId, Profile
        /// </summary>
        public Dictionary<int, NSyncProfile> NSyncProfiles { get; private set; } = new Dictionary<int, NSyncProfile>();

        public List<NSyncProfile> CurrentProfiles { get; private set; }
        public NSyncProfile LocalProfile { get; private set; }

        public void setLocalIsSuper(bool isLocalOnly = false)
        {
            if (LocalProfile.order <= 0)
            {
                LocalProfile.order = getEmptyOrder();
            }
            foreach (var p in NSyncProfiles)
            {
                p.Value.isSuper = p.Key == LocalProfile.uniqueId;
            }
            onProfileUpdatedEvent?.Invoke(isLocalOnly);
            onLocalProfileUpdatedEvent?.Invoke(LocalProfile);
        }

        public int getEmptyOrder()
        {
            var profiles = NSyncProfiles.Values.ToList();
            for (int i = 1; i <= MAX_PROFILE; i++)
            {
                if (profiles.Exists((p) => p.order == i))
                {
                    continue;
                }
                return i;
            }
            if (IS_RELEASE_OFFLINE)
            {
                for (int i = 1; i <= MAX_PROFILE; i++)
                {
                    var p = profiles.Find((p) => p.order == i);
                    if (p == null || p.activeStatus == ActiveStatus.OFFLINE)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        public void updateProfiles(NSyncProfile profile, bool isLocalOnly)
        {
            if (!NSyncProfiles.ContainsKey(profile.uniqueId))
            {
                NSyncProfiles.Add(profile.uniqueId, profile);
            }
            if (profile.order == -1)
            {
                profile.order = getEmptyOrder();
            }
            CurrentProfiles = NSyncProfiles.Values.ToList();
            onProfileUpdatedEvent?.Invoke(isLocalOnly);
        }
        public NSyncProfile getProfile(int uniqueId)
        {
            if (NSyncProfiles.ContainsKey(uniqueId))
                return NSyncProfiles[uniqueId];
            return null;
        }
        public NSyncProfile disconnectToProfile(int uniqueId, bool isLocalOnly)
        {
            if (NSyncProfiles.ContainsKey(uniqueId))
            {
                NSyncProfiles[uniqueId].activeStatus = ActiveStatus.OFFLINE;
                onProfileUpdatedEvent?.Invoke(isLocalOnly);
                return NSyncProfiles[uniqueId];
            }
            return null;
        }
        public byte[] exportToBytes()
        {
            var data = new byte[0];
            foreach (var p in NSyncProfiles.Values)
            {
                data = data.mergeTo(p.exportToBytes());
            }
            return data;
        }
        public void importBytes(byte[] inData, bool isLocalOnly)
        {
            NSyncProfiles.Clear();
            int offset = 0;
            while (offset < inData.Length)
            {
                var p = new NSyncProfile();
                var d = NUtils.getBlock(inData, offset, NSyncProfile.PROFILE_SIZE); offset += NSyncProfile.PROFILE_SIZE;
                p.importBytes(d);
                NSyncProfiles.Add(p.uniqueId, p);
                if (p.uniqueId == LocalProfile.uniqueId)
                {
                    LocalProfile = p;
                    onLocalProfileUpdatedEvent?.Invoke(LocalProfile);
                }
            }
            CurrentProfiles = NSyncProfiles.Values.ToList();
            onProfileUpdatedEvent?.Invoke(isLocalOnly);
        }
    }
    [Serializable]
    public class NSyncProfile
    {
        public const int PROFILE_SIZE = 10;
        public int uniqueId;
        public int order = -1;
        public ActiveStatus activeStatus = ActiveStatus.ONLINE;
        public bool isSuper;

        public byte[] exportToBytes()
        {
            var b_Id = BitConverter.GetBytes(uniqueId);
            var b_Order = BitConverter.GetBytes(order);
            var b_ActiveAndSuper = new byte[2] { (byte)activeStatus, BitConverter.GetBytes(isSuper)[0] };
            return NUtils.merge(b_Id, b_Order, b_ActiveAndSuper);
        }

        public NSyncProfile importBytes(byte[] inData)
        {
            int offset = 0;
            uniqueId = BitConverter.ToInt32(inData, offset); offset += 4;
            order = BitConverter.ToInt32(inData, offset); offset += 4;
            activeStatus = (ActiveStatus)inData[offset]; offset += 1;
            isSuper = inData[offset] == 1 ? true : false; offset += 1;
            return this;
        }
    }
    public enum ActiveStatus : byte
    {
        ONLINE,
        OFFLINE
    }
}
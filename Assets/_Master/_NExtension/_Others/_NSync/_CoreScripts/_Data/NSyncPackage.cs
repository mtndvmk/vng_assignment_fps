using NExtension.NUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension.NSync
{
    public enum NSyncDataType : byte
    {
        BYTE,
        BOOL,
        INT,
        INT64,
        FLOAT,
        STRING,
        VECTOR3
    }
    public enum NSyncComponentId : byte
    {
        WATCHER,
        NSYNC_ACTIVATION,
        NSYNC_TRANSFORM,
        NSYNC_LINE_RENDERER,
        OTHER
    }
    public class NSyncHeader
    {
        public const int HEADER_SIZE = 20;
        public int WatcherId { get; private set; }
        public long UpdatedTime { get; private set; }
        public int CompareId => ComponentOrder | ((byte)ComponentId << 8) | ((byte)DataType << 16);
        public int DataLength { get; private set; }

        public NSyncDataType DataType { get; private set; }
        public NSyncComponentId ComponentId { get; private set; }
        public byte ComponentOrder { get; private set; }

        private NSyncHeader()
        {

        }
        internal NSyncHeader(int watcherId, NSyncDataType dataType, NSyncComponentId componentId, byte componentOrder)
        {
            this.WatcherId = watcherId;
            this.DataType = dataType;
            this.ComponentId = componentId;
            this.ComponentOrder = componentOrder;
            updateDataLength(0);
        }
        public void changeWatcherId(int watcherId)
        {
            this.WatcherId = watcherId;
        }
        public bool isSame(NSyncHeader header)
        {
            return this.CompareId == header.CompareId;
        }
        public bool isNewer(NSyncHeader header)
        {
            return UpdatedTime >= header.UpdatedTime;
        }
        public bool isSameAndNewer(NSyncHeader header)
        {
            return isSame(header) & isNewer(header);
        }

        public void updateTime()
        {
            UpdatedTime = DateTime.Now.ToFileTime();
        }
        public void updateDataLength(int length)
        {
            DataLength = length;
            updateTime();
        }
        public byte[] toBytes()
        {
            var b_WatcherId = BitConverter.GetBytes(WatcherId);
            var b_CreatedTime = BitConverter.GetBytes(UpdatedTime);
            var b_CompareId = BitConverter.GetBytes(CompareId);
            var b_DataLength = BitConverter.GetBytes(DataLength);
            return NUtils.merge(b_WatcherId, b_CreatedTime, b_CompareId, b_DataLength);
        }
        public void set(NSyncHeader header)
        {
            this.WatcherId = header.WatcherId;
            this.DataType = header.DataType;
            this.ComponentId = header.ComponentId;
            this.ComponentOrder = header.ComponentOrder;
            this.UpdatedTime = header.UpdatedTime;
            this.DataLength = header.DataLength;
        }
        public static NSyncHeader fromBytes(byte[] data)
        {
            int offset = 0;
            int watchId = BitConverter.ToInt32(data, offset); offset += 4;
            long updatedTime = BitConverter.ToInt64(data, offset); offset += 8;
            int compareId = BitConverter.ToInt32(data, offset); offset += 4;
            int dataLength = BitConverter.ToInt32(data, offset); offset += 4;
            int componentOrder = 0xF & compareId;
            int componentId = 0xF & (compareId >> 8);
            int dataType = 0xF & (compareId >> 16);
            var header = new NSyncHeader();
            header.WatcherId = watchId;
            header.UpdatedTime = updatedTime;
            header.DataType = (NSyncDataType)dataType;
            header.ComponentId = (NSyncComponentId)componentId;
            header.ComponentOrder = (byte)componentOrder;
            header.DataLength = dataLength;
            return header;
        }
    }
    public class NSyncPackage
    {
        public NSyncHeader Header { get; protected set; }
        public byte[] Data { get; protected set; }
        public byte[] toBytes() => NUtils.merge(Header.toBytes(), Data);
        protected NSyncPackage() { }
        public void setDataFrom(byte[] data)
        {
            Data = data;
            updateValue();
        }
        public void setDataFrom(NSyncPackage syncPackage)
        {
            setDataFrom(syncPackage.Data);
        }
        public void from(NSyncPackage package)
        {
            Header.set(package.Header);
            setDataFrom(package.Data);
        }
        public static NSyncPackage fromBytes(byte[] data)
        {
            var offset = 0;
            var headerData = NUtils.getBlock(data, offset, NSyncHeader.HEADER_SIZE); offset += NSyncHeader.HEADER_SIZE;
            var bodyData = NUtils.getBlock(data, offset, data.Length - offset);

            var header = NSyncHeader.fromBytes(headerData);

            NSyncPackage pkg;
            switch (header.DataType)
            {
                case NSyncDataType.BYTE:
                    pkg = new NSyncPackageByte();
                    break;
                case NSyncDataType.BOOL:
                    pkg = new NSyncPackageBool();
                    break;
                case NSyncDataType.INT:
                    pkg = new NSyncPackageInt();
                    break;
                case NSyncDataType.INT64:
                    pkg = new NSyncPackageInt64();
                    break;
                case NSyncDataType.FLOAT:
                    pkg = new NSyncPackageFloat();
                    break;
                case NSyncDataType.STRING:
                    pkg = new NSyncPackageString();
                    break;
                case NSyncDataType.VECTOR3:
                    pkg = new NSyncPackageVector3();
                    break;
                default:
                    pkg = new NSyncPackage();
                    break;
            }
            pkg.Header = header;
            pkg.setDataFrom(bodyData);
            return pkg;
        }
        protected virtual void updateValue() { }
    }
    public abstract class NSyncPackage<T> : NSyncPackage
    {
        public T Value { get; protected set; }
        public void setValue(T value)
        {
            if (!value.Equals(Value))
            {
                Value = value;
                Data = convertValueToBytes();
                Header.updateDataLength(Data.Length);
            }
        }
        protected sealed override void updateValue()
        {
            var value = reverseBytesToValue(Data);
            setValue(value);
        }
        protected abstract byte[] convertValueToBytes();
        protected abstract T reverseBytesToValue(byte[] data);
        public byte[] getValueAsBytes()
        {
            return convertValueToBytes();
        }
    }

    public class NSyncPackageByte : NSyncPackage<byte[]>
    {
        internal NSyncPackageByte() { }
        public NSyncPackageByte(int watcherId, NSyncComponentId componentId = NSyncComponentId.WATCHER, byte componentOrder = 0)
        {
            this.Header = new NSyncHeader(watcherId, NSyncDataType.BYTE, componentId, componentOrder);
            Value = new byte[0];
        }

        protected override byte[] convertValueToBytes()
        {
            return Value;
        }

        protected override byte[] reverseBytesToValue(byte[] data)
        {
            return data;
        }
    }
    public class NSyncPackageBool : NSyncPackage<bool>
    {
        internal NSyncPackageBool() { }
        public NSyncPackageBool(int watcherId, NSyncComponentId componentId = NSyncComponentId.WATCHER, byte componentOrder = 0)
        {
            this.Header = new NSyncHeader(watcherId, NSyncDataType.BOOL, componentId, componentOrder);
        }

        protected override byte[] convertValueToBytes()
        {
            return BitConverter.GetBytes(Value);
        }

        protected override bool reverseBytesToValue(byte[] data)
        {
            return BitConverter.ToBoolean(data, 0);
        }
    }
    public class NSyncPackageInt : NSyncPackage<int>
    {
        internal NSyncPackageInt() { }
        public NSyncPackageInt(int watcherId, NSyncComponentId componentId = NSyncComponentId.WATCHER, byte componentOrder = 0)
        {
            this.Header = new NSyncHeader(watcherId, NSyncDataType.INT, componentId, componentOrder);
        }

        protected override byte[] convertValueToBytes()
        {
            return BitConverter.GetBytes(Value);
        }

        protected override int reverseBytesToValue(byte[] data)
        {
            return BitConverter.ToInt32(data, 0);
        }
    }
    public class NSyncPackageInt64 : NSyncPackage<long>
    {
        internal NSyncPackageInt64() { }
        public NSyncPackageInt64(int watcherId, NSyncComponentId componentId = NSyncComponentId.WATCHER, byte componentOrder = 0)
        {
            this.Header = new NSyncHeader(watcherId, NSyncDataType.INT64, componentId, componentOrder);
        }

        protected override byte[] convertValueToBytes()
        {
            return BitConverter.GetBytes(Value);
        }

        protected override long reverseBytesToValue(byte[] data)
        {
            return BitConverter.ToInt64(data, 0);
        }
    }
    public class NSyncPackageFloat : NSyncPackage<float>
    {
        internal NSyncPackageFloat() { }
        public NSyncPackageFloat(int watcherId, NSyncComponentId componentId = NSyncComponentId.WATCHER, byte componentOrder = 0)
        {
            this.Header = new NSyncHeader(watcherId, NSyncDataType.FLOAT, componentId, componentOrder);
        }

        protected override byte[] convertValueToBytes()
        {
            return BitConverter.GetBytes(Value);
        }

        protected override float reverseBytesToValue(byte[] data)
        {
            return BitConverter.ToSingle(data, 0);
        }
    }
    public class NSyncPackageString : NSyncPackage<string>
    {
        internal NSyncPackageString() { }
        public NSyncPackageString(int watcherId, NSyncComponentId componentId = NSyncComponentId.WATCHER, byte componentOrder = 0)
        {
            this.Header = new NSyncHeader(watcherId, NSyncDataType.STRING, componentId, componentOrder);
        }

        protected override byte[] convertValueToBytes()
        {
            return System.Text.Encoding.UTF8.GetBytes(Value);
        }

        protected override string reverseBytesToValue(byte[] data)
        {
            return System.Text.Encoding.UTF8.GetString(data);
        }
    }
    public class NSyncPackageVector3 : NSyncPackage<Vector3>
    {
        internal NSyncPackageVector3() { }
        public NSyncPackageVector3(int watcherId, NSyncComponentId componentId = NSyncComponentId.WATCHER, byte componentOrder = 0)
        {
            this.Header = new NSyncHeader(watcherId, NSyncDataType.VECTOR3, componentId, componentOrder);
        }

        protected override byte[] convertValueToBytes()
        {
            return NUtils.merge(BitConverter.GetBytes(Value.x), BitConverter.GetBytes(Value.y), BitConverter.GetBytes(Value.z));
        }

        protected override Vector3 reverseBytesToValue(byte[] data)
        {
            int offset = 0;
            var x = BitConverter.ToSingle(data, offset); offset += 4;
            var y = BitConverter.ToSingle(data, offset); offset += 4;
            var z = BitConverter.ToSingle(data, offset); offset += 4;
            return new Vector3(x, y, z);
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension.NSync.V2
{
    internal class NSyncString : AbsNSyncComponent<ComponentString>
    {
        public event Action<string> onStringChangedCallback;
        public Func<string,(bool, string)> onTrackedFunc;
        protected override void onChanged(ComponentString value)
        {
            onStringChangedCallback?.Invoke(value);
        }

        protected override (bool, ComponentString) track(ComponentString comData)
        {
            return onTrackedFunc.Invoke(comData);
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension.NSync.V2
{
    internal class NSyncPoint : AbsNSyncComponent<ComponentPoints>
    {
        public event Action<Vector3[]> onPointsChangedCallback;
        public Func<Vector3[], (bool, Vector3[])> onTrackedFunc;
        protected override void onChanged(ComponentPoints value)
        {
            onPointsChangedCallback?.Invoke(value);
        }

        protected override (bool, ComponentPoints) track(ComponentPoints comData)
        {
            return onTrackedFunc.Invoke(comData);
        }
    }
}
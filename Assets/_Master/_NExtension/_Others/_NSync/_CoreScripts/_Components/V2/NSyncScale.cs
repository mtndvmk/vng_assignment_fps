using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension.NSync.V2
{
    internal class NSyncScale : AbsNSyncComponent<ComponentVector3>
    {
        protected override void onChanged(ComponentVector3 value)
        {
            transform.localScale = value;
        }

        protected override (bool, ComponentVector3) track(ComponentVector3 comData)
        {
            return (transform.localScale == comData, transform.localScale);
        }
    }
}
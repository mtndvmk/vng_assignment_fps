using NExtension.NUtilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension.NSync.V2
{
    internal class NSyncPosition : AbsNSyncComponent<ComponentVector3>
    {
        [SerializeField] private bool m_IsWorldSpace;
        protected override void onChanged(ComponentVector3 position)
        {
            if (m_IsWorldSpace)
            {
                transform.position = position;
            }
            else
            {
                transform.localPosition = position;
            }
        }

        protected override (bool, ComponentVector3) track(ComponentVector3 comData)
        {
            var pos = m_IsWorldSpace ? transform.position : transform.localPosition;
            return (pos == comData.value, pos);
        }
    }
}
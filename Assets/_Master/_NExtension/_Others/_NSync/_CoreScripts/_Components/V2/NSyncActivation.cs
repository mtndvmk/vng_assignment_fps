using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension.NSync.V2
{
    internal class NSyncActivation : AbsNSyncComponent<ComponentBoolean>
    {
        protected override void onChanged(ComponentBoolean value)
        {
            gameObject.SetActive(value);
        }

        protected override (bool, ComponentBoolean) track(ComponentBoolean comData)
        {
            return (gameObject.activeSelf == comData, gameObject.activeSelf);
        }
    }
}
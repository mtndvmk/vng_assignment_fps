using NExtension.NUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace NExtension.NSync.V2
{
    internal interface IComponentData
    {
        byte[] getBytes();
        IComponentData setBytes(byte[] data);
    }
    internal struct ComponentBoolean : IComponentData
    {
        public bool value;
        public byte[] getBytes()
        {
            return BitConverter.GetBytes(value);
        }
        public IComponentData setBytes(byte[] data)
        {
            value = BitConverter.ToBoolean(data, 0);
            return this;
        }
        public static implicit operator ComponentBoolean(bool b)
        {
            return new ComponentBoolean() { value = b };
        }
        public static implicit operator bool(ComponentBoolean b)
        {
            return b.value;
        }
    }
    internal struct ComponentVector3 : IComponentData
    {
        public Vector3 value;

        public byte[] getBytes()
        {
            return value.vector3ToBytes();
        }

        public IComponentData setBytes(byte[] data)
        {
            value = NUtils.bytesToVector3(data);
            return this;
        }
        public static implicit operator ComponentVector3(Vector3 v3)
        {
            return new ComponentVector3() { value = v3 };
        }
        public static implicit operator Vector3(ComponentVector3 v3)
        {
            return v3.value;
        }
    }
    internal struct ComponentString : IComponentData
    {
        public String value;

        public byte[] getBytes()
        {
            return Encoding.Unicode.GetBytes(value);
        }

        public IComponentData setBytes(byte[] data)
        {
            value = Encoding.Unicode.GetString(data);
            return this;
        }
        public static implicit operator ComponentString(string str)
        {
            return new ComponentString() { value = str };
        }
        public static implicit operator string(ComponentString com)
        {
            return com.value;
        }
    }
    internal struct ComponentPoints : IComponentData
    {
        public Vector3[] values;

        public byte[] getBytes()
        {
            var data = new byte[0];
            foreach (var v in values)
            {
                data = data.mergeTo(v.vector3ToBytes());
            }
            return data;
        }

        public IComponentData setBytes(byte[] data)
        {
            values = new Vector3[data.Length / 12];
            int offset = 0;
            while (offset < data.Length)
            {
                values[offset / 12] = NUtils.bytesToVector3(data, offset); offset += 12;
            }
            return this;
        }
        public static implicit operator ComponentPoints(Vector3[] v3)
        {
            return new ComponentPoints() { values = v3 };
        }
        public static implicit operator Vector3[](ComponentPoints v3)
        {
            return v3.values;
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension.NSync.V2
{
    internal class NSyncRotation : AbsNSyncComponent<ComponentVector3>
    {
        [SerializeField] private bool m_IsWorldSpace;
        protected override void onChanged(ComponentVector3 value)
        {
            if (m_IsWorldSpace)
            {
                transform.eulerAngles = value;
            }
            else
            {
                transform.localEulerAngles = value;
            }
        }

        protected override (bool, ComponentVector3) track(ComponentVector3 comData)
        {
            var angles = m_IsWorldSpace ? transform.eulerAngles : transform.localEulerAngles;
            return (angles == comData, angles);
        }
    }
}
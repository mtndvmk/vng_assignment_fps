using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension.NSync
{
    public class NSyncTransform : AbsNSyncComponent
    {
        [Space(10)]
        [SerializeField] private bool m_IsSyncPosition = true;
        [SerializeField] private bool m_IsSyncRotation = true;
        [SerializeField] private bool m_IsSyncScale = true;
        [SerializeField] private bool m_IsWorldSpace; 
        private NWatcherComponent<Vector3> m_SyncPositionComponent;
        private NWatcherComponent<Vector3> m_SyncEulerAngleComponent;
        private NWatcherComponent<Vector3> m_SyncScaleComponent;
        protected override void onDestroy()
        {
            if (m_IsSyncPosition)
            {
                unregister(m_SyncPositionComponent);
            }
            if (m_IsSyncRotation)
            {
                unregister(m_SyncEulerAngleComponent);
            }
            if (m_IsSyncScale)
            {
                unregister(m_SyncScaleComponent);
            }
        }

        protected override void onInit()
        {
            if (m_IsSyncPosition)
            {
                NSyncPackageVector3 posPackage = new NSyncPackageVector3(MyWatcher.WatcherId, NSyncComponentId.NSYNC_TRANSFORM, 0);
                m_SyncPositionComponent = new NWatcherComponent<Vector3>(posPackage).setCompareFunc(compareTrackPosition).whenReceivedValue(reverseTrackPositionData);
                register(m_SyncPositionComponent);

            }
            if (m_IsSyncRotation)
            {
                NSyncPackageVector3 rotPackage = new NSyncPackageVector3(MyWatcher.WatcherId, NSyncComponentId.NSYNC_TRANSFORM, 1);
                m_SyncEulerAngleComponent = new NWatcherComponent<Vector3>(rotPackage).setCompareFunc(compareTrackEulerAngles).whenReceivedValue(reverseTrackAnglesData);
                register(m_SyncEulerAngleComponent);

            }
            if (m_IsSyncScale)
            {
                NSyncPackageVector3 scalePackage = new NSyncPackageVector3(MyWatcher.WatcherId, NSyncComponentId.NSYNC_TRANSFORM, 2);
                m_SyncScaleComponent = new NWatcherComponent<Vector3>(scalePackage).setCompareFunc(compareTrackScale).whenReceivedValue(reverseTrackScaleData);
                register(m_SyncScaleComponent);
            }
        }
        public override int getHashCode()
        {
            float hashCode = 0;
            if (m_IsSyncPosition)
            {
                hashCode += m_SyncPositionComponent.Value.magnitude;
            }
            if (m_IsSyncRotation)
            {
                hashCode += m_SyncEulerAngleComponent.Value.magnitude;
            }
            if (m_IsSyncScale)
            {
                hashCode += m_SyncScaleComponent.Value.magnitude;
            }
            return BitConverter.ToInt32(BitConverter.GetBytes(hashCode), 0);
        }
        private (bool, Vector3) compareTrackPosition(Vector3 inData)
        {
            var pos = m_IsWorldSpace ? transform.position : transform.localPosition;
            return (pos == inData, pos);
        }
        private (bool, Vector3) compareTrackEulerAngles(Vector3 inData)
        {
            var angles = m_IsWorldSpace ? transform.eulerAngles : transform.localEulerAngles;
            return (angles == inData, angles);
        }
        private (bool, Vector3) compareTrackScale(Vector3 inData)
        {
            var scale = transform.localScale;
            return (scale == inData, scale);
        }
        private void reverseTrackPositionData(Vector3 pos)
        {
            if (m_IsWorldSpace)
            {
                transform.position = pos;
            }
            else
            {
                transform.localPosition = pos;
            }
        }
        private void reverseTrackAnglesData(Vector3 angles)
        {
            if (m_IsWorldSpace)
            {
                transform.eulerAngles = angles;
            }
            else
            {
                transform.localEulerAngles = angles;
            }
        }
        private void reverseTrackScaleData(Vector3 scale)
        {
            transform.localScale = scale;
        }
    }
}
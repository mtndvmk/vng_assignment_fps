using NExtension.NUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension.NSync
{
    [RequireComponent(typeof(NSyncWatcher))]
    public abstract class AbsNSyncComponent : MonoBehaviour
    {
        [SerializeField] protected bool m_IsSender;
        [SerializeField] protected bool m_IsListener;

        protected List<NWatcherComponent> m_MyNSyncPackages;

        public NSyncWatcher MyWatcher { get; protected set; }
        public bool IsInitialized { get; protected set; }
        public event Action<NSyncPackage> onNewSenderSyncPackageEvent;

        public void init()
        {
            if (!IsInitialized)
            {
                MyWatcher = GetComponent<NSyncWatcher>();
                m_MyNSyncPackages = new List<NWatcherComponent>();
                onInit();
                IsInitialized = true;
            }
        }
        public void startSender(bool isStart)
        {
            m_IsSender = isStart;
        }
        public void startListener(bool isStart)
        {
            m_IsListener = isStart;
        }
        public void trackChangeAtSender()
        {
            if (m_IsSender)
            {
                foreach (var p in m_MyNSyncPackages)
                {
                    p.trackChangeAtSender();
                }
            }
        }
        public void forceSync()
        {
            if (m_IsSender)
            {
                foreach (var syncPackage in m_MyNSyncPackages)
                {
                    syncPackage.forceSync();
                }
            }
        }

        public void forceSyncAtSuper()
        {
            if (MyWatcher.LocalProfile.isSuper)
            {
                foreach (var syncPackage in m_MyNSyncPackages)
                {
                    syncPackage.forceSync();
                }
            }
        }


        protected void register(params NWatcherComponent[] watcherComponents)
        {
            foreach (var w in watcherComponents)
            {
                m_MyNSyncPackages.Add(w);
                w.onValueChangedAtSender += onNSyncPackageChangedAtSenderCallback;
            }
        }
        protected void unregister(NWatcherComponent watcherComponent)
        {
            m_MyNSyncPackages.Remove(watcherComponent);
        }
        protected void OnDestroy()
        {
            if (IsInitialized)
            {
                onDestroy();
            }
        }
        protected void onWatcherIdChangedCallback(int newWatcherId) 
        {
            foreach (var p in m_MyNSyncPackages)
            {
                p.MySyncPackage.Header.changeWatcherId(newWatcherId);
            }
        }
        protected void onNSyncPackageChangedAtSenderCallback(NSyncPackage syncPackage)
        {
            if (m_IsSender)
            {
                onNewSenderSyncPackageEvent?.Invoke(syncPackage);
            }
        }
        public bool updateNSyncPackage(NSyncPackage syncPackage)
        {
            if (m_IsListener)
            {
                var header = syncPackage.Header;
                foreach (var w in m_MyNSyncPackages)
                {
                    if (header.isSameAndNewer(w.MySyncPackage.Header))
                    {
                        w.MySyncPackage.Header.set(header);
                        w.setDataFrom(syncPackage);
                        return true;
                    }
                }
            }
            return false;
        }
        
        protected abstract void onInit();
        protected abstract void onDestroy();
        public abstract int getHashCode();
    }

    public class NWatcherComponent<T> : NWatcherComponent
    {
        private NSyncPackage<T> m_MySyncPackage;
        private Action<T> m_OnReceivedValueCallback;

        private Func<T, (bool,T)> m_CompareFunc;

        public NWatcherComponent(NSyncPackage<T> syncPackage)
        {
            m_MySyncPackage = syncPackage;
        }
        public override event Action<NSyncPackage> onValueChangedAtSender;
        public override NSyncPackage MySyncPackage { get => m_MySyncPackage; }
        public override void trackChangeAtSender()
        {
            var v = m_CompareFunc(m_MySyncPackage.Value);
            if (!v.Item1)
            {
                m_MySyncPackage.setValue(v.Item2);
                onValueChangedAtSender?.Invoke(m_MySyncPackage);
            }
        }
        public override void forceSync()
        {
            if (m_MySyncPackage.Data != null)
            {
                onValueChangedAtSender?.Invoke(m_MySyncPackage);
            }
        }
        public override void setDataFrom(NSyncPackage syncPackage)
        {
            MySyncPackage.setDataFrom(syncPackage);
            m_OnReceivedValueCallback?.Invoke(m_MySyncPackage.Value);
        }
        public NWatcherComponent<T> setCompareFunc(Func<T, (bool, T)> compareFunc)
        {
            m_CompareFunc = compareFunc;
            return this;
        }
        public NWatcherComponent<T> whenReceivedValue(Action<T> onReceivedValueCallback)
        {
            m_OnReceivedValueCallback = onReceivedValueCallback;
            return this;
        }
        public T Value => m_MySyncPackage.Value;
    }
    public abstract class NWatcherComponent
    {
        public abstract void trackChangeAtSender();
        public abstract void forceSync(); 
        public abstract void setDataFrom(NSyncPackage syncPackage);
        public virtual NSyncPackage MySyncPackage { get;}
        public virtual event Action<NSyncPackage> onValueChangedAtSender;
    }
}

namespace NExtension.NSync.V2
{
    internal abstract class NComponent
    {
        protected NSyncPackageByte m_SyncPackage;
        public NSyncPackage ComponentPackage => m_SyncPackage;
        public abstract void trackChangeAtSender();
        public abstract void forceSync();
        public abstract IComponentData getComponentData();
    }
    internal class NComponent<T> : NComponent where T : IComponentData
    {
        private T m_ComponentData;
        private Func<T,(bool,T)> m_TrackFunc;
        private Action<NSyncPackage,T> m_OnChanged;
        public NComponent(int watcherId, NSyncComponentId componentId, byte order, Func<T, (bool,T)> func, Action<NSyncPackage,T> onChanged)
        {
            m_SyncPackage = new NSyncPackageByte(watcherId, componentId, order);
            m_TrackFunc = func;
            m_OnChanged = onChanged;
        }
        public override void trackChangeAtSender()
        {
            var isEqual = m_TrackFunc.Invoke(m_ComponentData);
            if (!isEqual.Item1)
            {
                m_ComponentData = isEqual.Item2;
                m_SyncPackage.setValue(m_ComponentData.getBytes());
                forceSync();
            }
        }
        public override void forceSync()
        {
            if (m_SyncPackage.Data != null)
            {
                m_OnChanged?.Invoke(m_SyncPackage, m_ComponentData);
            }
        }
        public override IComponentData getComponentData()
        {
            return m_ComponentData;
        }
        public void setComponentData(T data)
        {
            m_SyncPackage.setValue(data.getBytes());
        }
    }
    [RequireComponent(typeof(NSyncWatcher))]
    internal abstract class AbsNSyncComponent : MonoBehaviour
    {
        [SerializeField] protected bool m_IsSender;
        [SerializeField] protected bool m_IsListener;

        internal NComponent m_NComponent;
        public NSyncWatcher NWatcher { get; protected set; }
        public bool IsInitialized { get; protected set; }

        public virtual event Action<NSyncPackage> onComponentChangedCallback;

        public abstract void init(byte order);
        public void startSender(bool isStart)
        {
            m_IsSender = isStart;
        }
        public void startListener(bool isStart)
        {
            m_IsListener = isStart;
        }
        public void trackChangeAtSender()
        {
            if (m_IsSender)
            {
                m_NComponent.trackChangeAtSender();
            }
        }
        public void forceSync()
        {
            if (m_IsSender)
            {
                m_NComponent.forceSync();
            }
        }
        public void forceSyncAtSupper()
        {
            if (NWatcher.LocalProfile.isSuper)
            {
                m_NComponent.forceSync();
            }
        }
        public abstract bool updateSyncComponent(NSyncPackage package);
    }
    internal abstract class AbsNSyncComponent<T> : AbsNSyncComponent where T : IComponentData
    {
        public override event Action<NSyncPackage> onComponentChangedCallback;
        public sealed override void init(byte order)
        {
            if (!IsInitialized)
            {
                NWatcher = GetComponent<NSyncWatcher>();
                m_NComponent = new NComponent<T>(NWatcher.WatcherId, NSyncComponentId.OTHER, order, track, onComponentChanged);
                IsInitialized = true;
                onInit();
            }
        }
        public sealed override bool updateSyncComponent(NSyncPackage package)
        {
            if (m_IsListener)
            {
                if (package.Header.isSameAndNewer(m_NComponent.ComponentPackage.Header))
                {
                    m_NComponent.ComponentPackage.from(package);
                    var com = m_NComponent.getComponentData().setBytes(package.Data);
                    onChanged((T)com);
                    return true;
                }
            }
            return false;
        }
        protected void OnDestroy()
        {
            if (IsInitialized)
            {
                IsInitialized = false;
                onDestroy();
            }
        }
        protected void onComponentChanged(NSyncPackage package, T componentData)
        {
            onComponentChangedCallback?.Invoke(package);    
        }

        protected virtual void onInit()
        {

        }
        protected virtual void onDestroy()
        {

        }

        protected abstract (bool,T) track(T comData);
        protected abstract void onChanged(T value);
    }
}
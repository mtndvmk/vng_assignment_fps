using NExtension.NUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension.NSync
{
    [RequireComponent(typeof(LineRenderer))]
    public class NSyncLineRenderer : AbsNSyncComponent
    {
        private const int PRE_POINT_LENGTH = 4;
        private const int SHAPE_DATA_LENGTH = 16;

        [Space(10)]
        [SerializeField] private int m_MaxLastTrackedPoint = 20;

        private LineRenderer m_LineRenderer;
        private NWatcherComponent<byte[]> m_PointWatcher;
        private NWatcherComponent<byte[]> m_ShapeWatcher;
        protected override void onDestroy()
        {
            unregister(m_PointWatcher);
            unregister(m_ShapeWatcher);
        }

        protected override void onInit()
        {
            m_LineRenderer = GetComponent<LineRenderer>();
            var pointPackage = new NSyncPackageByte(MyWatcher.WatcherId, NSyncComponentId.NSYNC_LINE_RENDERER, 0);
            var shapePackage = new NSyncPackageByte(MyWatcher.WatcherId, NSyncComponentId.NSYNC_LINE_RENDERER, 1);
            m_PointWatcher = new NWatcherComponent<byte[]>(pointPackage).setCompareFunc(compareTrackPointData).whenReceivedValue(reverseTrackPointData);
            m_ShapeWatcher = new NWatcherComponent<byte[]>(shapePackage).setCompareFunc(compareTrackShapeData).whenReceivedValue(reverseTrackShapeData);
            register(m_PointWatcher);
            register(m_ShapeWatcher);
        }
        public override int getHashCode()
        {
            int hashCode = m_PointWatcher.Value.Length;
            int offset = 0;
            while (offset + 4 < m_ShapeWatcher.Value.Length)
            {
                hashCode += BitConverter.ToInt32(m_ShapeWatcher.Value, offset);
            }
            return hashCode;
        }
        private (bool,byte[]) compareTrackPointData(byte[] inData)
        {
            var trackData = getTrackPointData();
            var b = trackData.compareTo(inData);
            return (b, trackData);
        }
        private (bool, byte[]) compareTrackShapeData(byte[] inData)
        {
            var trackData = getTrackShapeData();
            var b = trackData.compareTo(inData);
            return (b, trackData);
        }

        private byte[] getTrackPointData()
        {
            byte[] getLinePrePoint()
            {
                byte useWorldSpace = m_LineRenderer.useWorldSpace ? (byte)1 : (byte)0;
                byte[] prePoint = BitConverter.GetBytes(m_LineRenderer.positionCount);
                prePoint[prePoint.Length - 1] &= 0b11111110;
                prePoint[prePoint.Length - 1] |= useWorldSpace;

                return prePoint;
            }
            byte[] getTrackPointBody()
            {
                var startPoint = m_LineRenderer.positionCount - m_MaxLastTrackedPoint;
                var pointTrackCount = m_MaxLastTrackedPoint;
                if (startPoint < 0)
                {
                    startPoint = 0;
                    pointTrackCount = m_LineRenderer.positionCount;
                }

                var bodyData = BitConverter.GetBytes(startPoint);
                bodyData = bodyData.mergeTo(BitConverter.GetBytes(pointTrackCount));
                for (int i = startPoint; i < startPoint + pointTrackCount; i++)
                {
                    var pointByte = m_LineRenderer.GetPosition(i).vector3ToBytes();
                    bodyData = bodyData.mergeTo(pointByte);
                }

                return bodyData;
            }
            var prePoint = getLinePrePoint();
            var trackBody = getTrackPointBody();
            var trackData = NUtils.merge(prePoint, trackBody);
            return trackData;
        }
        private void reverseTrackPointData(byte[] trackData)
        {
            void reversePrePoint(byte[] prePoint, out bool useWorldSpace, out int lineLength)
            {
                var useWorldSpaceByte = prePoint[prePoint.Length - 1] & 0b00000001;
                prePoint[prePoint.Length - 1] &= 0b11111110;
                lineLength = BitConverter.ToInt32(prePoint, 0);
                useWorldSpace = useWorldSpaceByte == 1;
            }

            var prePoint = NUtils.getBlock(trackData, 0, PRE_POINT_LENGTH);
            var bodyData = NUtils.getBlock(trackData, PRE_POINT_LENGTH, trackData.Length - PRE_POINT_LENGTH);

            bool useWorldPoint;
            int lineLength;
            reversePrePoint(prePoint, out useWorldPoint, out lineLength);

            m_LineRenderer.positionCount = lineLength;
            m_LineRenderer.useWorldSpace = useWorldPoint;

            int offset = 0;
            var startPoint = BitConverter.ToInt32(bodyData, offset); offset += 4;
            var pointTrackCount = BitConverter.ToInt32(bodyData, offset); offset += 4;

            for (int i = startPoint; i < startPoint + pointTrackCount; i++)
            {
                var point = NUtils.bytesToVector3(NUtils.getBlock(bodyData, offset, 12)); offset += 12;
                m_LineRenderer.SetPosition(i, point);
            }
        }

        private byte[] getTrackShapeData()
        {
            var startWidthBytes = BitConverter.GetBytes(m_LineRenderer.startWidth);
            var endWidthBytes = BitConverter.GetBytes(m_LineRenderer.endWidth);
            var startColorBytes = m_LineRenderer.startColor.to4Bytes();
            var endColorBytes = m_LineRenderer.endColor.to4Bytes();

            var trackData = NUtils.merge(startWidthBytes, endWidthBytes, startColorBytes, endColorBytes);
            return trackData;
        }
        private void reverseTrackShapeData(byte[] shapeData)
        {
            int offset = 0;
            m_LineRenderer.startWidth = BitConverter.ToSingle(shapeData, offset); offset += 4;
            m_LineRenderer.endWidth = BitConverter.ToSingle(shapeData, offset); offset += 4;
            m_LineRenderer.startColor = NUtils.convert4BytesToColor(NUtils.getBlock(shapeData, offset, 4)); offset += 4;
            m_LineRenderer.endColor = NUtils.convert4BytesToColor(NUtils.getBlock(shapeData, offset, 4)); offset += 4;
        }
    }
}
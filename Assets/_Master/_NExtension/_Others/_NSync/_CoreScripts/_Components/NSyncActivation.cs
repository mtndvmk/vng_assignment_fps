using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension.NSync
{
    public class NSyncActivation : AbsNSyncComponent
    {
        private NWatcherComponent<bool> m_NSyncActivationPackage;
        protected override void onInit()
        {
            var activationPackage = new NSyncPackageBool(MyWatcher.WatcherId, NSyncComponentId.NSYNC_ACTIVATION);
            m_NSyncActivationPackage = new NWatcherComponent<bool>(activationPackage).setCompareFunc(trackActivationData).whenReceivedValue(reverseTrackActivation);
            register(m_NSyncActivationPackage);
        }

        private (bool, bool) trackActivationData(bool inData)
        {
            return (gameObject.activeSelf == inData, gameObject.activeSelf);
        }
        private void reverseTrackActivation(bool isActive)
        {
            gameObject.SetActive(isActive);
        }

        protected override void onDestroy()
        {
            unregister(m_NSyncActivationPackage);
        }
        public override int getHashCode()
        {
            return m_NSyncActivationPackage.MySyncPackage.Header.CompareId + (m_NSyncActivationPackage.Value ? 1 : 0);
        }
    }
}
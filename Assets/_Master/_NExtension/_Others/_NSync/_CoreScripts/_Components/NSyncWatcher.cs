using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension.NSync {
    public class NSyncWatcher : MonoBehaviour
    {
        [SerializeField] private int m_WatcherId;
        [SerializeField] private bool m_ForceSyncSuperWhenNewConnected = true;
        
        private AbsNSyncComponent[] m_NSyncComponents;
        private V2.AbsNSyncComponent[] m_V2SyncComponents;
        private NSyncProfile m_LocalProfile;
        
        public int WatcherId => m_WatcherId;
        public NSyncProfile LocalProfile => m_LocalProfile;
        public bool IsInitialized { get; private set; }

        public event Action<NSyncPackage> onNewSenderSyncPackageEvent;

        private void OnValidate()
        {
            if (m_WatcherId == 0)
            {
                generateId();
            }
        }

        public void init(NSyncProfile profile)
        {
            if (!IsInitialized)
            {
                if (m_WatcherId == 0)
                {
                    generateId();
                }
                m_NSyncComponents = GetComponents<AbsNSyncComponent>();
                foreach (var c in m_NSyncComponents)
                {
                    watchComponent(c);
                }
                m_V2SyncComponents = GetComponents<V2.AbsNSyncComponent>();
                for (byte i = 0; i < m_V2SyncComponents.Length; i++)
                {
                    var c = m_V2SyncComponents[i];
                    c.init(i);
                    c.onComponentChangedCallback += onNewSenderSyncPackageCallback;
                }
                m_LocalProfile = profile;
                IsInitialized = true;
            }
        }
        public void startSenderAtAll(bool isStart)
        {
            foreach (var c in m_NSyncComponents)
            {
                c.startSender(isStart);
            }
            foreach (var c in m_V2SyncComponents)
            {
                c.startSender(isStart);
            }
        }
        public void startListenerAtAll(bool isStart)
        {
            foreach (var c in m_NSyncComponents)
            {
                c.startListener(isStart);
            }
            foreach (var c in m_V2SyncComponents)
            {
                c.startListener(isStart);
            }
        }
        public void trackChangeAtSender()
        {
            foreach (var c in m_NSyncComponents)
            {
                c.trackChangeAtSender();
            }
            for (byte i = 0; i < m_V2SyncComponents.Length; i++)
            {
                var c = m_V2SyncComponents[i];
                c.trackChangeAtSender();
            }
        }
        public void setWatcherId(int watcherId)
        {
            m_WatcherId = watcherId;
        }
        public void forceSync()
        {
            foreach (var c in m_NSyncComponents)
            {
                c.forceSync();
            }
            foreach (var c in m_V2SyncComponents)
            {
                c.forceSync();
            }
        }
        public void forceSyncAtSuper()
        {
            foreach (var c in m_NSyncComponents)
            {
                c.forceSyncAtSuper();
            }
            foreach (var c in m_V2SyncComponents)
            {
                c.forceSyncAtSupper();
            }
        }

        public void forceSyncAtSupperWhenNewConnected()
        {
            if (m_ForceSyncSuperWhenNewConnected)
            {
                forceSyncAtSuper();
            }
        }

        public int getHashCode()
        {
            int hashCode = 0;
            foreach (var w in m_NSyncComponents)
            {
                hashCode |= w.getHashCode();
            }
            return hashCode;
        }
        public void receiveNSyncPackage(NSyncPackage package)
        {
            foreach (var c in m_NSyncComponents)
            {
                if (c.updateNSyncPackage(package)) break;
            }
            for (byte i = 0; i < m_V2SyncComponents.Length; i++)
            {
                var c = m_V2SyncComponents[i];
                if (c.updateSyncComponent(package)) break;
            }
        }

        [ContextMenu("Re-generate watcher id")]
        private void generateId()
        {
            setWatcherId(IdGenerator.DHM16x6013.generateId(0));
        }
        private void watchComponent(AbsNSyncComponent syncComponent)
        {
            syncComponent.init();
            syncComponent.onNewSenderSyncPackageEvent += onNewSenderSyncPackageCallback;
        }
        private void onNewSenderSyncPackageCallback(NSyncPackage package)
        {
            onNewSenderSyncPackageEvent?.Invoke(package);
        }

    }
}
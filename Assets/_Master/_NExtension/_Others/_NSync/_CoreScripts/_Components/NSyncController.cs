using NExtension.NUtilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension.NSync
{
    public class NSyncController : MonoBehaviour
    {
        private const int MAX_SIZE = 4096;

        private enum NSYNCCODE
        {
            REQ_ORDER_ID_CODE,
            SYNC_CONNECTED_DATA,
            REQ_SESSION_DATA_CODE,
            SYNC_CODE,
            SEND_SESSION_DATA_CODE
        }
        private class NSenderHeader
        {
            public const int HEADER_SIZE = 8;
            public int uniqueId;
            public byte[] receiveMask;
            public NSenderHeader(int uniqueId, byte[] receiveMask)
            {
                this.uniqueId = uniqueId;
                this.receiveMask = receiveMask;
            }
            public byte[] toBytes()
            {
                return NUtils.merge(BitConverter.GetBytes(uniqueId), receiveMask);
            }
            public static NSenderHeader fromBytes(byte[] inData, int offset)
            {
                int uniqueId = BitConverter.ToInt32(inData, offset); offset += 4;
                byte[] receiveMask = NUtils.getBlock(inData, offset, ReceiveMask.MASK_SIZE); offset += ReceiveMask.MASK_SIZE;
                return new NSenderHeader(uniqueId, receiveMask);
            }
        }

        [SerializeField] private List<NSyncWatcher> m_StartWatchers = new List<NSyncWatcher>();

        private Dictionary<int, NSyncWatcher> m_WatcherTable = new Dictionary<int, NSyncWatcher>();
        private Dictionary<int, NSplitPackage> m_TempSplitPackage = new Dictionary<int, NSplitPackage>();

        private Queue<NSyncPackage> m_SyncPackageQueue = new Queue<NSyncPackage>();

        public bool IsInitialized { get; private set; }
        public bool IsSuperClient => NSyncDataManager.LocalProfile.isSuper;
        public bool IsOrdered => IsInitialized && NSyncDataManager.LocalProfile.order != -1;
        public long DataHashCode => getDataHashCode();
        public NSyncDataManager NSyncDataManager { get; private set; }
        public NSyncProfile LocalProfile => NSyncDataManager.LocalProfile;
        
        public event Action<byte[]> onOutputDataEvent;
        public event Action<NSyncProfile> onLocalProfileUpdatedCallback;
        public event Action onCompleteConnectedEvent;
        public event Action<NSyncProfile> onNSyncRequestSessionEvent;

        [ContextMenu("Update Start Watcher list")]
        public void updateStartWatcher()
        {
            m_StartWatchers = new List<NSyncWatcher>(GameObject.FindObjectsOfType<NSyncWatcher>(true));
        }

        protected void LateUpdate()
        {
            foreach (var w in m_WatcherTable.Values)
            {
                w.trackChangeAtSender();
            }
            if (m_SyncPackageQueue.Count > 0)
            {
                var syncData = checkQueueToBytes(m_SyncPackageQueue);
                syncNow(NSYNCCODE.SYNC_CODE, ReceiveMask.ALL_NOT_ME, syncData);
            }
        }

        private byte[] createConnectedData()
        {
            var data = BitConverter.GetBytes(DataHashCode).mergeTo(NSyncDataManager.exportToBytes());
            return data;
        }

        private byte[] checkQueueToBytes(Queue<NSyncPackage> queue)
        {
            var arrayData = new byte[0];
            while (queue.Count > 0)
            {
                var b_Data = wrapDataByLength(queue.Dequeue().toBytes());
                arrayData = NUtils.merge(arrayData, b_Data);
            }
            return arrayData;
        }

        private byte[] wrapDataByLength(byte[] data)
        {
            return NUtils.merge(BitConverter.GetBytes(data.Length), data);
        }
        private void syncNow(NSYNCCODE code, byte[] receiveMask, byte[] inData)
        {
            inData = NUtils.merge(BitConverter.GetBytes((int)code), inData);
            NSplitPackage package = NSplitPackage.create(LocalProfile.order, MAX_SIZE, inData);
            var headerData = new NSenderHeader(LocalProfile.uniqueId, receiveMask).toBytes();
            for (int i = 0; i < package.Blocks.Length; i++)
            {
                var data = NUtils.merge(headerData, package.Blocks[i].toBytes());
                onOutputDataEvent?.Invoke(data);
            }
        }
        private long getDataHashCode()
        {
            long hashCode = 0;
            foreach (var w in m_WatcherTable.Values)
            {
                hashCode += w.getHashCode();
            }
            return hashCode;
        }


        public void addWatcher(NSyncWatcher watcher)
        {
            if (!m_WatcherTable.ContainsKey(watcher.WatcherId))
            {
                m_WatcherTable.Add(watcher.WatcherId, watcher);
                watcher.onNewSenderSyncPackageEvent += (pkg) =>
                {
                    if (!m_SyncPackageQueue.Contains(pkg))
                    {
                        m_SyncPackageQueue.Enqueue(pkg);
                    }
                };
            }
        }
        public void init(bool isReset = false)
        {
            if (isReset)
            {
                reset();
            }
            if (!IsInitialized)
            {
                NSyncDataManager = NSyncDataManager.initLocalProfile();
                NSyncDataManager.onProfileUpdatedEvent += (isLocalOnly) =>
                {
                    if (!isLocalOnly && IsSuperClient)
                    {
                        syncConnectedData();
                    }
                };
                NSyncDataManager.onLocalProfileUpdatedEvent += (p) => onLocalProfileUpdatedCallback?.Invoke(p);
                foreach (var w in m_StartWatchers)
                {
                    addWatcher(w);
                    w.init(LocalProfile);
                }
                IsInitialized = true;
            }
        }
        public void reset()
        {
            m_WatcherTable.Clear();
            m_TempSplitPackage.Clear();
            m_SyncPackageQueue.Clear();
            NSyncDataManager = null;
            IsInitialized = false;
        }

        public void setLocalIsSuper()
        {
            NSyncDataManager.setLocalIsSuper();
        }
        public void inputData(byte[] inData)
        {
            int offset = 0;
            var senderHeader = NSenderHeader.fromBytes(inData, offset); offset += NSenderHeader.HEADER_SIZE;

            //var uniqueId = BitConverter.ToInt32(inData, offset); offset += 4;
            //var recMask = NUtils.getBlock(inData, offset, 4); offset += 4;
            
            var orders = ReceiveMask.getOrderIds(senderHeader.receiveMask);
            if (orders.Count == 0) return;
            bool isCanReceived = false;
            if (orders[0] == ReceiveMask.ALL_ID)
            {
                isCanReceived = true;
            }
            else if (orders[0] == ReceiveMask.OTHERS_ID && !IsSuperClient)
            {
                isCanReceived = true;
            }
            else if (orders[0] == ReceiveMask.SUPER_ID && IsSuperClient)
            {
                isCanReceived = true;
            }
            else if (orders[0] == ReceiveMask.ALL_NOT_ME_ID && senderHeader.uniqueId != LocalProfile.uniqueId)
            {
                isCanReceived = true;
            }
            else if (orders.Contains(NSyncDataManager.LocalProfile.order))
            {
                isCanReceived = true;
            }

            if (isCanReceived)
            {
                var data = NUtils.getBlock(inData, offset, inData.Length - offset);
                var block = NSplitBlock.fromBytes(data);
                whenReceivedSyncSplitBlock(senderHeader, block);
            }
        }

        public NSyncProfile disconnectTo(int uniqueId)
        {
            if (NSyncDataManager != null)
            {
                return NSyncDataManager.disconnectToProfile(uniqueId, !NSyncDataManager.LocalProfile.isSuper);
            }
            return null;
        }
        #region FOR SUPER CLIENT
        private void syncConnectedData()
        {
            var data = createConnectedData();
            syncNow(NSYNCCODE.SYNC_CONNECTED_DATA, ReceiveMask.OTHERS, data);
        }
        private void sendSessionData(NSyncProfile profile)
        {
            var data = new byte[0];
            syncNow(NSYNCCODE.SEND_SESSION_DATA_CODE, ReceiveMask.getMask(profile.order), data);
            onNSyncRequestSessionEvent?.Invoke(profile);
            foreach (var w in m_WatcherTable.Values)
            {
                w.forceSyncAtSupperWhenNewConnected();
            }
        }
        #endregion

        #region FOR OTHER CLIENT
        public void connectToNSyncMaster()
        {
            if (NSyncDataManager.LocalProfile.isSuper)
            {
                return;
            }
            if (!IsOrdered)
            {
                requestOrderId();
            }
        }
        private void requestOrderId()
        {
            syncNow(NSYNCCODE.REQ_ORDER_ID_CODE, ReceiveMask.SUPER, NSyncDataManager.LocalProfile.exportToBytes());
        }
        private void requestSessionData()
        {
            syncNow(NSYNCCODE.REQ_SESSION_DATA_CODE, ReceiveMask.SUPER, NSyncDataManager.LocalProfile.exportToBytes());
        }
        #endregion

        #region Receive function
        /// <summary>
        /// call on super client
        /// </summary>
        private void whenReceivedGetIdCode(byte[] data)
        {
            var profile = new NSyncProfile().importBytes(data);
            NSyncDataManager.updateProfiles(profile, false);
        }
        /// <summary>
        /// call on super client
        /// </summary>
        private void whenReceivedReqSessionData(byte[] data)
        {
            var reqProfile = new NSyncProfile().importBytes(data);
            sendSessionData(reqProfile);
        }

        private void whenReceivedConnectedData(byte[] data)
        {
            int offset = 0;
            var hashcode = BitConverter.ToInt64(data, offset); offset += 8;
            var profileTableData = NUtils.getBlock(data, offset, data.Length - 8);

            NSyncDataManager.importBytes(profileTableData, true);
            if (!NSyncDataManager.LocalProfile.isSuper)
            {
                requestSessionData();
            }
        }
        private void whenReceivedSyncCode(byte[] data)
        {
            int offset = 0;
            while (offset < data.Length)
            {
                var len = BitConverter.ToInt32(data, offset); offset += 4;
                var packageData = NUtils.getBlock(data, offset, len); offset += len;
                var package = NSyncPackage.fromBytes(packageData);
                if (m_WatcherTable.ContainsKey(package.Header.WatcherId))
                {
                    m_WatcherTable[package.Header.WatcherId].receiveNSyncPackage(package);
                }
            }
        }
        private void whenReceivedSyncSplitBlock(NSenderHeader senderHeader, NSplitBlock block)
        {
            NSplitPackage pkg;
            if (m_TempSplitPackage.ContainsKey(block.PackageId))
            {
                pkg = m_TempSplitPackage[block.PackageId];
            }
            else
            {
                pkg = new NSplitPackage(block.PackageId, block.BlockCount);
            }

            pkg.addBlock(block);
            if (pkg.IsEntirety)
            {
                m_TempSplitPackage.Remove(pkg.Id);
                int offset = 0;
                NSYNCCODE syncCode = (NSYNCCODE)BitConverter.ToInt32(pkg.Data, offset);  offset += 4; 
                var data = NUtils.getBlock(pkg.Data, offset, pkg.Data.Length - offset);


                switch (syncCode)
                {
                    case NSYNCCODE.REQ_ORDER_ID_CODE:
                        {
                            if (!NSyncDataManager.LocalProfile.isSuper)
                            {
                                break;
                            }
                            whenReceivedGetIdCode(data);
                            break;
                        }
                    case NSYNCCODE.SYNC_CONNECTED_DATA:
                        {
                            whenReceivedConnectedData(data);
                            break;
                        }
                    case NSYNCCODE.REQ_SESSION_DATA_CODE:
                        {
                            if (!NSyncDataManager.LocalProfile.isSuper)
                            {
                                break;
                            }
                            whenReceivedReqSessionData(data);
                            break;
                        }
                    case NSYNCCODE.SYNC_CODE:
                        {
                            whenReceivedSyncCode(data);
                            break;
                        }
                    case NSYNCCODE.SEND_SESSION_DATA_CODE:
                        {
                            if (NSyncDataManager.LocalProfile.isSuper)
                            {
                                break;
                            }
                            whenReceivedSendSessionDataCode(data);
                            break;
                        }
                }
            }
        }
        private void whenReceivedSendSessionDataCode(byte[] data)
        {
            onCompleteConnectedEvent?.Invoke();
        }
        #endregion

        public NSyncProfile getProfile(int uniqueId)
        {
            return NSyncDataManager.getProfile(uniqueId);
        }
    }
    /// <summary>
    /// Size of mask is 32-bit
    /// </summary>
    public class ReceiveMask
    {
        /// <summary>
        /// Size of mask is 32-bit
        /// </summary>
        public const int MASK_SIZE = 4;
        public static byte[] getMask(params int[] orderIds)
        {
            uint mask = 0;
            uint one = 0x1;
            foreach (int id in orderIds)
            {
                mask |= one << id;
            }
            return BitConverter.GetBytes(mask);
        }
        public static byte[] ALL => BitConverter.GetBytes(0x80000000);
        public static byte[] OTHERS => BitConverter.GetBytes(0x40000000);
        public static byte[] SUPER => BitConverter.GetBytes(0x20000000);
        public static byte[] ALL_NOT_ME => BitConverter.GetBytes(0x10000000);

        public const int ALL_ID = -1;
        public const int OTHERS_ID = -2;
        public const int SUPER_ID = -3;
        public const int ALL_NOT_ME_ID = -4;

        /// <summary>
        /// <para>-1: ALL</para>
        /// <para>-2: OTHERS</para>
        /// <para>-3: SUPER</para>
        /// <para>-4: ALL_NOT_ME</para>
        /// </summary>
        /// <param name="inMask"></param>
        /// <returns></returns>
        public static List<int> getOrderIds(byte[] inMask)
        {
            var mask = BitConverter.ToUInt32(inMask, 0);
            if (inMask[3] == ALL[3]) return new List<int>() { ALL_ID };
            if (inMask[3] == OTHERS[3]) return new List<int>() { OTHERS_ID };
            if (inMask[3] == SUPER[3]) return new List<int>() { SUPER_ID };
            if (inMask[3] == ALL_NOT_ME[3]) return new List<int>() { ALL_NOT_ME_ID };
            List<int> orders = new List<int>();
            int offset = 0;
            while (offset < inMask.Length * 8)
            {
                var m = (mask >> offset) & 0x1;
                if (m > 0)
                {
                    orders.Add(offset);
                }
                offset++;
            }
            return orders;
        }
    }
}
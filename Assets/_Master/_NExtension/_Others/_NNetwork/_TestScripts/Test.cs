﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System;
using System.Threading.Tasks;
using System.Text;
using System.Threading;
using NExtension;
using UnityEngine.UI;
using NExtension.Network;

public class Test : MonoBehaviour
{

    private void Awake()
    {
        
    }

    private void Update()
    {
        if (Input.GetKeyDown("1"))
        {
            var pkg = new NPackageString("HELLO");
            //UdpNDiscovery.Instance.sayHello(true);
            UdpNDiscovery.Instance.sendToIp(pkg, "255.255.255.255", UdpNDiscovery.DEFAULT_PORT);
        }
        if (Input.GetKeyDown("2"))
        {
            NNetworkManager.Instance.startAsClient();
        }
        if (Input.GetKeyDown("3"))
        {
            NPackageString packageString = new NPackageString("Hello client(s)!");
            NNetworkManager.Instance.Server.sendToClients(packageString);
        }
        if (Input.GetKeyDown("4"))
        {
            NPackageString packageString = new NPackageString("Hello server!");
            NNetworkManager.Instance.Client.sendToServer(packageString);
        }
    }
}


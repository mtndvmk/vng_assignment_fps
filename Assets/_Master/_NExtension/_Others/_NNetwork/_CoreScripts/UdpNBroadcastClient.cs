﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using UnityEngine;

namespace NExtension.Network
{
    public class UdpNBroadcastListener : UdpNClient
    {
        public bool IsStarted { get; private set; }
        public uint MaxPackageSize { get; set; } = 1024;
        public Action<EndPoint, NPackage> m_OnReceivedPackage;
        public override void startClient(string ipAddress, int port)
        {
            stopClient();
            IsStarted = true;
            log($"Listener is started {ipAddress}:{port}");
            Task.Run(() =>
            {
                m_Socket = new Socket(AddressFamily.InterNetwork, m_SocketType, m_ProtocolType);
                IPEndPoint iep = new IPEndPoint(IPAddress.Parse(ipAddress), port);
                m_Socket.Bind(iep);
                var ep = (EndPoint)iep;
                while (IsStarted)
                {
                    byte[] data = new byte[MaxPackageSize];
                    int rcvLength = m_Socket.ReceiveFrom(data, ref ep);
                    if (IsStarted)
                    {
                        byte[] rcvData = new byte[rcvLength];
                        Buffer.BlockCopy(data, 0, rcvData, 0, rcvLength);
                        NPackage pkg = NPackage.serialize(rcvData);
                        NRunOnMainThread.Instance.addAction(() =>
                        {
                            log($"Received broadcast package - [{pkg.NPackageByte.Length}] from [{NNetworkUtils.getIPAddress(ep)}]");
                            m_OnReceivedPackage?.Invoke(ep, pkg);
                        });
                    }
                }
            });
        }

        public void stopClient()
        {
            if (IsStarted)
            {
                IsStarted = false;
                m_Socket.Close();
                log("Listener is stopped");
            }
        }

        protected override void log(string logText)
        {
            if (IsAllowLog)
            {
                Debug.Log("[NBroadcast Listener]: " + logText);
            }
        }

        public void callLog(string logText)
        {
            log(logText);
        }
    }
}

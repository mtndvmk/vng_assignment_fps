﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using UnityEngine;

namespace NExtension.Network
{
    public class UdpNBroadcastSender : UdpNServer
    {
        public UdpNBroadcastSender(string ipAdress, int port) : base (ipAdress, port) { }
        public uint MaxPackageSize { get; set; } = 1024;
        public override void startServer()
        {
            m_Socket = new Socket(AddressFamily.InterNetwork, m_SocketType, m_ProtocolType);
            m_Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);

        }

        public void sendToIp(NPackage pkg, string ip)
        {
            if (pkg.NPackageByte.Length > MaxPackageSize)
            {
                log("<color=yellow>[Warning]</color> Package is too large");
            }
            else
            {
                startServer();
                var ipEP = new IPEndPoint(IPAddress.Parse(ip), m_Port);
                m_Socket.SendTo(pkg.NPackageByte, ipEP);
                m_Socket.Close();
                log($"Sent a broadcast package - [{pkg.NPackageByte.Length}] to {ip}:{m_Port}");
            }
        }

        protected override void log(string logText)
        {
            if (IsAllowLog)
            {
                Debug.Log("[NBroadcast Sender]: " + logText);
            }
        }
    }
}

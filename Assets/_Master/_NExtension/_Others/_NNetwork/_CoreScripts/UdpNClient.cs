﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

namespace NExtension.Network
{
    public class UdpNClient : AbsNClient
    {
        public UdpNClient() : base(SocketType.Dgram, ProtocolType.Udp) { }
    }
}

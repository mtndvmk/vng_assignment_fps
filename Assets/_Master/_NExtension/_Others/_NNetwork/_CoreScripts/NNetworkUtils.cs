﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using System;


namespace NExtension
{
    public static class NNetworkParams
    {
        public const float DEFAULT_LIFETIME = 15;
    }
    public static class NNetworkUtils
    {
        public const string LOOPBACK_IP = "127.0.0.1";
        public const string BROADCAST_IP = "255.255.255.255";
        public static IPAddress getIPAddress(EndPoint endPoint)
        {
            return ((IPEndPoint)(endPoint)).Address;
        }

        public static IPAddress getFirstCardNetwork()
        {
            foreach (var ip in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip;
                }
            }
            return IPAddress.None;
        }
        public static IPAddress[] getAllCardNetwork()
        {
            List<IPAddress> iPs = new List<IPAddress>();
            foreach (var ip in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    iPs.Add(ip);
                }
            }
            return iPs.ToArray();
        }
        public static void tryDispose(this IDisposable t)
        {
            if (t != null)
            {
                t.Dispose();
            }
        }
        public static void tryDispose(this Socket socket)
        {
            if (socket != null)
            {
                if (socket.Connected)
                {
                    socket.Shutdown(SocketShutdown.Both);
                }
                socket.Close();
                socket.Dispose();
            }
        }
        public static int getFreeTcpPort()
        {
            TcpListener l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            return port;
        }
    }
    public enum WayToGetIp
    {
        CUSTOM,
        LOOPBACK,
        AUTO
    }
}
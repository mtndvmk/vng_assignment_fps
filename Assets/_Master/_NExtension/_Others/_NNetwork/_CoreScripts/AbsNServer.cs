﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using System;

namespace NExtension.Network
{

    public enum ClientStatus
    {
        NOT_CONNECT,
        CONNECTED,
        DISCONNECTED,
    }
    public class ClientData
    {
        public Socket Socket { get; private set; }
        public string IpAdress { get; private set; }
        public ClientStatus ClientStatus { get; set; }
        public int UniqueId { get; set; }
        public ClientData(Socket socket, ClientStatus clientStatus = ClientStatus.CONNECTED)
        {
            Socket = socket;
            IpAdress = NNetworkUtils.getIPAddress(socket.RemoteEndPoint).ToString();
            ClientStatus = clientStatus;
        }
        public void disconnect()
        {
            ClientStatus = ClientStatus.DISCONNECTED;
            if (Socket != null)
            {
                Socket.Shutdown(SocketShutdown.Both);
                Socket.Close();
            }
        }
        public void dispose()
        {
            ClientStatus = ClientStatus.DISCONNECTED;
            if (Socket != null)
            {
                Socket.Dispose();
            }
        }
    }
    public abstract class AbsNServer
    {
        protected IPAddress m_IpAddress;
        protected int m_Port;
        protected SocketType m_SocketType;
        protected ProtocolType m_ProtocolType;
        protected IPEndPoint m_IpEndPoint;
        protected Socket m_Socket;
        protected int m_ReadRange = 1024000;
        protected Dictionary<ClientData, float> m_SocketConnected = new Dictionary<ClientData, float>();
        protected List<ReconnectChecker> ReconnectCheckers = new List<ReconnectChecker>();
        protected List<string> m_ClientHaveConnected = new List<string>();
        protected bool m_ForceStopListeningFlag = false;
        protected bool m_StopFlag = false;

        public int ClientCount => m_SocketConnected.Count;
        public ClientData getClient(int uniqueId)
        {
            foreach(var client in m_SocketConnected.Keys)
            {
                if (client.UniqueId == uniqueId)
                {
                    return client;
                }
            }
            return null;
        }

        public bool IsAllowLog { get; set; } = true;
        public bool IsAllowErrorLog { get; set; } = true;
        public bool IsAllowWarningLog { get; set; } = true;
        public bool IsServer { get; set; }
        public bool IsLogLifeTimePackage { get; set; } = false;

        public IPAddress IpAddress => m_IpAddress;

        protected Action<ClientData, NPackage> m_OnServerReceivedData;
        protected Action m_OnServerStarted;
        protected Action m_OnStartListening;
        protected Action m_OnStopListening;
        protected Action<ClientData> m_OnClientConnected;
        protected Action<ClientData> m_OnHaveClientReconected;
        protected Action<ClientData> m_OnClientDisconnected;

        public void setOnServerReceivedDataCallback(Action<ClientData, NPackage> callback)
        {
            m_OnServerReceivedData = callback;
        }
        public void setOnServerStartedCallback(Action callback)
        {
            m_OnServerStarted = callback;
        }
        public void setOnStartListeningCallback(Action callback)
        {
            m_OnStartListening = callback;
        }
        public void setOnStopListeningCallback(Action callback)
        {
            m_OnStopListening = callback;
        }
        public void setOnClientConnectedCallback(Action<ClientData> callback)
        {
            m_OnClientConnected = callback;
        }
        public void setOnHaveClientReconnectedCallback(Action<ClientData> callback)
        {
            m_OnHaveClientReconected = callback;
        }
        public void setOnClientDisconnectedCallback(Action<ClientData> callback)
        {
            m_OnClientDisconnected = callback;
        }

        public IPEndPoint IPEndPoint => m_IpEndPoint;
        public Socket Socket => m_Socket;


        protected void startListen()
        {
            Task.Run(() =>
            {
                log($"Listening at [{IPEndPoint.Address}:{IPEndPoint.Port}]");
                m_StopFlag = false;
                NRunOnMainThread.Instance.addAction(() =>
                {
                    m_OnStartListening?.Invoke();
                });
                while (!m_ForceStopListeningFlag)
                {
                    var handler = m_Socket.Accept();
                    var clientData = new ClientData(handler);
                    var b = m_SocketConnected.ContainsKey(clientData);
                    if (b)
                    {
                        m_SocketConnected.Remove(clientData);
                    }
                    m_SocketConnected.Add(clientData, NNetworkParams.DEFAULT_LIFETIME + 1);

                    var clientIp = clientData.IpAdress;
                    if (m_ClientHaveConnected.Contains(clientIp))
                    {
                        onClientReconnected(clientData);
                    }
                    else
                    {
                        m_ClientHaveConnected.Add(clientIp);
                        onClientConnected(clientData);
                    }
                    startReceive(clientData);

                }
                NRunOnMainThread.Instance.addAction(() =>
                {
                    m_OnStopListening?.Invoke();
                });
                m_ForceStopListeningFlag = false;
            });
        }
        protected virtual void socketListen()
        {
            m_Socket.Listen(10);
        }
        protected void startReceive(ClientData client)
        {
            Task.Run(() =>
            {
                try
                {
                    while (client.Socket != null && IsServer)
                    {
                        var header = new byte[12];
                        int counter = 0;
                        int bytesRecLength = 0;
                        while (counter < 12)
                        {
                            byte[] buff = new byte[12 - counter];
                            bytesRecLength = client.Socket.Receive(buff);
                            Buffer.BlockCopy(buff, 0, header, counter, bytesRecLength);
                            counter += bytesRecLength;
                        }
                        lock (client.Socket)
                        {
                            int datalength = BitConverter.ToInt32(header, 8);
                            if (datalength == 0)
                            {
                                log("<color=red>[Error]</color>: Error when received data");
                                continue;
                            }
                            var receivedDataBytes = new byte[datalength];

                            int readLen = datalength < m_ReadRange ? datalength : m_ReadRange;
                            int bufferPointer = 0;

                            while (bufferPointer < datalength)
                            {
                                if (readLen > datalength - bufferPointer)
                                {
                                    readLen = datalength - bufferPointer;
                                }
                                var tempBytes = new byte[readLen];
                                bytesRecLength = client.Socket.Receive(tempBytes);
                                Buffer.BlockCopy(tempBytes, 0, receivedDataBytes, bufferPointer, bytesRecLength);
                                bufferPointer += bytesRecLength;
                            }
                            var totalBytes = new byte[header.Length + datalength];
                            Buffer.BlockCopy(header, 0, totalBytes, 0, header.Length);
                            Buffer.BlockCopy(receivedDataBytes, 0, totalBytes, header.Length, datalength);
                            onReceivedData(client, totalBytes);
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("ExCode: " + e.HResult);
                    if (e.HResult == -2147467259)
                    {
                        disconnect(client);
                    }
                    log("<color=red>[Error]</color>: " + e);
                }
            });
        }


        protected virtual void onReceivedData(ClientData from, byte[] bytes)
        {
            if (NRunOnMainThread.HasQuit)
            {
                return;
            }
            NRunOnMainThread.Instance.addAction(() =>
            {
                var pkg = NPackage.serialize(bytes);
                if (pkg.PackageType == NPackgeType.NETWORK_LIFETIME)
                {
                    onReceivedLifeTimePackage(from, pkg as NetworkLifeTimePackage);
                }
                else if (from.ClientStatus == ClientStatus.CONNECTED)
                {
                    m_OnServerReceivedData?.Invoke(from, pkg);
                    if (pkg.PackageType == NPackgeType.STRING)
                    {
                        log($"Received string: [{(pkg as NPackageString).StrData}][{pkg.DataLength}] from [{from.IpAdress}]");
                    }
                    else
                    {
                        log($"Received data: [{pkg.PackageType}][{pkg.DataLength}] from [{from.IpAdress}]");
                    }
                }
            });
        }
        protected virtual void onClientConnected(ClientData client)
        {
            if (NRunOnMainThread.HasQuit)
            {
                return;
            }
            NRunOnMainThread.Instance.addAction(() =>
            {
                m_OnClientConnected?.Invoke(client);
            });
            log("New client connected: " + client.IpAdress);
        }
        protected virtual void onClientReconnected(ClientData client)
        {
            if (NRunOnMainThread.HasQuit)
            {
                return;
            }
            ReconnectCheckers.Add(new ReconnectChecker(client, () =>
            {
                NRunOnMainThread.Instance.addAction(() =>
                {
                    m_OnHaveClientReconected?.Invoke(client);
                    log("Client reconnected: " + client.IpAdress);
                });
            }, ()=>
            {
                disconnect(client); 
            }));
            log("Start reconnecting with " + client.IpAdress);
        }
        protected virtual void onClientDisconnected(ClientData client)
        {
            if (NRunOnMainThread.HasQuit)
            {
                return;
            }
            if (client.ClientStatus == ClientStatus.CONNECTED)
            {
                client.ClientStatus = ClientStatus.DISCONNECTED;
                var clientIp = client.IpAdress;
                var ks = new List<ClientData>();
                ks.AddRange(m_SocketConnected.Keys);
                foreach (var c in ks)
                {
                    if (c.Socket == client.Socket)
                    {
                        NRunOnMainThread.Instance.clearQueue();
                        return;
                    }
                    //if (c.IpAdress == clientIp)
                    //{
                    //    NRunOnMainThread.Instance.clearQueue();
                    //    return;
                    //}
                }
                NRunOnMainThread.Instance.addAction(() =>
                {
                    m_OnClientDisconnected?.Invoke(client);
                });
                log($"Disconnect to client [{clientIp}]");
            }
        }


        protected AbsNServer(string ipAddress, int port, SocketType socketType, ProtocolType protocolType)
        {
            m_IpAddress = IPAddress.Parse(ipAddress);
            m_Port = port;
            m_SocketType = socketType;
            m_ProtocolType = protocolType;

            m_IpEndPoint = new IPEndPoint(m_IpAddress, m_Port);

        }
        protected virtual void log(string logText)
        {
            if (IsAllowLog)
            {
                var isErrorLog = logText.Contains("[Error]");
                var isWarningLog = logText.Contains("[Warning]");
                if (isWarningLog && IsAllowWarningLog
                    || isErrorLog && IsAllowErrorLog
                    || !isErrorLog && !isWarningLog)
                {
                    if (IsLogLifeTimePackage || !logText.ToUpper().Contains("LIFETIME"))
                    {
                        Debug.Log("[NServer]: " + logText);
                    }
                }
            }
        }
        protected void sendBytesToAllClient(byte[] buffer)
        {
            var ks = new List<ClientData>();
            ks.AddRange(m_SocketConnected.Keys);
            foreach (var handler in ks)
            {
                sendBytes(handler, buffer);
            }
        }
        protected void disconnect(ClientData client)
        {
            try
            {
                var b = m_SocketConnected.ContainsKey(client);
                if (b)
                {
                    m_SocketConnected.Remove(client);
                    onClientDisconnected(client);
                }
                client.disconnect();
                client.dispose();
            }
            catch (Exception e)
            {
                log("<color=yellow>[Warning]</color> disconnectException: " + e);
                client.dispose();
                onClientDisconnected(client);
                return;
            }
        }
        protected void sendBytes(ClientData client, byte[] buffer)
        {
            try
            {
                client.Socket.Send(buffer);
            }
            catch (Exception e)
            {
                Debug.Log("ExCode: " + e.HResult);
                if (e.HResult == -2147467259)
                {
                    disconnect(client);
                }
                log("<color=yellow>[Warning]</color> sendBytesException: " + e);
                return;
            }
        }

        public virtual void startServer()
        {
            m_Socket = new Socket(AddressFamily.InterNetwork, m_SocketType, m_ProtocolType);
            m_Socket.Bind(m_IpEndPoint);
            socketListen();
            IsServer = true;
            NRunOnMainThread.Instance.addAction(() =>
            {
                m_OnServerStarted?.Invoke();
            });
            startListen();
        }
        public void sendToClient(string ipAddress, NPackage pkg)
        {
            bool sent = false;
            var ks = new List<ClientData>();
            ks.AddRange(m_SocketConnected.Keys);
            foreach (var handler in ks)
            {
                if (handler != null && handler.Socket.Connected)
                {
                    if (handler.IpAdress == ipAddress)
                    {
                        sendBytes(handler, pkg.NPackageByte);
                        if (pkg.PackageType == NPackgeType.STRING)
                        {
                            log($"Sent data to [{handler.IpAdress}]: " + (pkg as NPackageString).StrData);
                        }
                        else if (pkg.PackageType != NPackgeType.NETWORK_LIFETIME)
                        {
                            log($"Sent data to [{handler.IpAdress}]: [{pkg.PackageType}][{pkg.DataLength}]");
                        }
                        sent = true;
                        break;
                    }
                }
                if (!sent) 
                {
                    if (handler != null)
                    {
                        log($"Not connected to [{handler.IpAdress}]");
                    }
                    else
                    {
                        log("Not connected to client");
                    }
                }
            }
        }
        public void sendToClient(ClientData client, NPackage pkg)
        {
            if (client != null && client.Socket.Connected && m_SocketConnected.ContainsKey(client))
            {
                sendBytes(client, pkg.NPackageByte);
                if (pkg.PackageType != NPackgeType.NETWORK_LIFETIME)
                {
                    if (pkg.PackageType == NPackgeType.STRING)
                    {
                        log($"Sent data to [{client.IpAdress}]: " + (pkg as NPackageString).StrData);
                    }
                    else if (pkg.PackageType != NPackgeType.NETWORK_LIFETIME)
                    {
                        log($"Sent data to [{client.IpAdress}]: [{pkg.PackageType}][{pkg.DataLength}]");
                    }
                }
            }
            else
            {
                if (client != null)
                {
                    log($"Not connected to [{client.IpAdress}]");
                }
                else
                {
                    log("Not connected to client");
                }
                }
        }
        public void sendToClients(NPackage pkg)
        {
            var ks = new List<ClientData>();
            ks.AddRange(m_SocketConnected.Keys);
            foreach (var handler in ks)
            {
                if (handler != null && handler.Socket.Connected)
                {
                    sendBytes(handler, pkg.NPackageByte);
                    if (pkg.PackageType != NPackgeType.NETWORK_LIFETIME)
                    {
                        if (pkg.PackageType == NPackgeType.STRING)
                        {
                            log($"Sent data to [{handler.IpAdress}]: " + (pkg as NPackageString).StrData);
                        }
                        else if (pkg.PackageType != NPackgeType.NETWORK_LIFETIME)
                        {
                            log($"Sent data to [{handler.IpAdress}]: [{pkg.PackageType}][{pkg.DataLength}]");
                        }
                    }
                }
                else
                {
                    if (handler != null)
                    {
                        log($"Not connected to [{handler.IpAdress}]");
                    }
                    else
                    {
                        log("Not connected to client");
                    }
                }
            }
        }
        public void stopListening()
        {
            m_ForceStopListeningFlag = true;
        }
        public void continueListening()
        {
            startListen();
        }

        public void setAllowLog(bool isAllow)
        {
            IsAllowLog = isAllow;
        }

        public void dispose()
        {
            var ks = new List<ClientData>();
            ks.AddRange(m_SocketConnected.Keys);
            foreach (var k in ks)
            {
                disconnect(k);
                k.dispose();
            }
            m_SocketConnected.Clear();
            m_ClientHaveConnected.Clear();
            ReconnectCheckers.Clear();
            m_Socket.tryDispose();
            m_StopFlag = true;
            IsServer = false;
        }

        private void startCheckClientConnection(ClientData client)
        {
            Task.Run(async () =>
            {
                while (client != null && !m_StopFlag && IsServer && client.ClientStatus == ClientStatus.CONNECTED)
                {
                    sendToClient(client, new NetworkLifeTimePackage());
                    await Task.Delay(1000);
                    var ks = new List<ClientData>();
                    ks.AddRange(m_SocketConnected.Keys);
                    foreach (var handler in ks)
                    {
                        if (handler == client)
                        {
                            m_SocketConnected[handler] -= 1;
                        }
                    }
                    foreach (var k in ks)
                    {
                        if (m_SocketConnected[k] < 0 && client == k)
                        {
                            disconnect(k);
                        }
                    }
                }
            });
        }
        private void onReceivedLifeTimePackage(ClientData k, NetworkLifeTimePackage pkg)
        {
            if (m_SocketConnected.ContainsKey(k))
            {
                if (m_SocketConnected[k] > NNetworkParams.DEFAULT_LIFETIME)
                {
                    startCheckClientConnection(k);
                }
                m_SocketConnected[k] = NNetworkParams.DEFAULT_LIFETIME;
            }
            foreach (var checker in ReconnectCheckers)
            {
                if (checker.ClientData == k)
                {
                    checker.onRefreshLifeTime();
                }
            }
            for (int i = ReconnectCheckers.Count - 1; i >= 0; i--)
            {
                if (ReconnectCheckers[i].IsReconnected)
                {
                    ReconnectCheckers.RemoveAt(i);
                }
            }
        }
    }
    public class ReconnectChecker
    {
        public const int TIME_TO_RECONNECT = 3;
        public const int TIME_TO_TIMEOUT = 10;
        public ClientData ClientData { get; set; }
        public int LifeTimeCount { get; private set; }
        public bool IsChecking { get; private set; }
        public bool IsReconnected { get; private set; }
        private Action m_ReconnectCallback;
        private Action m_ReconnectTimeout;
        private DateTime m_StartCheckTime;
        CustomTask m_CheckTask;
        public ReconnectChecker(ClientData clientData, Action reconnectCallback, Action reconnectTimeout)
        {
            ClientData = clientData;
            LifeTimeCount = 0;
            m_ReconnectCallback = reconnectCallback;
            m_ReconnectTimeout = reconnectTimeout;
            startChecker();
        }

        public void startChecker()
        {
            IsChecking = true;
            m_StartCheckTime = DateTime.Now;
            checkTimeout();
        }

        public void onRefreshLifeTime()
        {
            if (IsChecking)
            {
                LifeTimeCount++;
                if (LifeTimeCount >= TIME_TO_RECONNECT)
                {
                    IsChecking = false;
                    IsReconnected = true;
                    m_CheckTask?.stop();
                    m_ReconnectCallback?.Invoke();
                }
            }
        }

        public void checkTimeout()
        {
            m_CheckTask = new CustomTask(async (token) =>
            {
                while ((DateTime.Now - m_StartCheckTime).TotalSeconds < TIME_TO_TIMEOUT && !token.IsCancellationRequested)
                {
                    await System.Threading.Tasks.Task.Delay(1000);
                }
                if (!IsReconnected)
                {
                    m_ReconnectTimeout?.Invoke();
                }
                IsChecking = false;
            });
            m_CheckTask.start();
        }
    }
}
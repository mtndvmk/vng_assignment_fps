﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System;

namespace NExtension.Network
{
    public class TcpNServer : AbsNServer
    {
        public TcpNServer(string ipAddress, int port) : base(ipAddress, port, SocketType.Stream, ProtocolType.Tcp) { }
    }
}







﻿using NExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

namespace NExtension.Network
{
    public class NNetworkManager : AbsTcpNNetworkManager<NNetworkManager>
    {
        #region At server
        /// <summary>
        /// Server's callback
        /// </summary>
        public event Action onServer_ServerStartedEvent;
        /// <summary>
        /// Server's callback
        /// </summary>
        public event Action<ClientData> onServer_NewClientConnectedEvent;
        /// <summary>
        /// Server's callback
        /// </summary>
        public event Action<ClientData, NPackage> onServer_ReceivedDataEvent;
        /// <summary>
        /// Server's callback
        /// </summary>
        public event Action<ClientData> onServer_ClientReconnectedEvent;
        /// <summary>
        /// Server's callback
        /// </summary>
        public event Action<ClientData> onServer_ClientDisconnectedEvent;

        public event Action<string> onServer_FailedToStartServerEvent;

        protected override void onServerStarted()
        {
            base.onServerStarted();
            onServer_ServerStartedEvent?.Invoke();
        }
        protected override void onHaveNewClientConnected(ClientData client)
        {
            base.onHaveNewClientConnected(client);
            onServer_NewClientConnectedEvent?.Invoke(client);
        }
        protected override void onHaveClientReconnected(ClientData client)
        {
            base.onHaveClientReconnected(client);
            onServer_ClientReconnectedEvent?.Invoke(client);
        }
        protected override void onServerReceivedData(ClientData client, NPackage package)
        {
            base.onServerReceivedData(client, package);
            onServer_ReceivedDataEvent?.Invoke(client, package);
        }
        protected override void onClientDisconnected(ClientData client)
        {
            base.onClientDisconnected(client);
            onServer_ClientDisconnectedEvent?.Invoke(client);
        }
        protected override void onFailedToStartServer(string failedMsg)
        {
            base.onFailedToStartServer(failedMsg);
            onServer_FailedToStartServerEvent?.Invoke(failedMsg);
        }
        #endregion
        #region At client
        /// <summary>
        /// Client's callback
        /// </summary>
        public event Action onClient_TimeoutConnectToServer;
        /// <summary>
        /// Client's callback
        /// </summary>
        public event Action<NPackage> onClient_ReceivedData;
        /// <summary>
        /// Client's callback
        /// </summary>
        public event Action onClient_ConnectedToServer;
        /// <summary>
        /// Client's callback
        /// </summary>
        public event Action onClient_ReconnectedToServer;

        /// <summary>
        /// Client's callback
        /// </summary>
        public event Action onClient_DisconnectToServer;
        protected override void onTimeoutConnectToServer(Socket server)
        {
            base.onTimeoutConnectToServer(server);
            onClient_TimeoutConnectToServer?.Invoke();
        }
        protected override void onClientReceivedData(Socket server, NPackage package)
        {
            base.onClientReceivedData(server, package);
            onClient_ReceivedData?.Invoke(package);
        }
        protected override void onClientConnectedToServer(Socket server)
        {
            base.onClientConnectedToServer(server);
            onClient_ConnectedToServer?.Invoke();
        }
        protected override void onClientReconnectedToServer(Socket server)
        {
            base.onClientReconnectedToServer(server);
            onClient_ReconnectedToServer?.Invoke();
        }
        protected override void onDisconnectedToServer(Socket server)
        {
            base.onDisconnectedToServer(server);
            onClient_DisconnectToServer?.Invoke();
        }
        private void OnApplicationQuit()
        {
            if (IsServer)
            {
                Server.IsAllowErrorLog = false;
                Server.IsAllowWarningLog = false;
            }
            if (IsClient)
            {
                Client.IsAllowErrorLog = false;
                Client.IsAllowWarningLog = false;
            }
        }
        #endregion
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using NExtension.NUtilities;

public static class StringPrefix
{
    private const string DELIMITER = "#@^";
    public static string combineString(params string[] args)
    {
        string str = "";
        if (args.Length > 0)
        {
            for (int i = 0; i < args.Length - 1; i++)
            {
                str += args[i] + DELIMITER;
            }
            str += args.Last();
        }
        return str;
    }
    public static string[] splitCombineString(string str)
    {
        return str.Split(new string[] { StringPrefix.DELIMITER }, StringSplitOptions.RemoveEmptyEntries);
    }
}

public class WorkSession
{
    public const string DELIMETER = "#@!";
    public string DeviceName { get; private set; }
    public string SessionId { get; private set; }
    public WorkSession(string deviceName, string ssid)
    {
        DeviceName = deviceName;
        SessionId = ssid; 
    }
    public string toString()
    {
        return "HELLO" 
            + DELIMETER + DeviceName 
            + DELIMETER + SessionId;
    }
    public static WorkSession fromString(string str)
    {
        var strs = str.Split(new string[] { DELIMETER }, StringSplitOptions.RemoveEmptyEntries);
        if (strs.Length == 3)
        {
            return new WorkSession(strs[1], strs[2]);
        }
        return null;
    }
    public static bool isEqual(WorkSession l, WorkSession r)
    {
        if (l == null || r == null)
        {
            return false;
        }
        if (l.DeviceName != r.DeviceName)
        {
            return false;
        }
        if (l.SessionId != r.SessionId)
        {
            return false;
        }
        return true;
    }
}

//public static class ConvertUtils
//{
//    public static string boltTransformToBoltString(Transform t, bool isUseLocalParam = false)
//    {
//        if (isUseLocalParam)
//        {
//            var positionStr = t.localPosition.encodeVector3();
//            var eulerAngleStr = t.localEulerAngles.encodeVector3();
//            var scaleStr = t.localScale.encodeVector3();
//            return StringPrefix.combineString(StringPrefix.BOLT_TRANSFORM, positionStr, eulerAngleStr, scaleStr);
//        }
//        else
//        {
//            var positionStr = t.position.encodeVector3();
//            var eulerAngleStr = t.eulerAngles.encodeVector3();
//            var scaleStr = t.localScale.encodeVector3();
//            return StringPrefix.combineString(StringPrefix.BOLT_TRANSFORM, positionStr, eulerAngleStr, scaleStr);
//        }
//    }

//    public static Vector3[] boltStringToVector3(string boltString)
//    {
//        Vector3[] trans = new Vector3[3];
//        var strs = boltString.Split(new string[] { StringPrefix.DELIMITER }, StringSplitOptions.RemoveEmptyEntries);
//        if (strs.Length == 4)
//        {
//            trans[0] = NUtils.decodeToVector3(strs[1]);
//            trans[1] = NUtils.decodeToVector3(strs[2]);
//            trans[2] = NUtils.decodeToVector3(strs[3]);
//        } 
//        else if (strs.Length == 3)
//        {
//            trans[0] = NUtils.decodeToVector3(strs[0]);
//            trans[1] = NUtils.decodeToVector3(strs[1]);
//            trans[2] = NUtils.decodeToVector3(strs[2]);
//        }
//        else
//        {
//            Debug.LogError("Failed to convert bolt string to bolt transform");
//            return null;
//        }
//        return trans;
//    }
//}
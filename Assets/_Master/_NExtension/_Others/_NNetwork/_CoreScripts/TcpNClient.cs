﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

namespace NExtension.Network
{
    public class TcpNClient : AbsNClient
    {
        public TcpNClient() : base(SocketType.Stream, ProtocolType.Tcp) { }
    }
}

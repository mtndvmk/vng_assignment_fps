﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace NExtension.Network
{
    public class UdpNDiscovery : NSingleton<UdpNDiscovery>
    {
        public const string HELLO_STR = "iVRES_HELLO";
        public const string FWD_STR = "iVRES_FWD_IP";
        public const int DEFAULT_PORT = 51000;


        //[Header("Sender")]
        //[SerializeField] private int m_SenderPort = 51000;
        private const uint m_MaxPackageSize = 1024;

        [Header("Listener")]
        [SerializeField] private int m_ListenerPort = DEFAULT_PORT;
        [SerializeField] private bool m_StartListenerOnStart;
        private string m_ListenIp = "0.0.0.0";

        [Header("Others")]
        [SerializeField] private bool m_IsAllowLog;

        //private UdpNBroadcastSender m_BroadcastSender;
        private UdpNBroadcastListener m_BroadcastListener;

        public event Action<EndPoint> onReceivedHelloMsg;
        public event Action<string> onReceivedFwdContent;
        public bool Listening => m_BroadcastListener.IsStarted;
        private Func<string> funcGetFwdContent;


        protected override void onAwake()
        {
            //m_BroadcastSender = new UdpNBroadcastSender(getIp(), m_SenderPort);
            m_BroadcastListener = new UdpNBroadcastListener();

            m_BroadcastListener.MaxPackageSize = m_MaxPackageSize;

            m_BroadcastListener.setAllowLog(m_IsAllowLog);

            m_BroadcastListener.m_OnReceivedPackage = onReceivedBroadcast;

            if (m_StartListenerOnStart)
            {
                startListener();
            }
            setFwdContent(() => getIp());
        }

        private UdpNBroadcastSender createSender(int port)
        {
            var sender = new UdpNBroadcastSender(getIp(), port);
            sender.IsAllowLog = m_IsAllowLog;
            sender.MaxPackageSize = m_MaxPackageSize;
            return sender;
        }

        public void sendBroadcast(NPackage package, int port)
        {
            sendToIp(package, NNetworkUtils.BROADCAST_IP, port);
        }
        public void sayHello(int toPort)
        {
            sayHello(NNetworkUtils.BROADCAST_IP, toPort);
        }
        public void sayHello(int toPort, int fwdPort)
        {
            sayHello(NNetworkUtils.BROADCAST_IP, toPort, fwdPort);
        }
        public void sayHello(string ip, int toPort)
        {
            var pkg = new NPackageString(StringPrefix.combineString(HELLO_STR));
            sendToIp(pkg, ip, toPort);
        }
        public void sayHello(string ip, int toPort, int fwdPort)
        {
            var pkg = new NPackageString(StringPrefix.combineString(HELLO_STR, fwdPort.ToString()));
            sendToIp(pkg, ip, toPort);
        }
        public void sendToIp(NPackage package, string ip, int port)
        {
            var sender = createSender(port);
            sender.sendToIp(package, ip);
            sender.dispose();
        }
        public void startListener()
        {
            m_BroadcastListener.startClient(m_ListenIp, m_ListenerPort);
        }
        public void startListener(int port, Func<string> funcGetFwdContent = null)
        {
            m_ListenerPort = port;
            m_BroadcastListener.startClient(m_ListenIp, m_ListenerPort);
            setFwdContent(funcGetFwdContent);
        }

        protected void onReceivedBroadcast(EndPoint from, NPackage pkg)
        {
            var pkgStr = pkg as NPackageString;
            if (pkgStr == null)
            {
                return;
            }

            var content = StringPrefix.splitCombineString(pkgStr.StrData);
            if (content[0] == HELLO_STR)
            {
                var fromIp = NNetworkUtils.getIPAddress(from).ToString();
                onReceivedHelloMsg?.Invoke(from);
                if (content.Length > 1)
                {
                    if (int.TryParse(content[1], out var fromPort))
                    {
                        var sender = createSender(fromPort);
                        sender.sendToIp(new NPackageString(StringPrefix.combineString(FWD_STR, funcGetFwdContent?.Invoke())), fromIp);
                        sender.dispose();
                    }
                }
                return;
            }
            else if (content[0] == FWD_STR)
            {
                if (content.Length > 1)
                {
                    onReceivedFwdContent?.Invoke(content[1]);
                    m_BroadcastListener.callLog($"Have fwd content: {content[1]}");
                }
                return;
            }
        }
        public void stopListener()
        {
            m_BroadcastListener.stopClient();
        }
        public void setFwdContent(Func<string> funcGetFwdContent)
        {
            if (funcGetFwdContent != null)
            {
                this.funcGetFwdContent = funcGetFwdContent;
            }
        }

        private void OnDestroy()
        {
            m_BroadcastListener.stopClient();
            m_BroadcastListener.dispose();
        }

        private string getIp()
        {
            return NNetworkUtils.getFirstCardNetwork().ToString();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using System;


namespace NExtension.Network
{
    public abstract class AbsNClient
    {
        protected IPAddress m_ServerAddress;
        protected SocketType m_SocketType;
        protected ProtocolType m_ProtocolType;
        protected Socket m_Socket;
        protected int m_ReadRange = 1024000;
        protected float m_TimeoutConnecting = 10f;
        protected bool m_StopFlag = false;
        protected float m_LifeTime = NNetworkParams.DEFAULT_LIFETIME;

        public bool IsAllowLog { get; set; } = true;
        public bool IsAllowErrorLog { get; set; } = true;
        public bool IsAllowWarningLog { get; set; } = true;
        public bool IsConnectedToServer { get; private set; }
        public bool IsLogLifeTimePackage { get; set; } = false;
        public bool IsDisconnectWhenTimeout { get; set; } = true;
        public float LifeTime => m_LifeTime;
        public ClientData ClientData { get; private set; }

        public bool IsStarting { get; private set; }

        protected Action<Socket, NPackage> m_OnClientReceivedData;
        protected Action<Socket> m_OnConnectedToServer;
        protected Action<Socket> m_OnReconnectedToServer;
        protected Action<Socket> m_OnDisconnectedToServer;
        protected Action<Socket> m_OnTimeoutConnectToServerRequest;
        protected ReconnectChecker m_ReconnectChecker;

        public void setOnClientReceivedDataCallback(Action<Socket, NPackage> callback)
        {
            m_OnClientReceivedData = callback;
        }
        public void setOnConnectedToServerCallback(Action<Socket> callback)
        {
            m_OnConnectedToServer = callback;
        }
        public void setOnReconnectedToServerCallback(Action<Socket> callback)
        {
            m_OnReconnectedToServer = callback;
        }
        public void setOnDisconnectedToServerCallback(Action<Socket> callback)
        {
            m_OnDisconnectedToServer = callback;
        }
        public void setOnTimeoutConnectToServerCallback(Action<Socket> callback)
        {
            m_OnTimeoutConnectToServerRequest = callback;
        }

        public IPAddress ServerAddress => m_ServerAddress;
        public EndPoint ServerEP { get; private set; }

        private bool m_IsConnecting;
        public bool IsConnecting => m_IsConnecting;

        protected void startConnect(EndPoint serverEP)
        {
            if (m_IsConnecting)
            {
                return;
            }
            m_IsConnecting = true;
            m_StopFlag = false;
            Task.Run(async () =>
            {
                if (IsStarting) return;
                IsStarting = true;
                float countdown = 0;
                var serverIp = NNetworkUtils.getIPAddress(serverEP);
                log($"Connecting to server [{serverIp}]");
                while (countdown < m_TimeoutConnecting && !m_Socket.Connected)
                {
                    Task.Run(() =>
                    {
                        m_Socket.Connect(serverEP);
                        m_IsConnecting = false;
                        m_ServerAddress = serverIp;
                        onConnectedToServer(m_Socket, serverEP);
                        startReceive(m_Socket);
                        IsStarting = false;
                    });
                    await Task.Delay(200);
                    countdown += 0.2f;
                    //log($"Connecting left time [{(int)(m_TimeoutConnecting - countdown)}]");
                }
                if (!m_Socket.Connected)
                {
                    NRunOnMainThread.Instance.addAction(() =>
                    {
                        log("Timeout to connect to server");
                        IsStarting = false;
                        m_IsConnecting = false;
                        m_OnTimeoutConnectToServerRequest?.Invoke(m_Socket);
                    });
                }
            });
        }
        protected void startCheckConnection(Socket handler)
        {
            Task.Run(async () =>
            {
                if (!IsDisconnectWhenTimeout) return;
                m_LifeTime = NNetworkParams.DEFAULT_LIFETIME;
                while (!m_StopFlag && ClientData.ClientStatus == ClientStatus.CONNECTED)
                {
                    sendToServer(new NetworkLifeTimePackage());
                    await Task.Delay(1000);
                    m_LifeTime -= 1;
                    if (m_LifeTime < 0)
                    {
                        disconnectToServer();
                        return;
                    }
                }
            });

            //Task.Run(async () =>
            //{
            //    while (!m_StopFlag && handler != null)
            //    {
            //        bool flag = false;
            //        if (!handler.Connected)
            //        {
            //            flag = true;
            //        }
            //        while (!flag && handler != null && handler.Connected)
            //        {
            //            NNetworkUtils.disconnectChecker(handler, () =>
            //            {
            //                flag = true;
            //            });
            //            await Task.Delay(1000);
            //        }
            //        if (flag)
            //        {
            //            disconnect();
            //            break;
            //        }
            //    }
            //});
        }
        protected void startReceive(Socket handler)
        {
            Task.Run(async() =>
            {
                try
                {
                    while (!m_StopFlag && handler != null && handler.Connected)
                    {
                        var header = new byte[12];
                        int counter = 0;
                        int bytesRecLength = 0;
                        while (counter < 12)
                        {
                            byte[] buff = new byte[12 - counter];
                            bytesRecLength = handler.Receive(buff);
                            Buffer.BlockCopy(buff, 0, header, counter, bytesRecLength);
                            counter += bytesRecLength;
                        }
                        lock (handler)
                        {
                            int datalength = BitConverter.ToInt32(header, 8);
                            if (datalength == 0)
                            {
                                continue;
                            }
                            var receivedDataBytes = new byte[datalength];
                            int readLen = datalength < m_ReadRange ? datalength : m_ReadRange;
                            int bufferPointer = 0;

                            while (bufferPointer < datalength)
                            {
                                if (readLen > datalength - bufferPointer)
                                {
                                    readLen = datalength - bufferPointer;
                                }
                                var tempBytes = new byte[readLen];
                                bytesRecLength = handler.Receive(tempBytes);
                                Buffer.BlockCopy(tempBytes, 0, receivedDataBytes, bufferPointer, bytesRecLength);
                                bufferPointer += bytesRecLength;
                            }
                            var totalBytes = new byte[header.Length + datalength];
                            Buffer.BlockCopy(header, 0, totalBytes, 0, header.Length);
                            Buffer.BlockCopy(receivedDataBytes, 0, totalBytes, header.Length, datalength);
                            onReceivedData(handler, totalBytes);
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("ExCode: " + e.HResult);
                    if (e.HResult == -2147467259)
                    {
                        disconnectToServer();
                        onClientDisconnected(m_Socket);
                    }
                    log("<color=red>[Error]</color>: " + e);
                }
            });
        }

        public void setAllowLog(bool isAllow)
        {
            IsAllowLog = isAllow;
        }

        protected AbsNClient(SocketType socketType, ProtocolType protocolType)
        {
            m_SocketType = socketType;
            m_ProtocolType = protocolType;
        }
        protected void disconnect()
        {
            try
            {
                if (IsConnectedToServer)
                {
                    m_Socket.Shutdown(SocketShutdown.Both);
                    m_Socket.Close();
                    onClientDisconnected(m_Socket);
                    IsConnectedToServer = false;
                    m_StopFlag = true;
                }
            }
            catch (Exception e)
            {
                if (IsConnectedToServer)
                {
                    onClientDisconnected(m_Socket);
                    IsConnectedToServer = false;
                    m_StopFlag = true;
                }
                log("<color=red>[Error]</color>: " + e);
                return;
            }
        }
        protected void sendBytes(Socket socket, byte[] buffer)
        {
            try
            {
                socket.Send(buffer);
            }
            catch (Exception e)
            {
                log("<color=red>[Error]</color>: " + e);
                if (e.HResult == -2147467259)
                {
                    disconnectToServer();
                }
                return;
            }
        }
        protected virtual void log(string logText)
        {
            if (IsAllowLog)
            {
                var isErrorLog = logText.Contains("[Error]");
                var isWarningLog = logText.Contains("[Warning]");
                if (isWarningLog && IsAllowWarningLog
                    || isErrorLog && IsAllowErrorLog
                    || !isErrorLog && !isWarningLog)
                {
                    if (IsLogLifeTimePackage || !logText.ToUpper().Contains("LIFETIME"))
                    {
                        Debug.Log("[NClient]: " + logText);
                    }
                }
            }
        }


        protected virtual void onReceivedData(Socket handler, byte[] bytes)
        {
            if (NRunOnMainThread.HasQuit && !handler.Connected)
            {
                return;
            }
            var pkg = NPackage.serialize(bytes);
            if (pkg.PackageType == NPackgeType.NETWORK_LIFETIME)
            {
                onReceivedLifeTimePackage(pkg as NetworkLifeTimePackage);
                log("Refresh Lifetime");
            }
            else if (ClientData == null || ClientData.ClientStatus == ClientStatus.CONNECTED)
            {
                NRunOnMainThread.Instance.addAction(() =>
                {
                    if (handler != null && handler.Connected)
                    {
                        m_OnClientReceivedData?.Invoke(handler, pkg);
                        if (pkg.PackageType == NPackgeType.STRING)
                        {
                            log($"Received string: [{(pkg as NPackageString).StrData}][{pkg.DataLength}] from [{NNetworkUtils.getIPAddress(handler.RemoteEndPoint)}]");
                        }
                        else
                        {
                            log($"Received data: [{pkg.PackageType}][{pkg.DataLength}] from [{NNetworkUtils.getIPAddress(handler.RemoteEndPoint)}]");
                        }
                    }
                });
            }
            else
            {
                log($"Skip [{pkg.PackageType}][{pkg.DataLength}][{handler!=null}][{handler.Connected}][{ClientData != null}]");
            } 
        }
        protected virtual void onConnectedToServer(Socket handler, EndPoint serverEP)
        {
            if (NRunOnMainThread.HasQuit)
            {
                return;
            }
            var serverIp = NNetworkUtils.getIPAddress(handler.RemoteEndPoint).ToString();
            if (ServerEP !=null && serverIp == NNetworkUtils.getIPAddress(ServerEP).ToString())
            {
                var oldStatus = ClientData.ClientStatus;
                ClientData.dispose();
                ClientData = new ClientData(handler, oldStatus);
                sendToServer(new NetworkLifeTimePackage());
                m_ReconnectChecker = new ReconnectChecker(ClientData, () =>
                {
                    NRunOnMainThread.Instance.addAction(() =>
                    {
                        m_OnReconnectedToServer?.Invoke(handler);
                        IsConnectedToServer = true;
                        ClientData.ClientStatus = ClientStatus.CONNECTED;
                        log($"Reconnected to server [{m_ServerAddress}]");
                        startCheckConnection(m_Socket);
                        //sendConnectedToServer(handler);
                    });
                }, ()=>
                {
                    disconnect();
                    m_OnTimeoutConnectToServerRequest?.Invoke(ClientData.Socket);
                    log("Timeout to reconnect");
                });

                log($"Reconnecting to server [{m_ServerAddress}]");
            }
            else
            {
                ServerEP = serverEP;
                NRunOnMainThread.Instance.addAction(() =>
                {
                    m_OnConnectedToServer?.Invoke(handler);
                    IsConnectedToServer = true;
                    log($"Connected to server [{m_ServerAddress}]");
                    ClientData = new ClientData(handler);
                    startCheckConnection(m_Socket);
                    //sendConnectedToServer(handler);
                });
            }
        }
        protected virtual void onClientDisconnected(Socket handler)
        {
            if (NRunOnMainThread.HasQuit)
            {
                return;
            }
            if (ClientData.ClientStatus == ClientStatus.CONNECTED)
            {
                ClientData.dispose();
                ClientData.ClientStatus = ClientStatus.DISCONNECTED;
                NRunOnMainThread.Instance.addAction(() =>
                {
                    m_OnDisconnectedToServer?.Invoke(handler);
                    log($"Disconnect to server [{m_ServerAddress}]");
                });
            }
        }


        public virtual void startClient(EndPoint serverEP)
        {
            m_Socket.tryDispose();
            m_Socket = new Socket(AddressFamily.InterNetwork, m_SocketType, m_ProtocolType);
            startConnect(serverEP);
        }
        public virtual void startClient(string ipAddress, int port)
        {
            startClient(new IPEndPoint(IPAddress.Parse(ipAddress), port));
        }


        public void sendToServer(NPackage pkg)
        {
            if (m_Socket != null && m_Socket.Connected)
            {
                sendBytes(m_Socket, pkg.NPackageByte);
                if (pkg.PackageType == NPackgeType.STRING) 
                {
                    log("Send string to Server: " + (pkg as NPackageString).StrData);
                }
                else if (pkg.PackageType != NPackgeType.NETWORK_LIFETIME)
                {
                    log($"Sent data to server [{NNetworkUtils.getIPAddress(m_Socket.RemoteEndPoint)}]: [{pkg.PackageType}][{pkg.DataLength}]");
                }
            }
            else
            {
                log($"Failed to send to Server: Not connected");
            }
        }
        public void disconnectToServer()
        {
            if (IsConnectedToServer)
            {
                log("Disconnecting...");
            }
            disconnect();
        }
        public void dispose()
        {
            disconnect();
            m_Socket.tryDispose();
            m_Socket = null;
        }
        private void onReceivedLifeTimePackage(NetworkLifeTimePackage pkg)
        {
            m_LifeTime = NNetworkParams.DEFAULT_LIFETIME;
            if (m_ReconnectChecker != null)
            {
                if (!m_ReconnectChecker.IsReconnected)
                {
                    m_ReconnectChecker.onRefreshLifeTime();
                }
            }
        }
    }
}
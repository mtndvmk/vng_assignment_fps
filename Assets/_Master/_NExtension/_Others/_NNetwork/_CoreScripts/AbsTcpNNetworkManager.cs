﻿using NExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

namespace NExtension.Network
{
    public abstract class AbsTcpNNetworkManager<T> : NSingleton<T> where T : MonoBehaviour
    {
        [Header("Server")]
        [SerializeField] private string m_IpAddress = "127.0.0.1";
        [SerializeField] private int m_Port = 20000;

        [SerializeField] private bool m_IsUseDynamicPort = false;
        [SerializeField] private WayToGetIp m_WayToGetIp;
        [SerializeField] private bool m_IsStartServerOnAwake = true;
        [Header("Client")]
        [SerializeField] private bool m_IsStartClientOnAwake = true;
        [SerializeField] private bool m_IsAutoReconnectWhenDisconnect;
        [SerializeField] private bool m_IsAutoReconnectWhenTimeout;

        [Header("Others")]
        [SerializeField] private bool m_IsAlowLog = true;
        [SerializeField] private bool m_IsAllowErrorLog = false;
        [SerializeField] private bool m_IsAllowWarningLog = false;

        public bool IsServer => Server != null && Server.IsServer;
        public bool IsClient { get; private set; }
        public bool IsHost => IsServer && IsClient;

        public TcpNServer Server { get; protected set; }
        public TcpNClient Client { get; protected set; }
        public int Port { get => m_Port; set => m_Port = value; }
        public string IpAddress => m_IpAddress;

        private void OnValidate()
        {
            m_IpAddress = getIp();
        }

        protected override void onAwake()
        {
            if (m_IsStartServerOnAwake)
            {
                startAsServer();
            }
            if (m_IsStartClientOnAwake)
            {
                startAsClient();
            }
        }
        private void OnDestroy()
        {
            stopHost();
        }

        private void OnApplicationQuit()
        {
            stopHost();
        }

        private void OnDisable()
        {
            stopHost();
        }

        public void startAsServer()
        {
            if (IsServer)
            {
                Debug.Log("Server is running");
                return;
            }
            m_IpAddress = getIp();

            if (m_IsUseDynamicPort)
            {
                m_Port = NNetworkUtils.getFreeTcpPort();
            }
            Server?.dispose();
            Server = new TcpNServer(m_IpAddress, m_Port);
            Server.IsAllowLog = m_IsAlowLog;
            Server.IsAllowErrorLog = m_IsAllowErrorLog;
            Server.IsAllowWarningLog = m_IsAllowWarningLog;
            try
            {
                Server.startServer();
            }
            catch (Exception e)
            {
                if (e.HResult == -2147467259)
                {
                    if (m_IsUseDynamicPort)
                    {
                        m_Port = NNetworkUtils.getFreeTcpPort();
                        startAsServer();
                        return;
                    }
                    else
                    {
                        onFailedToStartServer(e.ToString());
                    }
                }
                else
                {
                    onFailedToStartServer(e.ToString());
                }
            }

            Server.setOnClientConnectedCallback(onHaveNewClientConnected);
            Server.setOnHaveClientReconnectedCallback(onHaveClientReconnected);
            Server.setOnClientDisconnectedCallback(onClientDisconnected);
            Server.setOnServerReceivedDataCallback(onServerReceivedData);
            Server.setOnStartListeningCallback(onStartListening);
            Server.setOnServerStartedCallback(onServerStarted);
            Server.setOnStopListeningCallback(onStopListening);
        }
        public void startAsClient()
        {
            if (IsClient)
            {
                return;
            }
            if (Client == null || !Client.IsConnecting)
            {
                Client?.dispose();
                Client = new TcpNClient();
                Client.IsDisconnectWhenTimeout = Server == null;
                Client.IsAllowLog = m_IsAlowLog;
                Client.IsAllowErrorLog = m_IsAllowErrorLog;
                Client.IsAllowWarningLog = m_IsAllowWarningLog;
                Client.startClient(m_IpAddress, m_Port);

                Client.setOnClientReceivedDataCallback(onClientReceivedData);
                Client.setOnConnectedToServerCallback(onClientConnectedToServer);
                Client.setOnReconnectedToServerCallback(onClientReconnectedToServer);
                Client.setOnDisconnectedToServerCallback(onDisconnectedToServer);
                Client.setOnTimeoutConnectToServerCallback(onTimeoutConnectToServer);
            }
        }

        public void startAsClient(string ipAddress, int port)
        {
            m_IpAddress = ipAddress;
            m_Port = port;
            startAsClient();
        }

        public void startAsClient(string ipAddress)
        {
            m_IpAddress = ipAddress;
            startAsClient();
        }

        public void startAsHost()
        {
            startAsServer();
            startAsClient();
        }

        public void stopServer()
        {
            Server?.dispose();
            Server = null;
        }
        public void stopClient()
        {
            Client?.dispose();
            Client = null;
        }
        public void stopHost()
        {
            stopServer();
            stopClient();
        }

        public void stopAll()
        {
            stopHost();
        }


        #region Server virtual functions
        protected virtual void onServerReceivedData(ClientData client, NPackage package)
        {

        }
        protected virtual void onStartListening()
        {

        }
        protected virtual void onStopListening()
        {

        }
        protected virtual void onHaveNewClientConnected(ClientData client)
        {

        }
        protected virtual void onHaveClientReconnected(ClientData client)
        {

        }
        protected virtual void onClientDisconnected(ClientData client)
        {

        }
        protected virtual void onServerStarted()
        {
            
        }
        protected virtual void onFailedToStartServer(string failedMsg)
        {

        }
        #endregion

        #region Client virtual functions
        protected virtual void onClientReceivedData(Socket server, NPackage package)
        {

        }
        protected virtual void onClientConnectedToServer(Socket server)
        {
            IsClient = true;
        }
        protected virtual void onClientReconnectedToServer(Socket server)
        {
            IsClient = true;
        }
        protected virtual void onDisconnectedToServer(Socket server)
        {
            IsClient = false;
            if (m_IsAutoReconnectWhenDisconnect && Client.ServerEP != null)
            {
                NCoroutine.runDelay(3f, () =>
                {
                    Client.startClient(Client.ServerEP);
                });
            }
        }
        protected virtual void onTimeoutConnectToServer(Socket server)
        {
            if (m_IsAutoReconnectWhenTimeout && Client.ServerEP != null)
            {
                NRunOnMainThread.Instance.addAction(() =>
                {
                    NCoroutine.runDelay(3f, () =>
                    {
                        Client.startClient(Client.ServerEP);
                    });
                });
            }
        }
        #endregion
        private string getIp()
        {
            switch (m_WayToGetIp)
            {
                case WayToGetIp.CUSTOM:
                    return m_IpAddress;
                case WayToGetIp.LOOPBACK:
                    return NNetworkUtils.LOOPBACK_IP;
                case WayToGetIp.AUTO:
                    return NNetworkUtils.getFirstCardNetwork().ToString();

            }
            return "";
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace NExtension.Network
{
    public enum NPackgeType
    {
        BYTE = 0,
        INT = 1,
        FLOAT = 2,
        STRING = 3,
        CUSTOM = 99,
        JPG = 111,
        NETWORK_LIFETIME = 99999
    }
    public abstract class NPackage
    {
        private static uint m_CurrentSession = 0;
        public uint PackageSession { get; set; }
        public NPackgeType PackageType { get; set; }
        public int DataLength { get; set; }
        public byte[] NPackageByte { get; set; }
        public byte[] RawData { get; protected set; }
        public NPackage(NPackgeType packgeType)
        {
            PackageSession = m_CurrentSession++;
            PackageType = packgeType;
        }
        protected void setNPackageData()
        {
            DataLength = RawData.Length;
            NPackageByte = new byte[4 + 4 + 4 + DataLength];
            Buffer.BlockCopy(BitConverter.GetBytes(PackageSession), 0, NPackageByte, 0, 4); //Set session
            Buffer.BlockCopy(BitConverter.GetBytes((int)PackageType), 0, NPackageByte, 4, 4); //Set packgetype
            Buffer.BlockCopy(BitConverter.GetBytes(DataLength), 0, NPackageByte, 8, 4); //Set data length
            Buffer.BlockCopy(RawData, 0, NPackageByte, 12, DataLength); // Set data
        }
        protected void updateDataFromByte(byte[] bytes)
        {
            PackageSession = BitConverter.ToUInt32(bytes, 0);
            DataLength = BitConverter.ToInt32(bytes, 8);
            RawData = new byte[DataLength];
            Buffer.BlockCopy(bytes, 12, RawData, 0, DataLength);
            NPackageByte = bytes;
        }
        public static NPackage serialize(byte[] bytes)
        {
            NPackgeType packgeType = (NPackgeType)(BitConverter.ToInt32(bytes, 4));
            switch (packgeType)
            {
                case NPackgeType.BYTE:
                    return new NPackageByte(bytes, true);
                case NPackgeType.INT:
                    return new NPackageInt(bytes);
                case NPackgeType.FLOAT:
                    return new NPackageFloat(bytes);
                case NPackgeType.STRING:
                    return new NPackageString(bytes);
                case NPackgeType.JPG:
                    return new NPackageJPG(bytes);
                case NPackgeType.NETWORK_LIFETIME:
                    return new NetworkLifeTimePackage(bytes);
                default:
                    return null;
            }

        }
    }
    public interface ICustomPackage
    {
        void parse(byte[] bytes);
        byte[] toBytes();
    }


    public class NPackageByte : NPackage
    {
        public byte[] ByteData { get; protected set; }
        public NPackageByte(byte[] bytes) : base(NPackgeType.BYTE)
        {
            RawData = bytes;
            ByteData = bytes;
            setNPackageData();
        }
        /// <summary>
        /// from bytes to NPackage 
        /// </summary>
        /// <param name="bytes">if isNpackage is true, bytes is NpackagBytes, otherwise bytes is RawData</param>
        public NPackageByte(byte[] bytes, bool isNPackage) : base(NPackgeType.BYTE)
        {
            if (isNPackage)
            {
                updateDataFromByte(bytes);
                ByteData = RawData;
            }
            else
            {
                RawData = bytes;
                ByteData = bytes;
                setNPackageData();
            }
        }
    }
    public class NPackageInt : NPackage
    {
        public int IntData { get; protected set; }
        public NPackageInt(int num) : base(NPackgeType.INT)
        {
            RawData = BitConverter.GetBytes(num);
            IntData = num;
            setNPackageData();
        }
        /// <summary>
        /// from bytes to NPackage
        /// </summary>
        /// <param name="bytes"></param>
        public NPackageInt(byte[] bytes) : base(NPackgeType.INT)
        {
            updateDataFromByte(bytes);
            IntData = BitConverter.ToInt32(RawData, 0);
        }
    }
    public class NPackageFloat : NPackage
    {
        public float FloatData { get; protected set; }
        public NPackageFloat(float num) : base(NPackgeType.FLOAT)
        {
            RawData = BitConverter.GetBytes(num);
            FloatData = num;
            setNPackageData();
        }
        /// <summary>
        /// from bytes to NPackage
        /// </summary>
        /// <param name="bytes"></param>
        public NPackageFloat(byte[] bytes) : base(NPackgeType.FLOAT)
        {
            updateDataFromByte(bytes);
            FloatData = BitConverter.ToSingle(RawData, 0);
        }
    }
    public class NPackageString : NPackage
    {
        public string StrData { get; protected set; }
        public NPackageString(string str) : base(NPackgeType.STRING)
        {
            RawData = Encoding.UTF8.GetBytes(str);
            StrData = str;
            setNPackageData();
        }
        /// <summary>
        /// from bytes to NPackage
        /// </summary>
        /// <param name="bytes"></param>
        public NPackageString(byte[] bytes) : base(NPackgeType.STRING)
        {
            updateDataFromByte(bytes);
            StrData = Encoding.UTF8.GetString(RawData);
        }
    }

    public class NPackageJPG : NPackage
    {
        public JpgPkg JpgPkg { get; private set; }
        public NPackageJPG(JpgPkg jpgPkg) : base(NPackgeType.JPG)
        {
            JpgPkg = jpgPkg;
            RawData = JpgPkg.toBytes();
            setNPackageData();
        }
        /// <summary>
        /// from bytes to NPackage
        /// </summary>
        /// <param name="bytes"></param>
        public NPackageJPG(byte[] bytes) : base(NPackgeType.JPG)
        {
            updateDataFromByte(bytes);
            JpgPkg = JpgPkg.parse(RawData);
        }
    }

    public class JpgPkg
    {
        public int Id { get; private set; }
        public byte[] JpgData { get; set; }
        
        private byte[] PgkBytes { get; set; }

        public JpgPkg(int id, byte[] jpgData)
        {
            Id = id;
            JpgData = jpgData;
            PgkBytes = NUtilities.NUtils.merge(BitConverter.GetBytes(Id), JpgData);
        }
        public byte[] toBytes()
        {
            return PgkBytes; 
        }
        public static JpgPkg parse(byte[] bytes)
        {
            int id = BitConverter.ToInt32(bytes, 0);
            var data = new byte[bytes.Length - 4];
            Buffer.BlockCopy(bytes, 4, data, 0, data.Length);
            return new JpgPkg(id, data);
        }
    }

    public class NetworkLifeTimePackage : NPackage
    {
        public int IntData { get; protected set; }
        public NPackage CustomPackage { get; protected set; }
        public NetworkLifeTimePackage() : base(NPackgeType.NETWORK_LIFETIME)
        {
            RawData = BitConverter.GetBytes((int)NPackgeType.NETWORK_LIFETIME);
            IntData = (int)NPackgeType.NETWORK_LIFETIME;
            setNPackageData();
        }
        /// <summary>
        /// from bytes to NPackage
        /// </summary>
        /// <param name="bytes"></param>
        public NetworkLifeTimePackage(byte[] bytes) : base(NPackgeType.NETWORK_LIFETIME)
        {
            updateDataFromByte(bytes);
            IntData = BitConverter.ToInt32(RawData, 0);
        }
    }
    //public class NCustomPackage : NPackage
    //{
    //    public NPackage CustomPackage { get; protected set; }
    //    public NCustomPackage(ICustomPackage customPackage) : base(NPackgeType.CUSTOM)
    //    {

    //    }
    //}
}

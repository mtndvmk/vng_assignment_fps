﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

namespace NExtension.Network
{
    public class UdpNServer : AbsNServer
    {
        public UdpNServer(string ipAddress, int port) : base(ipAddress, port, SocketType.Dgram, ProtocolType.Udp) { }
        protected override void socketListen()
        {
            
        }
    }
}

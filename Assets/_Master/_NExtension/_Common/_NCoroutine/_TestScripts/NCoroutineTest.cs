﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NExtension;

public class NCoroutineTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("1"))
        {
            NCoroutine.startCoroutine(coroutine1());
        }
        if (Input.GetKeyDown("2"))
        {
            NCoroutine.startCoroutine(coroutine2());
        }
        if (Input.GetKeyDown("3"))
        {
            NCoroutine.stopCoroutine(coroutine1());
        }
        if (Input.GetKeyDown("4"))
        {
            NCoroutine.stopCoroutine(coroutine2());
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            NCoroutine.stopAllCoutines();
        }
    }

    IEnumerator coroutine1()
    {
        int i = 0;
        while (i < 10)
        {
            Debug.Log("C1: " + i++);
            yield return new WaitForSeconds(1);
        }
    }
    IEnumerator coroutine2()
    {
        int i = 0;
        while (i < 20)
        {
            Debug.Log("C2: " + i++);
            yield return new WaitForSeconds(1);
        }
    }
}

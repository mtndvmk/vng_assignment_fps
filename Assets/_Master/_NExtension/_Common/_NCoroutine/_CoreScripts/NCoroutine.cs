﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NExtension
{
    public class NCoroutine : NSingleton<NCoroutine>
    {
        private class NCoroutinePrams
        {
            public int GroupId { get; private set; }
            public string Name { get; private set; }
            public Coroutine Coroutine { get; private set; }
            public bool IsDone { get; private set; }
            public NCoroutinePrams(IEnumerator func, int groupId, UnityAction doneCallback = null)
            {
                GroupId = groupId;
                Name = func.ToString();
                IsDone = false;
                IEnumerator enumerator()
                {
                    yield return func;
                    IsDone = true;
                    doneCallback?.Invoke();
                    NCoroutine.Instance.NCoroutineList.Remove(this);
                }
                Coroutine = Instance.StartCoroutine(enumerator());
            }
        }
        private void OnDestroy()
        {
            Instance.StopAllCoroutines();
        }
        private List<NCoroutinePrams> NCoroutineList { get; set; } = new List<NCoroutinePrams>();
        private List<NCoroutinePrams> getNPramsIENumerator(IEnumerator enumerator, int groupId)
        {
            List<NCoroutinePrams> nCoroutines = new List<NCoroutinePrams>();
            string key = enumerator.ToString();
            for (int i = 0; i < NCoroutineList.Count; i++)
            {
                if (key == NCoroutineList[i].Name && NCoroutineList[i].GroupId == groupId)
                {
                    nCoroutines.Add(NCoroutineList[i]);
                }
            }
            return nCoroutines;
        }
        private List<NCoroutinePrams> getNPramsIENumerator(int groupId)
        {
            List<NCoroutinePrams> nCoroutines = new List<NCoroutinePrams>();
            for (int i = 0; i < NCoroutineList.Count; i++)
            {
                if (NCoroutineList[i].GroupId == groupId)
                {
                    nCoroutines.Add(NCoroutineList[i]);
                }
            }
            return nCoroutines;
        }
        public static Coroutine startCoroutine(IEnumerator function, bool stopAllOld = false, UnityAction finishCoroutineCallback = null)
        {
            return startCoroutine(function, -1, stopAllOld, finishCoroutineCallback);
        }
        public static Coroutine startCoroutine(IEnumerator func, int groupId, bool stopAllOld = false, UnityAction finishCoroutineCallback = null)
        {
            if (Instance == null)
            {
                var o = new GameObject();
                o.name = "NCoroutineInstance";
                o.AddComponent<NCoroutine>();
            }
            if (stopAllOld)
            {
                var nCRT = Instance.getNPramsIENumerator(func, groupId);
                for (int i = nCRT.Count - 1; i >= 0; i--)
                {
                    Instance.StopCoroutine(nCRT[i].Coroutine);
                    Instance.NCoroutineList.Remove(nCRT[i]);
                }
            }

            var ncrt = new NCoroutinePrams(func, groupId, finishCoroutineCallback);
            Instance.NCoroutineList.Add(ncrt);
            return ncrt.Coroutine;
        }
        /// <summary>
        /// Stop Coroutine(s) which are started by NCoroutine object
        /// </summary>
        /// <param name="function"></param>
        /// <param name="stopKey">0: first coroutine --- 1: last coroutine --- 2: random coroutine --- 3 or default: all coroutines</param>
        public static void stopCoroutine(IEnumerator function, int stopKey = 3)
        {
            if (m_HasQuit)
            {
                return;
            }
            var nCRT = Instance.getNPramsIENumerator(function, -1);
            if (nCRT.Count <= 0)
            {
                return;
            }
            int idx = -1;
            switch (stopKey)
            {
                case 0:
                    idx = 0;
                    break;
                case 1:
                    idx = nCRT.Count - 1;
                    break;
                case 2:
                    idx = Random.Range(0, nCRT.Count);
                    break;
                case 3:
                    idx = -1;
                    for (int i = nCRT.Count - 1; i >= 0; i--)
                    {
                        Instance.StopCoroutine(nCRT[i].Coroutine);
                        Instance.NCoroutineList.Remove(nCRT[i]);
                    }
                    break;
                default:
                    idx = -1;
                    for (int i = nCRT.Count - 1; i >= 0; i--)
                    {
                        Instance.StopCoroutine(nCRT[i].Coroutine);
                        Instance.NCoroutineList.Remove(nCRT[i]);
                    }
                    break;
            }
            if (idx != -1)
            {
                Instance.StopCoroutine(nCRT[idx].Coroutine);
                Instance.NCoroutineList.Remove(nCRT[idx]);
            }
        }
        public static void stopGroupCoroutines(int groupId)
        {
            if (m_HasQuit)
            {
                return;
            }
            var nCRT = Instance.getNPramsIENumerator(groupId);
            if (nCRT.Count <= 0)
            {
                return;
            }
            for (int i = nCRT.Count - 1; i >= 0; i--)
            {
                Instance.StopCoroutine(nCRT[i].Coroutine);
                Instance.NCoroutineList.Remove(nCRT[i]);
            }
        }
        public static void stopAllCoutines()
        {
            Instance.StopAllCoroutines();
            Instance.NCoroutineList.Clear();
        }
        public static Coroutine runDelay(float time, UnityAction action)
        {
            IEnumerator enumerator()
            {
                yield return new WaitForSeconds(time);
                action?.Invoke();
            }
            return Instance.StartCoroutine(enumerator());
        }

        public static Coroutine runDelay(YieldInstruction yieldInstruction, UnityAction action)
        {
            IEnumerator enumerator()
            {
                yield return yieldInstruction;
                action?.Invoke();
            }
            return Instance.StartCoroutine(enumerator());
        }
        public static Coroutine runDelay(CustomYieldInstruction yieldInstruction, UnityAction action)
        {
            IEnumerator enumerator()
            {
                yield return yieldInstruction;
                action?.Invoke();
            }
            return Instance.StartCoroutine(enumerator());
        }
        public static void stopCoroutine(Coroutine coroutine)
        {
            if (coroutine != null && !m_HasQuit)
            {
                Instance.StopCoroutine(coroutine);
            }
        }
    }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace NExtension.EditorUtils
{
    public class FindObjecvtReferences
    {
        [ExecuteInEditMode]
        [MenuItem("NEditorUtils/FindObjectReferences")]
        public static void findObjectReferences()
        {
            Component[] tmpObjects = Editor.FindObjectsOfType<Component>(true);
            Dictionary<int, Component> allComponents = new Dictionary<int, Component>();
            foreach (var c in tmpObjects)
            {
                var k = NEditorUtils.getLocalIdentfierInFile(c);
                if (!allComponents.ContainsKey(k)) 
                {
                    allComponents.Add(k, c);
                }
            }


            var activeScene = EditorSceneManager.GetActiveScene();
            string activeSceneString = "";
            if (activeScene.isDirty)
            {
                try
                {
                    var nPath = activeScene.path + "t.unity";
                    EditorSceneManager.SaveScene(activeScene, nPath, true);
                    activeSceneString = File.ReadAllText(nPath);
                    File.Delete(nPath);
                    File.Delete(nPath + ".meta");
                }
                catch (Exception e) { throw e; }
            }
            else
            {
                activeSceneString = File.ReadAllText(activeScene.path);
            }

            if (Selection.gameObjects.Length == 0)
            {
                Debug.Log("No selected object!");
            }
            else
            {
                foreach (var g in Selection.gameObjects)
                {
                    var components = g.GetComponents<Component>();
                    foreach (var com in components)
                    {
                        var id = NEditorUtils.getLocalIdentfierInFile(com);
                        var fIdPattern = "{fileID: " + id + "}";
                        var refIdPattern = "--- !u!";
                        var indexs = new List<int>();
                        var listRefObjects = new Dictionary<int, string>();
                        int pointer = 0;
                        while (pointer < activeSceneString.Length)
                        {
                            var idx = activeSceneString.IndexOf(fIdPattern, pointer);
                            if (idx > pointer)
                            {
                                var lIdx = activeSceneString.LastIndexOf("\n", idx, idx);
                                var tStr = activeSceneString.Substring(lIdx, idx - lIdx).Trim();

                                if (!tStr.Contains("- component:") && !tStr.Contains("m_Father:") && tStr != "-")
                                {
                                    indexs.Add(idx);
                                }
                                pointer = idx + 1;
                            }
                            else
                            {
                                break;
                            }
                        }

                        for (int i = 0; i < indexs.Count; i++)
                        {
                            var lIdx = activeSceneString.LastIndexOf(refIdPattern, indexs[i], indexs[i]);
                            var rIdx = activeSceneString.IndexOf("\n", lIdx, indexs[i] - lIdx);
                            var refId = int.Parse(activeSceneString.Substring(lIdx, rIdx - lIdx).Split('&')[1].Trim());
                            var refComponentPattern = "component: {fileID: " + refId + "}";
                            var tIdx = activeSceneString.IndexOf(refComponentPattern);
                            var tStartNameIdx = activeSceneString.IndexOf("m_Name", tIdx) + 8;
                            var tEndNameIdx = activeSceneString.IndexOf("\n", tStartNameIdx);
                            var tName = activeSceneString.Substring(tStartNameIdx, tEndNameIdx - tStartNameIdx);

                            if (!listRefObjects.ContainsKey(refId))
                            {
                                listRefObjects.Add(refId, $"{tName} {{fileId: {refId}}}");
                            }
                        }

                        if (listRefObjects.Count > 0)
                        {
                            foreach (var refObject in listRefObjects)
                            {
                                var log = $"{g.name} {{m_FileId: {id}}} → ";
                                log += refObject.Value;
                                Debug.Log(log, allComponents[refObject.Key]);
                            }
                        }
                        else
                        {
                            var log = $"{g.name} {{m_FileId: {id}}} → No references";
                            Debug.Log(log);
                        }
                    }
                }
            }
        }
    }
}
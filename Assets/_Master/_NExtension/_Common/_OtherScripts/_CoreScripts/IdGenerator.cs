using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdGenerator
{
    private const int MAX_DAY = 31;
    private const int MAX_HOUR = 24;
    private const int MAX_MINUTE = 60;
    private const int MAX_UNIQUE_ID = 16;
    private const int MAX_ORDER_ID = 6013;
    private const int MAX_OF_MILISECOND = 1000;
    private const int MAX_OF_SECOND = 60;
    public static int convertId(uint uId)
    {
        var idInt = BitConverter.ToInt32(BitConverter.GetBytes(uId), 0);
        return idInt;
    }
    public static uint revertId(int id)
    {
        var idInt = BitConverter.ToUInt32(BitConverter.GetBytes(id), 0);
        return idInt;
    }
    public class DHM16x6013
    {
        private const int MAX_HOUR = 24;
        private const int MAX_MINUTE = 60;
        private const int MAX_UNIQUE_ID = 16;
        private const int MAX_ORDER_ID = 6013;

        private static int s_Order;
        private static DateTime s_LastTime;

        public static int generateId(int orderUniqueId)
        {
            var now = DateTime.Now;
            var s_now = now.ToString("yyyy/MM/dd HH:mm");
            var s_last = s_LastTime.ToString("yyyy/MM/dd HH:mm");
            if (s_now != s_last)
            {
                s_LastTime = now;
                s_Order = 0;
            }
            var uId = generateId(now.Day, now.Hour, now.Minute, orderUniqueId, s_Order++);
            return convertId(uId);
        }
        public static void serializeId(int id, out int day, out int hour, out int minute, out int uniqueId, out int orderId)
        {
            var uId = revertId(id);
            serializeId(uId, out day, out hour, out minute, out uniqueId, out orderId);
        }
        private static uint generateId(int day, int hour, int minute, int uniqueId, int orderId)
        {
            if (day > 31 || day <= 0) throw new Exception($"Day must less than {MAX_UNIQUE_ID} and greater than 0");
            if (uniqueId >= MAX_UNIQUE_ID) throw new Exception($"Max of uniqueId must less than {MAX_UNIQUE_ID}");
            if (orderId >= MAX_ORDER_ID) throw new Exception($"Max of orderId must less than {MAX_ORDER_ID}");
            day -= 1;
            uint orderFactor = 1;
            var uniqueIdFactor = MAX_ORDER_ID * orderFactor;
            var minuteFactor = MAX_UNIQUE_ID * uniqueIdFactor;
            var hourFactor = MAX_MINUTE * minuteFactor;
            var dayFactor = MAX_HOUR * hourFactor;
            var id = orderId * orderFactor + uniqueId * uniqueIdFactor + minute * minuteFactor + hour * hourFactor + day * dayFactor;
            return (uint)id;
        }
        private static void serializeId(uint id, out int day, out int hour, out int minute, out int uniqueId, out int orderId)
        {
            uint orderFactor = 1;
            var uniqueIdFactor = MAX_ORDER_ID * orderFactor;
            var minuteFactor = MAX_UNIQUE_ID * uniqueIdFactor;
            var hourFactor = MAX_MINUTE * minuteFactor;
            var dayFactor = MAX_HOUR * hourFactor;
            var num = id;
            day = (int)(num / dayFactor);
            num -= (uint)(day * dayFactor);
            hour = (int)(num / hourFactor);
            num -= (uint)(hour * hourFactor);
            minute = (int)(num / minuteFactor);
            num -= (uint)(minute * minuteFactor);
            uniqueId = (byte)(num / uniqueIdFactor);
            num -= (uint)(uniqueId * uniqueIdFactor);
            orderId = (int)(num / orderFactor);
            day += 1;
        }
    }
    public class DHMSx1000
    {
        private const int MAX_DAY = 31;
        private const int MAX_MINUTE = 60;
        private const int MAX_OF_MILISECOND = 1000;
        private const int MAX_OF_SECOND = 60;

        private static int s_Order;
        private static DateTime s_LastTime;

        public static int generateId()
        {
            var now = DateTime.Now;
            var s_now = now.ToString("yyyy/MM/dd HH:mm:ss");
            var s_last = s_LastTime.ToString("yyyy/MM/dd HH:mm:ss");
            if (s_now != s_last)
            {
                s_LastTime = now;
                s_Order = 0;
            }

            int dayOfMonth = now.Day - 1;
            int msFactor = MAX_DAY;
            var sFactor = msFactor * MAX_OF_MILISECOND;
            var minuteFactor = MAX_OF_SECOND * sFactor;
            var hourFactor = MAX_MINUTE * minuteFactor;

            var milisecond = (now.Millisecond + s_Order++) % 1000;

            var uniqueId = (uint)(dayOfMonth + milisecond * msFactor + now.Second * sFactor + now.Minute * minuteFactor + now.Hour * hourFactor);
            var id = convertId(uniqueId);
            return id;
        }
        public static void serializeId(int id, out int dayOfMonth, out int hour, out int minute, out int second, out int milisecond)
        {
            int msFactor = MAX_DAY;
            var sFactor = msFactor * MAX_OF_MILISECOND;
            var minuteFactor = MAX_OF_SECOND * sFactor;
            var hourFactor = MAX_MINUTE * minuteFactor;

            var num = revertId(id);
            hour = (int)(num / hourFactor);
            num -= (uint)(hour * hourFactor);
            minute = (int)(num / minuteFactor);
            num -= (uint)(minute * minuteFactor);
            second = (int)(num / sFactor);
            num -= (uint)(second * sFactor);
            milisecond = (int)(num / msFactor);
            dayOfMonth = (int)(num - (uint)(milisecond * msFactor));
        }
    }

}

﻿using System;
using UnityEngine;

namespace NExtension
{
    public class ScreenResizeDetector : NSingleton<ScreenResizeDetector>
    {
        public Vector2Int ScreenSize { get; private set; }
        public event Action<Vector2Int> OnScreenResize;
        void Update()
        {
            if (ScreenSize.x != Screen.width || Screen.height != Screen.height)
            {
                ScreenSize = new Vector2Int(Screen.width, Screen.height);
                OnScreenResize?.Invoke(ScreenSize);
            }
        }
    }
}

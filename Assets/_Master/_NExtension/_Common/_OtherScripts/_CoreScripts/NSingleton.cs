﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NExtension
{
    public abstract class NSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        protected static T s_Instance;
        protected static bool m_HasQuit;
        public static bool HasQuit => m_HasQuit;
        public static T Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    try
                    {
                        initialized();
                    }
                    catch
                    {
                        Debug.LogError($"★ The singleton [{typeof(T)}] must be initialized in MainThread before use");
                        throw new System.Exception("Can't initialize GameObject in sub-threads");
                    }
                }
                return s_Instance;
            }
        }

        [SerializeField] protected bool m_DontDestroyOnLoad = false;
        [SerializeField] protected bool m_GetInstanceNewer = false;
        [SerializeField] protected bool m_IsUsePrefixName = false;
        [SerializeField] protected string m_NamePrefix = "[NSingleton]";
        private void Awake()
        {
            if (s_Instance == null)
            {
                s_Instance = GetComponent<T>();
                if (m_IsUsePrefixName)
                {
                    s_Instance.name = $"{m_NamePrefix} " + getSingletonName<T>();
                }
                s_Instance.name.Trim();
            }
            else if (s_Instance != this)
            {
                if (m_GetInstanceNewer)
                {
                    Destroy(s_Instance.gameObject);
                    s_Instance = GetComponent<T>();
                    if (m_IsUsePrefixName)
                    {
                        s_Instance.name = $"{m_NamePrefix} " + getSingletonName<T>();
                    }
                    s_Instance.name.Trim();
                    if (m_DontDestroyOnLoad)
                    {
                        DontDestroyOnLoad(this);
                    }
                    return;
                }
                else
                {
                    Destroy(gameObject);
                }
                return;
            }
            if (m_DontDestroyOnLoad)
            {
                DontDestroyOnLoad(this);
            }
            onAwake();
        }
        /// <summary>
        /// don't need call
        /// </summary>
        protected virtual void onAwake()
        {

        }

        public static void initialized()
        {
            s_Instance = FindObjectOfType<T>();
            if (s_Instance == null)
            {
                if (!m_HasQuit)
                {
                    var o = new GameObject("[Generated] " + getSingletonName<T>(), typeof(T));
                    s_Instance = o.GetComponent<T>();
                }
            }
        }

        private void OnApplicationQuit()
        {
            m_HasQuit = true;
        }

        private static string getSingletonName<T>()
        {
            return typeof(T).ToString().Split(new char[] { '.' }).Last();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NExtension
{
    public class NLoggerUI : NSingleton<NLoggerUI>
    {
        [SerializeField] private Image m_BG;
        [SerializeField] private Text m_LogText;
        [SerializeField] private Scrollbar m_Scrollbar;

        [SerializeField] private bool m_IsEnableBG;

        protected override void onAwake()
        {
            m_BG.gameObject.SetActive(m_IsEnableBG);
        }

        public void clear()
        {
            m_LogText.text = "";
        }

        public void log(string text, bool clear = false)
        {
            if (clear)
            {
                this.clear();
            }
            Debug.Log(text);
            m_LogText.text += text + "\n";
            m_Scrollbar.value = 0;
        }
    }
}
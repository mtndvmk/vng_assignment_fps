using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace NExtension
{
    public class NGetLogFromUnity : NSingleton<NGetLogFromUnity>
    {
        [SerializeField] private bool m_IsEnable;
        private string LogFolderId;
        private string LogPath => Path.Combine(Application.persistentDataPath, "NGetLogContainer");

        private StreamWriter m_LogWriter;
        private FileStream m_FileStream;

        public event Action<FileInfo> onHaveLogFile;

        protected override void onAwake()
        {
            if (m_IsEnable)
            {
                onHaveLogFile += (f) =>
                {
                    if (string.IsNullOrEmpty(LogFolderId))
                    {
                        GoogleDriveUtils.findFolder("LogContainer", null, (b, driveFile) =>
                        {
                            if (b)
                            {
                                LogFolderId = driveFile.id;
                                uploadFile(f);
                            }
                            else
                            {
                                GoogleDriveUtils.createFolder("LogContainer", null, (b2, driveFile2) =>
                                {
                                    if (b2)
                                    {
                                        LogFolderId = driveFile.id;
                                        uploadFile(f);
                                    }
                                });
                            }
                        });

                    }
                    else
                    {
                        uploadFile(f);
                    }
                };
            }
        }

        private void uploadFile(FileInfo f)
        {
            var data = File.ReadAllBytes(f.FullName);
            if (!string.IsNullOrEmpty(LogFolderId))

                GoogleDriveUtils.uploadFile(data, f.Name, LogFolderId, null, (b, d) =>
                {
                    if (b)
                    {
                        try
                        {
                            File.Delete(f.FullName);
                        }
                        catch (Exception e)
                        {

                        }
                    }
                    else
                    {

                    }
                });
        }

        void Start()
        {
            if (m_IsEnable)
            {
                try
                {
                    if (!Directory.Exists(LogPath))
                    {
                        Directory.CreateDirectory(LogPath);
                    }
                    var d = new DirectoryInfo(LogPath);
                    var files = d.GetFiles("*.txt");
                    foreach (var file in files)
                    {
                        onHaveLogFile?.Invoke(file);
                    }

                    Debug.Log(LogPath);
                }

                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }
        }

        public void init(string privateName)
        {
            if (m_IsEnable)
            {
                try
                {
                    var f = Path.Combine(LogPath, DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_" + privateName + ".txt");
                    m_FileStream = new FileStream(f, FileMode.OpenOrCreate, FileAccess.Write);
                    m_LogWriter = new StreamWriter(m_FileStream);
                    Application.logMessageReceivedThreaded += ReceivedLogThread;
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }
        }

        private void ReceivedLogThread(string condition, string stackTrace, LogType type)
        {
            if (type == LogType.Error || type == LogType.Exception)
            {
                m_LogWriter.Write(stackTrace + "\n-------------------\n");
            }
        }

        private void OnApplicationQuit()
        {
            if (m_IsEnable)
            {
                m_LogWriter?.Close();
                m_LogWriter?.Dispose();
                m_FileStream?.Close();
                m_FileStream?.Dispose();
            }
        }
    }
}
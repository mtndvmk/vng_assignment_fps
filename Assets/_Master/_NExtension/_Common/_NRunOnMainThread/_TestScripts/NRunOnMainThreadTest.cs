﻿using NExtension;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NExtension
{
    public class NRunOnMainThreadTest : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            NRunOnMainThread.initialized();
            CustomTask customTask = new CustomTask((token) =>
            {
                NRunOnMainThread.Instance.addAction(() =>
                {
                    Debug.Log("--> Test NRunOnMainThreadTest");
                });
            });
            customTask.start();
        }
    }
}
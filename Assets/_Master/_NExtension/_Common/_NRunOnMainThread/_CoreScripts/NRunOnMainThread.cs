﻿//#define DEBUG_LOG

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NExtension
{
    public class NRunOnMainThread : NSingleton<NRunOnMainThread>
    {
        private Queue<UnityAction> m_ActionQueue = new Queue<UnityAction>();
        public void addAction(UnityAction action)
        {
            void func()
            {
                action?.Invoke();
#if DEBUG_LOG
                Debug.Log("★ " + action.ToString() + " has completed");
#endif
            }
            m_ActionQueue.Enqueue(func);
        }
        public void clearQueue()
        {
            m_ActionQueue.Clear();
#if DEBUG_LOG
            Debug.Log("★ m_ActionQueue has been cleaned");
#endif
        }

        protected override void onAwake()
        {
        }

        private void Update()
        {
            while (m_ActionQueue.Count > 0)
            {
                m_ActionQueue.Dequeue()?.Invoke();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEditor;
using UnityEngine;

namespace NExtension
{
    namespace NUtilities
    {
        public static class NAttributeUtils
        {
            #region Unity Transform and Point Utils
            /// <summary>
            /// return Vector3(self.x, self.y, self.z)
            /// </summary>
            public static Vector3 toVector3(this Vector4 self)
            {
                Vector3 vector3;
                vector3.x = self.x;
                vector3.y = self.y;
                vector3.z = self.z;
                return vector3;
            }
            /// <summary>
            /// return (vector3.x * x, vector3.y * y, vector3.z * z)
            /// </summary>
            /// <param name="vector3"></param>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <param name="z"></param>
            /// <returns></returns>
            public static Vector3 multipleAxis(this Vector3 vector3, float x, float y, float z)
            {
                return new Vector3(vector3.x * x, vector3.y * y, vector3.z * z);
            }
            /// <summary>
            /// return (vector3.x * factor.x, vector3.y * factor.y, vector3.z * factor.z)
            /// </summary>
            /// <param name="vector3"></param>
            /// <param name="factor"></param>
            /// <returns></returns>
            public static Vector3 multipleAxis(this Vector3 vector3, Vector3 factor)
            {
                return new Vector3(vector3.x * factor.x, vector3.y * factor.y, vector3.z * factor.z);
            }
            public static void resetTransform(this Transform self, bool isLocal = false)
            {
                if (!isLocal)
                {
                    self.position = Vector3.zero;
                    self.eulerAngles = Vector3.zero;
                    self.localScale = Vector3.one;
                }
                else
                {
                    self.localPosition = Vector3.zero;
                    self.localEulerAngles = Vector3.zero;
                    self.localScale = Vector3.one;
                }
            }

            public static Vector4 toVector4(this Quaternion quaternion)
            {
                return new Vector4(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
            }

            public static Quaternion toQuaternion(this Vector4 vector4)
            {
                return new Quaternion(vector4.x, vector4.y, vector4.z, vector4.w);
            }

            #endregion

            #region Unity Color Utils
            /// <summary>
            /// return Color(r=self.x, g=self.y, b=self.z, a=self.w)
            /// </summary>
            public static Color toColor(this Vector4 self)
            {
                float max = 1 > self.x ? 1 : self.x;
                max = max > self.y ? max : self.y;
                max = max > self.z ? max : self.z;
                max = max > self.w ? max : self.w;
                return new Color(self.x / max, self.y / max, self.z / max, self.w / max);
            }
            /// <summary>
            /// convert Color to the form Vector4
            /// </summary>
            public static Vector4 toVector4(this Color self)
            {
                return new Vector4(self.r, self.g, self.b, self.a);
            }
            public static Color getTransparentColor(this Color self, float a = 0)
            {
                return new Color(self.r, self.g, self.b, a);
            }
            public static string toHex(this Color color)
            {
                Color32 c = color;
                var hex = string.Format("#{0:X2}{1:X2}{2:X2}{3:X2}", c.r, c.g, c.b, c.a);
                return hex;
            }
            /// <summary>
            /// </summary>
            /// <param name="htmlCode">format: #XXXXXXXX</param>
            /// <returns></returns>
            public static Color toColor(this string htmlCode)
            {
                Color c = Color.clear;
                var b = ColorUtility.TryParseHtmlString(htmlCode, out c);
                if (!b)
                {
                    throw new Exception("---> Error to parse html color");
                }
                return c;
            }
            public static byte[] to4Bytes(this Color color)
            {
                byte r = (byte)(color.r * 255);
                byte g = (byte)(color.g * 255);
                byte b = (byte)(color.b * 255);
                byte a = (byte)(color.a * 255);
                var rgba = new byte[4] { r, g, b, a };
                return rgba;
            }
            #endregion

            #region String Utils
            public static string firstCharToUpper(this string input)
            {
                if (String.IsNullOrEmpty(input))
                    throw new ArgumentException("Input is null or empty");
                return input.First().ToString().ToUpper() + input.Substring(1).ToLower();
            }
            #endregion
        }
        public static class NUtils
        {
            #region List and Array Utils
            /// <summary>
            /// append appendArray to self (target array)
            /// </summary>
            /// <typeparam name="T">is a built-in type, struct or class</typeparam>
            /// <param name="self">is target array</param>
            /// <param name="appendArray">is append array</param>
            /// <returns></returns>
            public static T[] appendArray<T>(this T[] self, T[] appendArray)
            {
                int oldLength = self.Length;
                Array.Resize(ref self, self.Length + appendArray.Length);
                for (int i = oldLength; i < self.Length; i++)
                {
                    self[i] = appendArray[i - appendArray.Length];
                }
                return self;
            }

            public static void extendArray<T>(this T[] self, int length)
            {
                if (length <= self.Length)
                {
                    Debug.LogWarning("New capacity must larger old capacity");
                }
                else
                {
                    var arrTemp = new T[length];
                    for (int i = 0; i < self.Length; i++)
                    {
                        arrTemp[i] = self[i];
                    }
                    self = arrTemp;
                }
            }

            /// <summary>
            /// get a random item in contain list, throw Exception if list count is 0
            /// </summary>
            /// <typeparam name="T">is a built-in type, struct or class</typeparam>
            /// <param name="self">is contain list</param>
            /// <param name="index">index of returned item</param>
            /// <param name="exceptIndex">exclude indexes if you didn't want it is returned</param>
            /// <returns></returns>
            public static T randItem<T>(this List<T> self, out int index, List<int> exceptIndex = null)
            {
                if (self.Count == 0)
                {
                    throw new Exception("Item count of input list is 0");
                }
                int randNum = UnityEngine.Random.Range(0, self.Count);

                if (exceptIndex == null)
                {
                    index = randNum;
                    return self[randNum];
                }
                else
                {
                    // create a enumerable which contain indexes of contain list and exclude index of items don't want be returned
                    var indexEnum = Enumerable.Range(0, self.Count).Where(i => !exceptIndex.Contains(i));
                    // get random index of enumerable
                    randNum = UnityEngine.Random.Range(0, self.Count - exceptIndex.Count);
                    // get value of random enumerable, which is index of random item
                    index = indexEnum.ElementAt(randNum);
                    // return random item
                    return self[indexEnum.ElementAt(randNum)];
                }
            }
            /// <summary>
            /// remove items in list
            /// </summary>
            /// <typeparam name="T">is a built-in type, struct or class</typeparam>
            /// <param name="self">is list contains item which you want remove</param>
            /// <param name="indexList">index of items which you want remove</param>
            public static void removeAt<T>(this List<T> self, List<int> indexList)
            {
                for (int i = indexList.Count - 1; i >= 0; --i)
                {
                    self.RemoveAt(indexList[i]);
                }
            }
            /// <summary>
            /// remove items in list
            /// </summary>
            /// <typeparam name="T">is a built-in type, struct or class</typeparam>
            /// <param name="self">is list contains item which you want remove</param>
            /// <param name="indexs">index of items which you want remove</param>
            public static void removeAt<T>(this List<T> self, int[] indexs)
            {
                for (int i = indexs.Length - 1; i >= 0; --i)
                {
                    self.RemoveAt(indexs[i]);
                }
            }
            public static T[][] switchRowToCol<T>(this T[][] self)
            {
                if (self.Length == 0)
                {
                    throw new Exception("input have row is 0");
                }
                if (self[0].Length == 0)
                {
                    throw new Exception("input have col is 0");
                }
                var rowCount = self.Length;
                var colCount = self[0].Length;
                List<T[]> nArray = new List<T[]>();
                for (int i = 0; i < colCount; i++)
                {
                    var tempRow = new T[rowCount];
                    for (int j = 0; j < rowCount; j++)
                    {
                        tempRow[j] = self[j][i];
                    }
                    nArray.Add(tempRow);
                }
                return nArray.ToArray();
            }

            public static bool compareTo(this byte[] a, byte[] b)
            {
                if (a == null || b == null)
                {
                    return false;
                }
                if (a.Length != b.Length)
                {
                    return false;
                }
                return Enumerable.SequenceEqual(a, b);
                //for (int i = 0; i < a.Length; i++)
                //{
                //    if (a[i] != b[i])
                //    {
                //        return false;
                //    }
                //}
                //return true;
            }

            #endregion

            #region Random Utils
            public static int randInt32(int min, int max, List<int> exceptIndex)
            {
                var intRand = new System.Random();
                if (exceptIndex == null || exceptIndex.Count == 0)
                {
                    return intRand.Next(min, max);
                }
                var idxEnum = Enumerable.Range(min, max).Where(i => !exceptIndex.Contains(i));
                var randIdx = intRand.Next(max - exceptIndex.Count);
                return idxEnum.ElementAt(randIdx);
            }
            public static (int[], int) fillRandomArray(int arrayLength, int fillCount)
            {
                int[] array = new int[arrayLength];
                var rnd = new System.Random();
                var randomNumbers = Enumerable.Range(0, arrayLength).OrderBy(x => rnd.Next()).Take(fillCount).ToList();
                int num = 1;
                for (int i = 0; i < randomNumbers.Count; i++)
                {
                    array[randomNumbers[i]] = num++;
                }
                return (array, randomNumbers[0]);
            }
            #endregion

            #region NativeArray Utils
            public static void copyToFast<T>(this NativeArray<T> src, NativeArray<T> dst) where T : struct
            {
                if (dst == null)
                {
                    throw new NullReferenceException(nameof(dst) + " is null");
                }
                int srcLength = src.Length;
                if (dst.Length < srcLength)
                {
                    throw new IndexOutOfRangeException(
                        nameof(dst) + " is shorter than " + nameof(src));
                }
#if UNSAFE_ENABLE
            unsafe
            {
                int byteLength = src.Length * UnsafeUtility.SizeOf<T>();
                void* dstBuffer = dst.GetUnsafePtr();
                void* srcBuffer = src.GetUnsafePtr();
                UnsafeUtility.MemCpy(dstBuffer, srcBuffer, byteLength);
            }
#else
                for (int i = 0; i < srcLength; i++)
                {
                    dst[i] = src[i];
                }
#endif
            }
            public static void tryDispose<T>(this NativeArray<T> arr) where T : struct
            {
                if (arr.IsCreated)
                {
                    arr.Dispose();
                }
            }
            public static void extendArray<T>(this NativeArray<T> arr, int capacity, Allocator allocator) where T : struct
            {
                if (capacity <= arr.Length)
                {
                    Debug.LogWarning("New capacity must larger old capacity");
                }
                else
                {
                    var nativeArrTemp = new NativeArray<T>(capacity, allocator);
                    arr.copyToFast(nativeArrTemp);
                    arr.tryDispose();
                    arr = nativeArrTemp;
                }
            }
            #endregion

            #region Convert Utils
            public static Vector3 bytesToVector3(byte[] bytes)
            {

                float x = BitConverter.ToSingle(bytes, 0);
                float y = BitConverter.ToSingle(bytes, 4);
                float z = BitConverter.ToSingle(bytes, 8);

                return new Vector3(x, y, z);
            }public static Vector3 bytesToVector3(byte[] src, int startIndex)
            {
                float x = BitConverter.ToSingle(src, startIndex + 0);
                float y = BitConverter.ToSingle(src, startIndex + 4);
                float z = BitConverter.ToSingle(src, startIndex + 8);

                return new Vector3(x, y, z);
            }
            public static byte[] vector3ToBytes(this Vector3 vector3)
            {

                byte[] x = BitConverter.GetBytes(vector3.x);
                byte[] y = BitConverter.GetBytes(vector3.y);
                byte[] z = BitConverter.GetBytes(vector3.z);

                byte[] bytes = new byte[x.Length + y.Length + z.Length];

                Buffer.BlockCopy(x, 0, bytes, 0, x.Length);
                Buffer.BlockCopy(y, 0, bytes, x.Length, y.Length);
                Buffer.BlockCopy(z, 0, bytes, x.Length + y.Length, z.Length);

                return bytes;
            }
            public static byte[] merge(params byte[][] arrays)
            {
                int totalLength = 0;
                for (int i = 0; i < arrays.Length; i++)
                {
                    totalLength += arrays[i].Length;
                }
                var data = new byte[totalLength];
                int pointer = 0;
                for (int i = 0; i < arrays.Length; i++)
                {
                    Buffer.BlockCopy(arrays[i], 0, data, pointer, arrays[i].Length);
                    pointer += arrays[i].Length;
                }
                return data;
            }
            public static byte[] mergeTo(this byte[] a, byte[] b)
            {
                var aOffset = a.Length;
                Array.Resize(ref a, a.Length + b.Length);
                Buffer.BlockCopy(b, 0, a, aOffset, b.Length);
                return a;
            }
            public static byte[] getBlock(byte[] src, int startIndex, int count)
            {
                byte[] b = new byte[count];
                Buffer.BlockCopy(src, startIndex, b, 0, count);
                return b;
            }
            public static Vector4 bytesToVector4(byte[] bytes)
            {
                float x = BitConverter.ToSingle(bytes, 0);
                float y = BitConverter.ToSingle(bytes, 4);
                float z = BitConverter.ToSingle(bytes, 8);
                float w = BitConverter.ToSingle(bytes, 12);
                return new Vector4(x, y, z, w);
            }
            public static Vector4 bytesToVector4(byte[] src, int startIndex)
            {
                float x = BitConverter.ToSingle(src, startIndex + 0);
                float y = BitConverter.ToSingle(src, startIndex + 4);
                float z = BitConverter.ToSingle(src, startIndex + 8);
                float w = BitConverter.ToSingle(src, startIndex + 12);
                return new Vector4(x, y, z, w);
            }
            public static byte[] vector4ToBytes(this Vector4 vector4)
            {

                byte[] x = BitConverter.GetBytes(vector4.x);
                byte[] y = BitConverter.GetBytes(vector4.y);
                byte[] z = BitConverter.GetBytes(vector4.z);
                byte[] w = BitConverter.GetBytes(vector4.w);

                byte[] bytes = new byte[x.Length + y.Length + z.Length + w.Length];

                Buffer.BlockCopy(x, 0, bytes, 0, x.Length);
                Buffer.BlockCopy(y, 0, bytes, x.Length, y.Length);
                Buffer.BlockCopy(z, 0, bytes, x.Length + y.Length, z.Length);
                Buffer.BlockCopy(w, 0, bytes, x.Length + y.Length + z.Length, w.Length);

                return bytes;
            }
            public static byte[] exportToBytes(this LineRenderer lineRenderer)
            {
                byte[] data = new byte[1];
                if (lineRenderer.useWorldSpace)
                {
                    data[0] = 1;
                }
                else
                {
                    data[0] = 0;
                }
                data = merge(data, lineRenderer.startColor.to4Bytes(), lineRenderer.endColor.to4Bytes());
                data = merge(data, BitConverter.GetBytes(lineRenderer.startWidth), BitConverter.GetBytes(lineRenderer.endWidth));
                data = merge(data, lineRenderer.transform.localPosition.vector3ToBytes(),
                    lineRenderer.transform.localRotation.toVector4().vector4ToBytes(),
                    lineRenderer.transform.localScale.vector3ToBytes());
                for (int i = 0; i < lineRenderer.positionCount; i++)
                {
                    data = data.mergeTo(lineRenderer.GetPosition(i).vector3ToBytes());
                }
                return data;
            }
            public static void importFromBytes(this LineRenderer lineRenderer, byte[] data)
            {
                int offset = 0;
                lineRenderer.useWorldSpace = data[0] == 1; offset += 1;
                lineRenderer.startColor = convert4BytesToColor(data, offset); offset += 4;
                lineRenderer.endColor = convert4BytesToColor(data, offset); offset += 4;
                lineRenderer.startWidth = BitConverter.ToSingle(data, offset); offset += 4;
                lineRenderer.endWidth = BitConverter.ToSingle(data, offset); offset += 4;

                lineRenderer.transform.localPosition = bytesToVector3(data, offset); offset += 12;
                lineRenderer.transform.localRotation = bytesToVector4(data, offset).toQuaternion(); offset += 16;
                lineRenderer.transform.localScale = bytesToVector3(data, offset); offset += 12;

                List<Vector3> posList = new List<Vector3>();

                while (offset < data.Length)
                {
                    var pos = bytesToVector3(data, offset); offset += 12;
                    posList.Add(pos);
                }
                lineRenderer.positionCount = posList.Count;
                lineRenderer.SetPositions(posList.ToArray());
            }
            public static Color convert4BytesToColor(byte[] rgba)
            {
                float r = rgba[0] / 255f;
                float g = rgba[1] / 255f;
                float b = rgba[2] / 255f;
                float a = rgba[3] / 255f;
                return new Color(r, g, b, a);
            }
            public static Color convert4BytesToColor(byte[] rgba, int startIndex)
            {
                float r = rgba[startIndex + 0] / 255f;
                float g = rgba[startIndex + 1] / 255f;
                float b = rgba[startIndex + 2] / 255f;
                float a = rgba[startIndex + 3] / 255f;
                return new Color(r, g, b, a);
            }
            #endregion

            #region Nullable parse
            public static int? tryParseInt(string s)
            {
                if (int.TryParse(s, out var r))
                {
                    return r;
                }
                return null;
            }
            public static float? tryParseFloat (string s)
            {
                if (float.TryParse(s, out var r))
                {
                    return r;
                }
                return null;
            }
            public static string nullableToString<T>(this T? t) where T : struct
            {
                if (!t.HasValue) return "";
                return t.Value.ToString();
            }
            #endregion
        }
    }
}
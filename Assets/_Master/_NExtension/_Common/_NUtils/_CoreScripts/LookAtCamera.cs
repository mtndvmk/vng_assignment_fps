﻿using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    [SerializeField] protected Transform m_Target;
    [SerializeField] protected Vector3 m_OrgAngle;
    [SerializeField] public bool m_LookAtX = true;
    [SerializeField] public bool m_LookAtY = true;
    [SerializeField] public bool m_LookAtZ = true;
    [SerializeField] public bool m_AlwaysLookAt = true;
    [SerializeField] public bool m_IsRotateFollowCamera = true;

    private Vector3 m_LastCameraPosition = Vector3.one * -1f;

    public bool IsAlwaysLookAt
    {
        get { return m_AlwaysLookAt; }
        set { m_AlwaysLookAt = value; }
    }

    protected virtual void Awake()
    {
        //m_Camera = Camera.main;
        if (m_Target == null)
        {
            m_Target = Camera.main.transform;
        }

        //init last camera position, make sure that transform will update at the first time
        m_LastCameraPosition = m_Target.transform.position - Vector3.one;
    }

    private void OnValidate()
    {
        m_OrgAngle = transform.localEulerAngles;
    }

    protected virtual void OnEnable()
    {
        updateTransform(true);
    }

    protected virtual void LateUpdate()
    {
        if (m_AlwaysLookAt)
        {
            updateTransform();
        }
    }

    public void updateTransform(bool forceUpdate = false)
    {
        if (m_Target == null)
        {
            return;
        }
        if (m_IsRotateFollowCamera)
        {
            var angles = m_Target.transform.eulerAngles;

            if (!m_LookAtX) angles.x = m_OrgAngle.x;
            if (!m_LookAtY) angles.y = m_OrgAngle.y;
            if (!m_LookAtZ) angles.z = m_OrgAngle.z;

            transform.eulerAngles = angles;
        }
        else
        {
            //if camera not moving, don't update transform of panel (save performance)
            //if (!forceUpdate && Vector3.SqrMagnitude(m_Camera.transform.position - m_LastCameraPosition) < 0.001f)
            //{
            //    return;
            //}

            transform.LookAt(m_Target.transform);
            transform.Rotate(Vector3.up, 180f, Space.Self);

            //var angles = new Vector3(transform.localEulerAngles.x, 180f + transform.localEulerAngles.y, transform.localEulerAngles.z);
            var angles = transform.localEulerAngles;

            if (!m_LookAtX) angles.x = m_OrgAngle.x;
            if (!m_LookAtY) angles.y = m_OrgAngle.y;
            if (!m_LookAtZ) angles.z = m_OrgAngle.z;
            transform.localEulerAngles = angles;

            //Debug.Log("Org angle = " + m_OrgAngle + " - current angle = " + angles);

            m_LastCameraPosition = m_Target.transform.position;
        }

        //Debug.Log(transform.lossyScale);
    }
}
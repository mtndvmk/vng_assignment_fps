namespace NExtension
{
    public interface INBytes
    {
        byte[] exportToBytes();
        void importBytes(byte[] inData);
    }
}
using NExtension.NUtilities;
using System;

namespace NExtension
{
    public class NSplitPackage
    {
        private NSplitPackage() { }
        public int Id { get; private set; }
        public bool IsEntirety { get; private set; }
        public byte[] Data { get; private set; }
        public int BlockCount => Blocks.Length;

        public NSplitBlock[] Blocks { get; private set; }
        public NSplitPackage(int packageId, int blockCount)
        {
            Id = packageId;
            Blocks = new NSplitBlock[blockCount];
        }
        public static NSplitPackage create(int orderId, int splitSize, byte[] inData)
        {
            splitSize -= NSplitBlock.HEADER_SIZE;
            var splitPackage = new NSplitPackage();
            splitPackage.Id = IdGenerator.DHM16x6013.generateId(orderId);
            splitPackage.Data = inData;
            var blockCount = inData.Length / splitSize + (inData.Length % splitSize != 0 ? 1 : 0);
            splitPackage.Blocks = new NSplitBlock[blockCount];
            int offset = 0;
            for (int i = 0; i < blockCount; i++)
            {
                var len = inData.Length - offset;
                if (len > splitSize) len = splitSize;
                splitPackage.Blocks[i] = new NSplitBlock(splitPackage.Id, i, blockCount, NUtils.getBlock(inData, offset, len)); offset += len; 
            }
            splitPackage.IsEntirety = true;
            return splitPackage;
        }
        public void addBlock(NSplitBlock block)
        {
            Blocks[block.BlockOrder] = block;
            IsEntirety = checkEntiretyPackage();
            if (IsEntirety)
            {
                mergeData();
            }
        }
        private bool checkEntiretyPackage()
        {
            for (int i = 0; i < BlockCount; i++)
            {
                if (Blocks[i] == null) return false;
            }
            return true;
        }
        private void mergeData()
        {
            Data = Blocks[0].Data;
            for (int i = 1; i < BlockCount; i++)
            {
                Data = Data.mergeTo(Blocks[i].Data);
            }
        }
    }
    public class NSplitBlock
    {
        public const int HEADER_SIZE = 12;
        public int PackageId { get; private set; }
        public int BlockOrder { get; private set; }
        public int BlockCount { get; private set; }
        public byte[] Data { get; private set; }
        private NSplitBlock() { }
        public NSplitBlock(int packageId, int blockOrder, int blockCount, byte[] inData)
        {
            PackageId = packageId;
            BlockOrder = blockOrder;
            BlockCount = blockCount;
            Data = inData;
        }
        public byte[] toBytes()
        {
            return NUtils.merge(BitConverter.GetBytes(PackageId), BitConverter.GetBytes(BlockOrder), BitConverter.GetBytes(BlockCount), Data);
        }
        public static NSplitBlock fromBytes(byte[] data)
        {
            int offset = 0;
            NSplitBlock block = new NSplitBlock();
            block.PackageId = BitConverter.ToInt32(data, offset); offset += 4;
            block.BlockOrder = BitConverter.ToInt32(data, offset); offset += 4;
            block.BlockCount = BitConverter.ToInt32(data, offset); offset += 4;
            block.Data = NUtils.getBlock(data, offset, data.Length - offset);
            return block;
        }
    }
}
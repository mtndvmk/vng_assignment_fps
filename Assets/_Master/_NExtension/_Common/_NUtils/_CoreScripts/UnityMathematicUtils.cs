﻿using UnityEngine;

namespace NExtension
{
    namespace NUtilities
    {
        public static class UnityMathematicsUtils
        {
            private static Vector3 m_vector3Tmp = Vector3.zero;
            private static Vector4 m_float4Tmp = Vector4.zero;
            private static Color m_ColorTmp = Color.clear;
            public static Vector4 toFloat4(this Vector3 vector3)
            {
                m_float4Tmp.x = vector3.x;
                m_float4Tmp.y = vector3.y;
                m_float4Tmp.z = vector3.z;
                m_float4Tmp.w = 0;
                return m_float4Tmp;
            }
            public static Vector3 toVector3(this ref Vector4 self)
            {
                m_vector3Tmp.x = self.x;
                m_vector3Tmp.y = self.y;
                m_vector3Tmp.z = self.z;
                return m_vector3Tmp;
            }
            public static Vector4 toFloat4(this Color color)
            {
                m_float4Tmp.x = color.r;
                m_float4Tmp.y = color.g;
                m_float4Tmp.z = color.b;
                m_float4Tmp.w = color.a;
                return m_float4Tmp;
            }
            public static Color toColor(this Vector4 float4)
            {
                m_ColorTmp.r = float4.x;
                m_ColorTmp.g = float4.y;
                m_ColorTmp.b = float4.z;
                m_ColorTmp.a = float4.w;
                return m_ColorTmp;
            }
        }
    }
}


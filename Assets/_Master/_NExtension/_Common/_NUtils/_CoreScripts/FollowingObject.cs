﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowingObject : MonoBehaviour
{
    [SerializeField] private Transform m_Target;
    [SerializeField] private float m_SmoothSpeed = 1;
    [SerializeField] private float m_Distance = 3;
    [SerializeField] private float m_DefaultAngleX = 0;
    [SerializeField] private float m_MovingAngleX = -30;
    [SerializeField] private float m_Speed = 1;
    [SerializeField] private Vector3 m_TargetOffset = Vector3.zero;
    [SerializeField] private Vector3 m_EyeOffset = Vector3.zero;
    [SerializeField] private Transform m_TargetSecond;
    [SerializeField] private float m_BillboardSize = 0.3f;
    [SerializeField] private bool m_AppearOnShow = false;
    private Vector3 m_Direction = Vector3.zero;
    private float m_CurrendSpeed = 0;
    private float m_DistanceRate;

    [SerializeField] private float m_MaxDistance = 1.0f;
    public float CurrentDistance { get => m_Direction.magnitude; set { m_Distance = value; } }
    public float DefaultAngleX { get => m_DefaultAngleX; set { m_DefaultAngleX = value; } }
    public float CurrentSpeed { get => m_CurrendSpeed; }

    private Vector3 m_CurrentTargetPos;
    public Vector3 TargetOffset
    {
        get { return m_TargetOffset; }
        set { m_TargetOffset = value; }
    }

    private void OnValidate()
    {
        if (!m_Target && Camera.main)
        {
            m_Target = Camera.main.transform;
        }
    }

    private void OnEnable()
    {
        if (m_AppearOnShow)
        {
            Vector3 targetPos;
            if (m_TargetSecond != null)
            {
                targetPos = m_TargetSecond.position;
            }
            else
            {
                targetPos = m_Target.TransformPoint(m_TargetOffset) + m_Target.forward * m_Distance;
            }

            m_CurrentTargetPos = targetPos;

            transform.position = m_CurrentTargetPos;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPos;
        if (m_TargetSecond != null)
        {
            targetPos = m_TargetSecond.position;
        }
        else
        {
            targetPos = m_Target.TransformPoint(m_TargetOffset) + m_Target.forward * m_Distance;
        }

        if ((targetPos - m_CurrentTargetPos).magnitude < m_BillboardSize)
        {
            targetPos = m_CurrentTargetPos;
        }
        else
        {
            m_CurrentTargetPos = targetPos;
        }
        Vector3 direction = targetPos - transform.position;
        direction.y = 0;
        float distance = direction.magnitude;
        distance = Mathf.Min(distance, m_MaxDistance);
        m_DistanceRate = distance / m_MaxDistance;

        var targetAngles = Vector3.zero;
        targetAngles.x = m_DefaultAngleX + m_DistanceRate * m_MovingAngleX;
        targetAngles.y = Vector2.SignedAngle(Vector2.up, new Vector2(direction.x, -direction.z));

        direction = m_Target.TransformPoint(m_EyeOffset) - transform.position;
        var eyeAngle = Vector3.zero;
        eyeAngle.x = m_DefaultAngleX;
        eyeAngle.y = Vector2.SignedAngle(Vector2.up, new Vector2(direction.x, -direction.z));

        var deltaAngleY = Mathf.Abs(targetAngles.y - eyeAngle.y);
        if (Mathf.Abs(targetAngles.y + 360 - eyeAngle.y) < deltaAngleY)
        {
            targetAngles.y += 360;
        }
        else if (Mathf.Abs(targetAngles.y - 360 - eyeAngle.y) < deltaAngleY)
        {
            targetAngles.y -= 360;
        }

        Vector3 angle = Vector3.Lerp(eyeAngle, targetAngles, m_DistanceRate);

        var currentAngleY = transform.eulerAngles.y;
        deltaAngleY = Mathf.Abs(angle.y - currentAngleY);
        if (Mathf.Abs(angle.y + 360 - currentAngleY) < deltaAngleY)
        {
            angle.y += 360;
        }
        else if (Mathf.Abs(angle.y - 360 - currentAngleY) < deltaAngleY)
        {
            angle.y -= 360;
        }

        angle.y = Mathf.Lerp(transform.eulerAngles.y, angle.y, Time.deltaTime * 0.5f);
        transform.eulerAngles = angle;

        var newDirection = targetPos - transform.position;

        m_Direction = Vector3.Lerp(m_Direction, newDirection, Time.deltaTime * m_SmoothSpeed);
        m_CurrendSpeed = (newDirection - m_Direction).magnitude;
        transform.position += m_Direction * Math.Min(Time.deltaTime * m_Speed, 1);
    }

    public void setFirstTarget(Transform transform)
    {
        m_Target = transform;
    }

    public void setSecondTarget(Transform transform)
    {
        m_TargetSecond = transform;
    }
}

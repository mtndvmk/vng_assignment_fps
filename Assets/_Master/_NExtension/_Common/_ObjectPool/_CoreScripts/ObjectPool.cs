﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool
{
    public static List<PoolIdentity> Pool { get; set; } = new List<PoolIdentity>();
    private static PoolIdentity findObject(int id)
    {
        for (int i = 0; i < Pool.Count; i++)
        {
            if (Pool[i].Id == id)
            {
                var o = Pool[i];
                o.gameObject.SetActive(true);
                Pool.Remove(o);
                //o.resetSpriteRenderer();
                //o.resetAnimator();
                Debug.Log("Get from pool object id: " + id);
                return o;
            }
        }
        return null;
    }
    public static GameObject getObject(GameObject gameObject, Transform parent = null, bool worldPositionStays = true)
    {
        if (gameObject == null)
        {
            Debug.LogWarning("gameObject is null");
            return null;
        }
        var poolId = gameObject.GetComponent<PoolIdentity>();
        if (poolId == null)
        {
            Debug.Log("No have PoolIdentity");
            return null;
        }
        var o = findObject(poolId.Id);
        if (o == null)
        {
            Debug.Log("Spawn new object with id: " + poolId.Id);
            o = GameObject.Instantiate(gameObject).GetComponent<PoolIdentity>();
        }
        o.ResetRequirementFlag = o.ResetRequirement;
        o.transform.SetParent(parent, worldPositionStays);
        return o.gameObject;
    }
    public static GameObject getObject(GameObject gameObject, Vector3 position, bool isLocal = false, Transform parent = null, bool worldPositionStays = true)
    {
        if (gameObject == null)
        {
            Debug.LogWarning("gameObject is null");
            return null;
        }
        var o = getObject(gameObject, parent, worldPositionStays);
        if (isLocal)
        {
            o.transform.localPosition = position;
        }
        else
        {
            o.transform.position = position;
        }
        return o;
    }
    public static void sendToPool(ref GameObject gameObject)
    {
        if (gameObject == null)
        {
            return;
        }
        var poolId = gameObject.GetComponent<PoolIdentity>();
        if (poolId == null)
        {
            Debug.Log("No have PoolIdentity");
            return;
        }
        if (poolId.AutoReset)
        {
            poolId.resetAll();
        }
        if (poolId.ResetRequirementFlag)
        {
            Debug.LogError("Please reset state of gameobject before send to pool");
        }
        else
        {
            Pool.Add(poolId);
            poolId.gameObject.SetActive(false);
            gameObject = null;
        }
    }
}

﻿using UnityEngine;

public class PoolIdentity : MonoBehaviour
{
    public int Id;
    /// <summary>
    /// Require reset state of object before reset
    /// </summary>
    public bool ResetRequirement;
    public bool AutoReset;

    [Header("Default parameters")]
    [SerializeField] private int m_SortingOrderDefault;
    [SerializeField] private string m_SortingLayerNameDefault;

    /// Transform
    [HideInInspector, SerializeField] private Vector3 m_DefaultPosition;
    [HideInInspector, SerializeField] private Vector3 m_DefaultRotation;
    [HideInInspector, SerializeField] private Vector3 m_DefaultScale;

    /// Sprite Renderer
    [HideInInspector, SerializeField] private Color m_DefaultColor;
    [HideInInspector, SerializeField] private bool m_DefaultFlipX;
    [HideInInspector, SerializeField] private bool m_DefaultFlipY;

    /// Animator
    [HideInInspector, SerializeField] private string m_DefaultNameAnimation;

    public bool ResetRequirementFlag { get; set; }

    private void OnValidate()
    {
        var spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer == null)
        {
            m_SortingOrderDefault = int.MinValue;
            m_SortingLayerNameDefault = "null";
        }
        else
        {
            m_SortingOrderDefault = spriteRenderer.sortingOrder;
            m_SortingLayerNameDefault = spriteRenderer.sortingLayerName;
            m_DefaultColor = spriteRenderer.color;
            m_DefaultFlipX = spriteRenderer.flipX;
            m_DefaultFlipY = spriteRenderer.flipY;
        }

        var animator = GetComponent<Animator>();
        if (animator == null)
        {
            m_DefaultNameAnimation = "null";
        }
        else if (animator.isActiveAndEnabled)
        {
            if (animator.GetCurrentAnimatorClipInfo(0).Length > 0)
            {
                m_DefaultNameAnimation = animator.GetCurrentAnimatorClipInfo(0)[0].clip.name;
            }
        }

        m_DefaultPosition = transform.position;
        m_DefaultRotation = transform.eulerAngles;
        m_DefaultScale = transform.localScale;
    }

    public void resetAnimator()
    {
        var animator = GetComponent<Animator>();
        if (animator != null)
        {
            animator.Play(m_DefaultNameAnimation);
            animator.speed = 1;
        }
    }
    public void resetSpriteRenderer()
    {
        var spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer != null)
        {
            spriteRenderer.sortingOrder = m_SortingOrderDefault;
            spriteRenderer.sortingLayerName = m_SortingLayerNameDefault;
            spriteRenderer.color = m_DefaultColor;
            spriteRenderer.flipX = m_DefaultFlipX;
            spriteRenderer.flipX = m_DefaultFlipY;
        }
    }
    public void resetTransform()
    {
        transform.position = m_DefaultPosition;
        transform.eulerAngles = m_DefaultRotation;
        transform.localScale = m_DefaultScale;
    }
    public void resetAll()
    {
        resetTransform();
        resetSpriteRenderer();
        resetAnimator();
        ResetRequirementFlag = false;
    }
}

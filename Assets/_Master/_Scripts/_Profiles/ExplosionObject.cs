using NExtension;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionObject : MonoBehaviour,IBulletTarget,IZombieTarget
{
    [SerializeField] private GameObject m_ExplosionEffect;
    public int m_Hp = 5;
    public int m_ExplosionAtk = 5;
    public float m_ExplosionRange = 10;

    public bool IsExplosive => m_Hp <= 0;

    public void dealDmg(int hp)
    {
        m_Hp -= hp;
        if (m_Hp <= 0)
        {
            explosion();
        }
    }

    public int getLeftHp()
    {
        return m_Hp;
    }

    void explosion()
    {
        if (m_ExplosionEffect)
        {
            var vfx = Instantiate(m_ExplosionEffect, null);
            vfx.transform.position = transform.position;
            vfx.transform.rotation = transform.rotation;
            var objs = Physics.OverlapSphere(vfx.transform.position, m_ExplosionRange);
            foreach (var o in objs)
            {
                if (o == gameObject)
                {
                    break;
                }
                var target = o.GetComponent<IExplosionTarget>();
                if (target != null)
                {
                    var dt = Vector3.Distance(vfx.transform.position, o.transform.position);
                    var atk = Mathf.Lerp(m_ExplosionAtk, 0, dt / m_ExplosionRange);
                    NCoroutine.runDelay(0.2f, () =>
                    {
                        target.dealDmg((int)atk);
                    });
                }
            }
        }
        Destroy(gameObject);
    }
}

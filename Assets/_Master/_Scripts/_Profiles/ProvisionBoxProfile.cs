using NExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProvisionBoxProfile : MonoBehaviour
{
    [SerializeField] private int m_BulletCount;
    [SerializeField] private int m_FirstAidKitCount;

    public int BulletCount => m_BulletCount;
    public int FirstAidKitCount => m_FirstAidKitCount;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<CharacterController>() != null)
        {
            GetComponent<Collider>().enabled = false;

            doScale(Vector3.zero, 1, () =>
            {
                Destroy(gameObject);
                GameplayManager.Instance.receiveProvision(this);
            });
        }
    }

    public void init(int bulletCount, int firstAidCount)
    {
        m_BulletCount = bulletCount;
        m_FirstAidKitCount = firstAidCount;
    }

    private void doScale(Vector3 target, float duration, Action completeCallback)
    {
        if (duration > 0)
        {
            NCoroutine.startCoroutine(coroutineScale(target, duration, completeCallback), GetInstanceID(), true);
        }
        else
        {
            transform.transform.localScale = target;
            completeCallback?.Invoke();
        }
    }
    private IEnumerator coroutineScale(Vector3 target, float duration, Action completeCallback)
    {
        var t = 0f;
        while (t < duration)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, target, t / duration);
            t += Time.deltaTime;
            yield return null;
        }
        transform.transform.localScale = target;
        completeCallback?.Invoke();
    }
}

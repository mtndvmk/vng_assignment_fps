using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProfile : MonoBehaviour
{
    [SerializeField] private GameObject m_CollideEffect;
    [SerializeField] private float m_MaxLifeTime = 1f;
    [SerializeField] private float m_FireSpeed = 100;
    [SerializeField] private int m_Atk = 1;
    private Ray m_Ray;
    private float m_StartMoveTime;
    private float m_DistanceToCollide;
    private RaycastHit m_Target;
    public void move(Ray ray)
    {
        m_Ray = ray;
        m_StartMoveTime = Time.time;

        m_Target = findTarget();
    }

    private void FixedUpdate()
    {
        transform.position += m_Ray.direction * m_FireSpeed * Time.fixedDeltaTime;
        var target = findTarget();
        if (target.collider != null)
        {
            if (Vector3.Distance(transform.position, target.point) < m_FireSpeed * Time.fixedDeltaTime * 2)
            {
                hitTarget(target);
            }
        }
        else if (m_Target.collider != null)
        {
            if (Vector3.Distance(transform.position, m_Target.point) < m_FireSpeed * Time.fixedDeltaTime * 2)
            {
                hitTarget(m_Target);
            }
        }

        if (Time.time - m_StartMoveTime > m_MaxLifeTime)
        {
            Destroy(gameObject);
        }
    }

    private void hitTarget(RaycastHit hit)
    {
        var vfx = Instantiate(m_CollideEffect, null);
        vfx.transform.position = hit.point;
        vfx.transform.eulerAngles = hit.normal;

        Destroy(gameObject);
        var target = hit.collider.GetComponent<IBulletTarget>();
        if (target != null)
        {
            target.dealDmg(m_Atk);
        }
    }

    private RaycastHit findTarget()
    {
        if (Physics.Raycast(m_Ray, out var hit, 200, LayerMask.GetMask("Environment", "Zombie")))
        {
            return hit;
        }
        return default(RaycastHit);
    }
}

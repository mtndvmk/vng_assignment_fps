using NExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieProfile : MonoBehaviour, IBulletTarget, IExplosionTarget
{
    [SerializeField] private Animator m_MyAnimator;
    [SerializeField] private float m_MaxHp;
    [SerializeField] private float m_AttackRange;
    [SerializeField] private int m_Atk = 1;

    public static event Action<ZombieProfile> onZombieDeadEvent;

    public float AttackRange => m_AttackRange; 

    private float m_CurrentHp;
    public bool IsDead { get; private set; }

    private void OnValidate()
    {
        if (m_MyAnimator)
        {
            m_MyAnimator = GetComponent<Animator>();
        }
    }

    private void Start()
    {
        m_CurrentHp = m_MaxHp;
    }

    public void setProfile(int maxHp, int atk, float speed)
    {
        m_MaxHp = maxHp;
        m_CurrentHp = m_MaxHp;
        m_Atk = atk;
        GetComponent<ZombieNavMesh>().setSpeed(speed);
    }
    public void multiBase(float f)
    {
        m_MaxHp *= f;
        m_CurrentHp *= f;
        m_Atk = (int)(m_Atk * f);
        GetComponent<ZombieNavMesh>().multiSpeed(f);
    }
    public void playDead()
    {
        if (!IsDead)
        {
            GetComponent<Collider>().enabled = false;
            GetComponent<ZombieNavMesh>().stopNav();
            m_MyAnimator.Play("Dead");
            onZombieDeadEvent?.Invoke(this);
            gameObject.transform.parent = null;
            GetComponent<AudioSource>().Stop();
        }
    }
    public void playEat()
    {
        if (!IsDead)
        {
            m_MyAnimator.Play("Zombie_Eating");
        }
    }
    public void playWalk()
    {
        if (!IsDead)
        {
            m_MyAnimator.Play("Zombie_Walk");
        }
    }
    public void attack(IZombieTarget target)
    {
        target.dealDmg(m_Atk);
    }

    public void dealDmg(int hp)
    {
        m_CurrentHp -= hp;
        if (m_CurrentHp <= 0)
        {
            playDead();
            IsDead = true;
        }
    }
}

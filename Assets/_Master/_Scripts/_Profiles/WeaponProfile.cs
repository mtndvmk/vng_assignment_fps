using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponProfile : MonoBehaviour
{
    public int weaponType;
    public float animYAngle;
    public Vector3 weaponPosition;
    public Vector3 weaponRotation;
    public int maxBullet;
    public float m_FireDelayTime;
    public BulletProfile bulletProfile;
    public GameObject vfx_FireObject;
    public AudioSource audioSrc;
    public int LeftBullet { get; set; }
    private float m_LastFireTime;

    public void init()
    {
        transform.localPosition = weaponPosition;
        transform.localEulerAngles = weaponRotation;
        LeftBullet = maxBullet;
    }

    [ContextMenu("Update transform")]
    public void updateTransform()
    {
        weaponPosition = transform.localPosition;
        weaponRotation = transform.localEulerAngles;
    }
    public bool isDelayFire()
    {
        if (Time.time - m_LastFireTime <= m_FireDelayTime)
        {
            return true;
        }
        return false;
    }
    public void updateFireTime(float time)
    {
        m_LastFireTime = time;
    }
    public void playFireEffect(bool isStart)
    {
        vfx_FireObject.gameObject.SetActive(isStart);
    }
    public void playFireSound(bool isStart)
    {
        if (audioSrc.isPlaying != isStart)
        {
            if (isStart)
            {
                audioSrc.Play();
                audioSrc.loop = true;
            }
            else
            {
                audioSrc.loop = false;
            }
        }
    }
}

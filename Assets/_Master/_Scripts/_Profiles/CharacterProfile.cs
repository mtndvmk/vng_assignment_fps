using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterProfile : MonoBehaviour, IZombieTarget, IExplosionTarget
{
    [SerializeField] private int m_MaxHp = 5;
    [SerializeField] private bool m_IsMainCharacter;

    private int m_Hp;

    public bool IsCanRecover => m_Hp < m_MaxHp;

    public event Action onDeadEvent;

    void Start()
    {
        m_Hp = m_MaxHp;
        if (m_IsMainCharacter)
        {
            CharacterUIManager.Instance.setLifeUI(m_Hp);
        }
    }

    public void dealDmg(int hp)
    {
        m_Hp -= hp;
        m_Hp = Mathf.Min(m_Hp, m_MaxHp);
        if (m_IsMainCharacter)
        {
            CharacterUIManager.Instance.setLifeUI(m_Hp);
            if (hp > 0)
            {
                CharacterUIManager.Instance.showAttackedWarning();
            }
        }
        if (m_Hp <= 0)
        {
            GetComponentInChildren<Animator>().SetBool("Death_b", true);
            GetComponentInChildren<CharacterController>().enabled = false;
            onDeadEvent?.Invoke();
        }
    }

    public int getLeftHp()
    {
        return m_Hp;
    }
}

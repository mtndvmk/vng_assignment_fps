using NExtension;
using NExtension.NUtilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterUIManager : NSingleton<CharacterUIManager>
{
    [SerializeField] private Image m_AttackedWarningImage;
    [SerializeField] private GameObject[] m_LifeUIs;
    [SerializeField] private Text m_WeaponBulletText;
    [SerializeField] private Text m_FirstAidCountText;
    [SerializeField] private Text m_ScoreText;
    [SerializeField] private Text m_ZombieLeftText;
    [SerializeField] private GameObject m_GameoverUI;
    [SerializeField] private Text m_GameoverScoreText;
    [SerializeField] private Button m_GameOverRestartButton;
    [SerializeField] private Button m_GameOverQuitButton;
    [SerializeField] private GameObject m_RecoverUI;
    [SerializeField] private GameObject m_InGameUI;
    [SerializeField] private GameObject m_TutorialUI;
    [SerializeField] private Button m_GoButton;

    protected override void onAwake()
    {
        m_GameOverRestartButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(0);
        });
        m_GameOverQuitButton.onClick.AddListener(() =>
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        });
        m_GoButton.onClick.AddListener(() =>
        {
            m_InGameUI.gameObject.SetActive(true);
            m_TutorialUI.gameObject.SetActive(false);
            GameplayManager.Instance.start();
        });
    }

    public void setLifeUI(int hp)
    {
        hp = hp > 0 ? hp : 0;
        for (int i = 0; i < hp; i++)
        {
            m_LifeUIs[i].gameObject.SetActive(true);
        }
        for (int i = hp; i < m_LifeUIs.Length; i++)
        {
            m_LifeUIs[i].gameObject.SetActive(false);
        }
    }
    public void updateWeaponUI(int inWeaponBullet, int bagBullet)
    {
        m_WeaponBulletText.text = inWeaponBullet + "/" + bagBullet;
    }
    public void updateFirstAidCountUI(int firstAidCount)
    {
        m_FirstAidCountText.text = firstAidCount.ToString();
    }
    public void updateScoreText(int score)
    {
        m_ScoreText.text = "Score: " + score;
    }
    public void updateZombieLeftText(int left)
    {
        m_ZombieLeftText.text = "Zombies\n" + left;
    }
    public void showGameover(int score)
    {
        int highestScore = PlayerPrefs.GetInt("Highscore", 0);
        if (score > highestScore)
        {
            PlayerPrefs.SetInt("Highscore", score);
            highestScore = score;
        }
        m_GameoverUI.gameObject.SetActive(true);
        m_GameoverScoreText.text = "Score: " + score + "\nHighest score: " + highestScore;
    }
    public void updateRecoverUI(float p)
    {
        m_RecoverUI.gameObject.SetActive(p > 0);
        m_RecoverUI.transform.GetChild(0).localScale = new Vector3(p, 1, 1);
    }
    public void showAttackedWarning()
    {
        NCoroutine.startCoroutine(coroutineAttackedWarning(), GetHashCode(), true);
    }
    IEnumerator coroutineAttackedWarning()
    {
        float t = 0.5f;
        while (t > 0)
        {
            var a = Mathf.Lerp(0.2f, 0, t / 0.5f);
            m_AttackedWarningImage.color = m_AttackedWarningImage.color.getTransparentColor(a);
            t -= Time.deltaTime;
            yield return null;
        }
        m_AttackedWarningImage.color = m_AttackedWarningImage.color.getTransparentColor(0);
    }
}

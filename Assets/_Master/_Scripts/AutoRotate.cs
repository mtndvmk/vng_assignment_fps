using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotate : MonoBehaviour
{
    [SerializeField] private Vector3 m_Axis = Vector3.up;
    [SerializeField] private float m_Speed = 100;

 
    void Update()
    {
        transform.Rotate(m_Axis, m_Speed * Time.deltaTime);   
    }
}

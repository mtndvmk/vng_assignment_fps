using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieNavMesh : MonoBehaviour
{
    [SerializeField] private ZombieProfile m_ZombieProfile;
    [SerializeField] private Transform m_Target;
    [SerializeField] private NavMeshAgent m_MyNashMeshAgent;
    [SerializeField] private float m_Speed;

    private bool m_IsNavigating;


    public bool HasPath => m_MyNashMeshAgent.hasPath;
    public bool IsNearTarget => Vector3.Distance(m_Target.transform.position, transform.position) <= m_ZombieProfile.AttackRange;
    public Transform Target => m_Target;

    private void Start()
    {
        stopNav();
        startNav(m_Target);
    }

    public void startNav(Transform target)
    {
        m_Target = target;
        continueNav();
    }
    public void continueNav()
    {
        if (!m_IsNavigating)
        {
            m_IsNavigating = true;
            m_MyNashMeshAgent.isStopped = false;
        }
    }
    public void stopNav()
    {
        if (m_IsNavigating)
        {
            m_IsNavigating = false;
            m_MyNashMeshAgent.isStopped = true;
        }
    }
    public void setSpeed(float speed)
    {
        m_Speed = speed;
    }
    public void multiSpeed(float f)
    {
        m_Speed *= f;
    }
    private void Update()
    {
        if (m_IsNavigating && !m_ZombieProfile.IsDead)
        {
            m_MyNashMeshAgent.SetDestination(m_Target.position);
            m_MyNashMeshAgent.speed = m_Speed;
        }
    }

    private void OnValidate()
    {
        if (!m_ZombieProfile)
        {
            m_ZombieProfile = GetComponent<ZombieProfile>();
        }
        if (!m_MyNashMeshAgent)
        {
            m_MyNashMeshAgent = GetComponent<NavMeshAgent>();
            m_Speed = Random.Range(2, 8);
        }
    }
}

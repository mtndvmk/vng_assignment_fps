using NExtension;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieEater : MonoBehaviour
{
    [SerializeField] private ZombieProfile m_ZombieProfile;
    [SerializeField] private ZombieNavMesh m_ZombieNavMesh;

    private bool m_CanEat = true;

    private void OnValidate()
    {
        if (!m_ZombieProfile)
        {
            m_ZombieProfile = GetComponent<ZombieProfile>();
        }
        if (!m_ZombieNavMesh)
        {
            m_ZombieNavMesh = GetComponent<ZombieNavMesh>();
        }
    }

    private void FixedUpdate()
    {
        if (!m_ZombieProfile.IsDead)
        {
            var target = nearTarget();
            if (target != null)
            {
                eat(target);
                m_ZombieNavMesh.stopNav();
                m_ZombieProfile.playEat();
            }
            else
            {
                m_ZombieProfile.playWalk();
                m_ZombieNavMesh.continueNav();
            }
        }
    }

    private IZombieTarget nearTarget()
    {
        if (m_ZombieNavMesh.IsNearTarget)
        {
            var t = m_ZombieNavMesh.Target.GetComponent<IZombieTarget>();
            if (t.getLeftHp() > 0)
            {
                return t;
            }
        }
        else
        {
            var ray = new Ray(transform.position + Vector3.up, transform.forward);
            if (Physics.Raycast(ray, out var hit, m_ZombieProfile.AttackRange / 2, LayerMask.GetMask("Character", "Environment")))
            {
                var target = hit.collider.GetComponent<IZombieTarget>();
                if (target != null && target.getLeftHp() > 0)
                {
                    return target;
                }
            }
        }
        return null;
    }

    private void eat(IZombieTarget target)
    {
        if (m_CanEat)
        {
            m_ZombieProfile.attack(target);
            m_CanEat = false;
            NCoroutine.runDelay(1f, () =>
            {
                m_CanEat = true;
            });
        }
    }
}

public interface IExplosionTarget
{
    void dealDmg(int hp);
}
public interface IZombieTarget
{
    void dealDmg(int hp);
    int getLeftHp();
}

public interface IBulletTarget
{
    void dealDmg(int hp);
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterController : MonoBehaviour
{
    [SerializeField] private ProvisionBoxProfile m_ProvisionBox;
    [SerializeField] private float m_Speed = 100;
    [SerializeField] private float m_DistanceToReset = 500;

    private Ray m_MoveRay;
    private Vector3 m_StartPosition;
    private float m_DistanceToProvide;

    public bool IsMoving { get; private set; }
    public bool IsProvided { get; private set; }

    public void moveTo(Vector3 from, Vector3 dir, float distanceToProvide)
    {
        m_StartPosition = from;
        m_DistanceToProvide = distanceToProvide < m_DistanceToReset * 0.8f ? distanceToProvide : m_DistanceToReset * 0.8f;
        IsMoving = true;

        transform.position = m_StartPosition;
        gameObject.SetActive(true);

        var angle = Vector3.SignedAngle(Vector3.forward, dir - transform.position, Vector3.up);
        transform.rotation = Quaternion.identity;
        transform.Rotate(transform.up, angle);
        m_MoveRay = new Ray(transform.position, dir - transform.position);

    }

    private void Update()
    {
        if (IsMoving)
        {
            transform.position += m_MoveRay.direction * Time.deltaTime * m_Speed;
            var moveDistance = Vector3.Distance(transform.position, m_StartPosition);
            if (moveDistance > m_DistanceToReset * 2)
            {
                reset();
            }
            else if (moveDistance > m_DistanceToProvide && !IsProvided)
            {
                IsProvided = true;
                var box = Instantiate(m_ProvisionBox, transform.parent);
                box.transform.position = transform.position;
                box.init(Random.Range(3, 31) * 10, Random.Range(1, 4));
            }
        }
    }

    private void reset()
    {
        gameObject.SetActive(false);
        IsMoving = false;
        IsProvided = false;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieGenerateManager : MonoBehaviour
{
    [SerializeField] private Transform m_ZombieContainer;
    [SerializeField] private ZombieProfile[] m_ZombiePrefabs;
    [SerializeField] private Transform[] m_GeneratePoints;

    [SerializeField] private float m_TimeToGenerate = 30;
    
    private float m_StartTime;
    private float m_LastGenerateTime;
    private Transform m_StartTarget;

    public void init(Transform startTarget)
    {
        m_StartTarget = startTarget;
        m_StartTime = Time.time;
        generate(10);
    }

    public void generate(int c)
    {
        var f = (Time.time - m_StartTime) / 150 + 1;
        for (int i = 0; i < c; i++)
        {
            var point = m_GeneratePoints[Random.Range(0, m_GeneratePoints.Length)];
            var zomPrefab = m_ZombiePrefabs[Random.Range(0, m_ZombiePrefabs.Length)];
            var z = Instantiate(zomPrefab, m_ZombieContainer);
            z.transform.position = point.transform.position + new Vector3(Random.Range(0.2f, 2), 0, Random.Range(0.2f, 2));
            z.multiBase(f);
            z.GetComponent<ZombieNavMesh>().startNav(m_StartTarget);
        }
        m_LastGenerateTime = Time.time;
    }

    private void Update()
    {
        if (Time.time - m_LastGenerateTime > m_TimeToGenerate)
        {
            m_LastGenerateTime = Time.time;
            generate(5);
        }
        CharacterUIManager.Instance.updateZombieLeftText(m_ZombieContainer.childCount);
    }
}

using NExtension;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProvisionManager : NSingleton<ProvisionManager>
{
    [SerializeField] private HelicopterController m_Helicopter;
    [SerializeField] private Vector3 m_StartProvideFrom;
    [SerializeField] private float m_MinTimeToProvide = 60;
    [SerializeField] private float m_MaxTimeToProvide = 120;

    private float m_NextProvideTime;

    private void OnValidate()
    {
        if (m_Helicopter != null)
        {
            m_StartProvideFrom = m_Helicopter.transform.position;
        }
    }

    private void Start()
    {
        startProvide();
    }
    public void startProvide()
    {
        startProvide(GameplayManager.Instance.getProvidePostion());
    }
    public void startProvide(Vector3 dst)
    {
        dst.y = m_StartProvideFrom.y;
        var dt = Vector3.Distance(dst, m_StartProvideFrom);
        dt = Random.Range(dt * 0.25f, dt * 1.25f);
        m_Helicopter.moveTo(m_StartProvideFrom, dst, dt);
        m_NextProvideTime += Random.Range(m_MinTimeToProvide, m_MaxTimeToProvide);
    }

    private void Update()
    {
        if (Time.time > m_NextProvideTime)
        {
            startProvide();
        }
    }
}

using NExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : NSingleton<GameplayManager>
{
    [SerializeField] private CharacterProfile m_CharacterProfile;
    [SerializeField] private ZombieGenerateManager m_ZombieGenerateManager;

    public Inventory Inventory { get; private set; }
    public bool IsStarted { get; private set; }
    public int score;
    
    public event Action onReceivedProvisionEvent;
    
    public void Start()
    {
        ZombieProfile.onZombieDeadEvent += killZombie;
        m_CharacterProfile.onDeadEvent += () =>
        {
            CharacterUIManager.Instance.showGameover(score);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        };
        reset();
    }

    public void start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        transform.GetChild(0).gameObject.SetActive(true);
        reset();
        m_ZombieGenerateManager.init(m_CharacterProfile.transform);
        IsStarted = true;
    }

    private void updateScoreUI()
    {
        CharacterUIManager.Instance.updateScoreText(score);
    }
    private void killZombie(ZombieProfile zombie)
    {
        score++;
        updateScoreUI();
    }
    public void reset()
    {
        Inventory = new Inventory();
        score = 0;
        updateScoreUI();
        m_CharacterProfile.GetComponent<SimpleCharacterController>().updateInventoryUI();
    }
    public Vector3 getProvidePostion()
    {
        return m_CharacterProfile.transform.position;
    }
    public void receiveProvision(ProvisionBoxProfile provisionBox)
    {
        Inventory.leftBullet += provisionBox.BulletCount;
        Inventory.leftFirstAidKit += provisionBox.FirstAidKitCount;
        onReceivedProvisionEvent?.Invoke();
    }
    private void OnDestroy()
    {
        ZombieProfile.onZombieDeadEvent -= killZombie;
    }
}

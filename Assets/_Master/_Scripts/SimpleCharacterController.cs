using NExtension;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCharacterController : MonoBehaviour
{
    [SerializeField] private CharacterController m_MyCharacterController;
    [SerializeField] private GameObject m_MyCamera;
    [SerializeField] private Transform m_ViewBody;
    [SerializeField] private Transform m_WeaponHand;
    [SerializeField] private Transform m_RedDot;

    [SerializeField] private float m_MoveSpeed = 1;
    [SerializeField] private float m_RotateSpeed = 1;
    [SerializeField] private float m_JumpForce = 1;
    [SerializeField, Range(0f, 180f)] private float m_UpperRotateAngle = 90;
    [SerializeField, Range(0f, 180f)] private float m_LowerRotateAngle = 45;

    [SerializeField] private WeaponProfile m_PrimaryWeaponPrefab;
    
    [SerializeField] private AudioSource m_MoveAudioSrc;
    [SerializeField] private AudioSource m_ReloadAudioSrc;
    [SerializeField] private AudioSource m_RecoveryAudioSrc;


    private List<WeaponProfile> m_CurrentWeapons = new List<WeaponProfile>();
    private WeaponProfile m_CurrentWeapon => m_CurrentWeapons[0];
    private Animator m_MyAnimator;
    private float m_Gravity;
    private CharacterProfile m_CharacterProfile;

    private float m_FireTime;

    private void Awake()
    {
        m_MyAnimator = GetComponentInChildren<Animator>();
        m_CharacterProfile = GetComponent<CharacterProfile>();
        GameplayManager.Instance.onReceivedProvisionEvent += () =>
        {
            updateInventoryUI();
        };
    }

    private void Start()
    {
        m_Gravity = Physics.gravity.y;
        addWeapon(m_PrimaryWeaponPrefab);
    }
    void Update()
    {
        if (GameplayManager.Instance.IsStarted)
        {
            exeGravity();
            exeJump();
            exeRotate();
            exeMove();
            exeFire();
            exeReload();
            exeRecover();
            exeFall();
        }
    }

    private void addWeapon(WeaponProfile weapon)
    {
        var w = Instantiate(weapon, m_WeaponHand);
        w.init();
        m_ViewBody.transform.localEulerAngles = new Vector3(0, w.animYAngle, 0);
        m_MyAnimator.SetInteger("WeaponType_int", w.weaponType);
        m_CurrentWeapons.Add(w);
        updateInventoryUI();
    }
    public void updateInventoryUI()
    {
        CharacterUIManager.Instance.updateWeaponUI(m_CurrentWeapon.LeftBullet, GameplayManager.Instance.Inventory.leftBullet);
        CharacterUIManager.Instance.updateFirstAidCountUI(GameplayManager.Instance.Inventory.leftFirstAidKit);
    }


    private void exeGravity()
    {
        if (!m_MyCharacterController.isGrounded)
        {
            m_Gravity += Physics.gravity.y * Time.deltaTime;
        }
    }
    private void exeJump()
    {
        if (getJumpInput())
        {
            if (m_MyCharacterController.isGrounded)
            {
                m_Gravity = m_JumpForce;
            }
        }
    }
    private void exeRotate()
    {
        var rotateInput = getRotateInput();
        var rX = m_MyCamera.transform.localEulerAngles.x + rotateInput.x * m_RotateSpeed;
        while (rX > 180) rX -= 360;
        while (rX < -180) rX += 360;
        rX = Mathf.Clamp(rX, -m_UpperRotateAngle, m_LowerRotateAngle);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + rotateInput.y * m_RotateSpeed, 0);
        m_MyCamera.transform.localEulerAngles = new Vector3(rX, 0, 0);
    }
    private void exeMove()
    {
        var moveInput = getMoveInput();
        if (m_MoveAudioSrc.isPlaying != (moveInput != Vector2.zero))
        {
            if (moveInput != Vector2.zero)
            {
                m_MoveAudioSrc.Play();
            }
            else
            {
                m_MoveAudioSrc.Stop();
            }
        }
        var moveDir = transform.forward * moveInput.x + transform.right * moveInput.y;
        moveDir.y = m_Gravity;
        m_MyCharacterController.Move(moveDir * m_MoveSpeed * Time.deltaTime);
    }
    private void exeFire()
    {
        if (getFireInput() && m_CurrentWeapon.LeftBullet > 0 && !m_IsReloading)
        {

            m_FireTime += Time.deltaTime;
            if (m_FireTime > 0.1f)
            {
                m_MyAnimator.SetBool("Shoot_b", true);
                m_MyAnimator.speed = 4;
            }
            else
            {
                m_MyAnimator.ResetTrigger("Shoot_b");
                m_MyAnimator.SetBool("Shoot_b", true);
            }
            m_CurrentWeapon.playFireEffect(true);
            m_CurrentWeapon.playFireSound(true);
            if (!m_CurrentWeapon.isDelayFire())
            {
                m_CurrentWeapon.LeftBullet--;
                updateInventoryUI();

                var origin = m_MyCamera.transform.position;
                var ray = new Ray(origin, m_RedDot.position - origin);

                m_CurrentWeapon.updateFireTime(Time.time);
                var bullet = Instantiate(m_CurrentWeapon.bulletProfile, null);
                bullet.transform.position = ray.GetPoint(1);

                bullet.move(ray);
            }
        }
        else
        {
            if (m_FireTime > 0)
            {
                if (m_FireTime > 0.2f)
                {
                    m_CurrentWeapon.playFireEffect(false);
                }
                else
                {
                    NCoroutine.runDelay(0.1f, () =>
                    {
                        m_CurrentWeapon.playFireEffect(false);
                    });
                }
                m_CurrentWeapon.playFireSound(false);
            }
            m_MyAnimator.SetBool("Shoot_b", false);
            m_FireTime = 0;
            m_MyAnimator.speed = 1; 
        }
    }
    private bool m_IsReloading;
    private void exeReload()
    {
        if (!m_IsReloading)
        {
            if (getReloadInput() && m_CurrentWeapon.LeftBullet < m_CurrentWeapon.maxBullet || m_CurrentWeapon.LeftBullet <= 0)
            {
                m_ReloadAudioSrc.Play();
                m_IsReloading = true;
                m_MyAnimator.SetBool("Reload_b", true);
                NCoroutine.runDelay(1.6f, () =>
                {
                    m_ReloadAudioSrc.Stop();
                    m_MyAnimator.SetBool("Reload_b", false);
                    GameplayManager.Instance.Inventory.leftBullet -= m_CurrentWeapon.maxBullet - m_CurrentWeapon.LeftBullet;
                    m_CurrentWeapon.LeftBullet = m_CurrentWeapon.maxBullet;
                    updateInventoryUI();
                    m_IsReloading = false;
                });
            }
        }
    }
    private float? m_FallY;
    private void exeFall()
    {
        if (!m_MyCharacterController.isGrounded)
        {
            if (!m_FallY.HasValue)
            {
                m_FallY = transform.position.y;
            }
            else
            {
                if (m_FallY - transform.position.y > 25)
                {
                    m_CharacterProfile.dealDmg(5);
                    m_FallY = null;
                }
            }
        }
        else
        {
            if (m_FallY.HasValue)
            {
                var dt = m_FallY - transform.position.y;
                if (dt > 10)
                {
                    m_CharacterProfile.dealDmg((int)((dt - 10) / 3));
                }
                m_FallY = null;
            }
        }
    }
    private bool m_IsRecovering;
    private bool IsCanRecover => m_CharacterProfile.IsCanRecover & GameplayManager.Instance.Inventory.leftFirstAidKit > 0;
    private void exeRecover()
    {
        if (!m_IsRecovering)
        {
            if (getFirstAidInput() && IsCanRecover)
            {
                m_IsRecovering = true;
                IEnumerator firstAid()
                {
                    m_RecoveryAudioSrc.Play();
                    float t = 5;
                    while (t > 0 && m_IsRecovering)
                    {
                        t -= Time.deltaTime;
                        CharacterUIManager.Instance.updateRecoverUI(t / 5);
                        yield return null;
                    }
                    CharacterUIManager.Instance.updateRecoverUI(0);
                    if (t <= 0)
                    {
                        m_CharacterProfile.dealDmg(-3);
                        GameplayManager.Instance.Inventory.leftFirstAidKit--;
                        updateInventoryUI();
                    }
                    m_RecoveryAudioSrc.Stop();
                }
                NCoroutine.startCoroutine(firstAid(), GetHashCode(), true);
            }
        }
        else
        {
            if (getFireInput() || getJumpInput() || getReloadInput() || getMoveInput() != Vector2.zero)
            {
                m_IsRecovering = false;
            }
        }
    }
    private Vector2 getMoveInput()
    {
        return new Vector2(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));
    }
    private Vector2 getRotateInput()
    {
        return new Vector2(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"));
    }
    private bool getJumpInput()
    {
        return Input.GetKeyDown(KeyCode.Space);
    }
    private bool getFireInput()
    {
        return Input.GetButton("Fire1");
    }
    private bool getReloadInput()
    {
        return Input.GetKeyDown(KeyCode.R);
    }
    private bool getFirstAidInput()
    {
        return Input.GetKeyDown(KeyCode.F) & (getMoveInput() == Vector2.zero);
    }
}
